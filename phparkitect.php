<?php

declare(strict_types=1);

use Arkitect\ClassSet;
use Arkitect\CLI\Config;
use Arkitect\Expression\ForClasses\DependsOnlyOnTheseNamespaces;
use Arkitect\Expression\ForClasses\ResideInOneOfTheseNamespaces;
use Arkitect\Rules\Rule;

return static function (Config $config): void {
    $moduleClasses = ClassSet::fromDir(__DIR__.'/src');

    $rules = [];

    $rules[] = Rule::allClasses()
        ->that(new ResideInOneOfTheseNamespaces('Bittacora\Bpanel4\Orders'))
        ->should(new DependsOnlyOnTheseNamespaces(
            'Bittacora\Bpanel4\Orders',
            'Bittacora\Bpanel4\Prices',
            '*\Contracts\*Module', // Otros módulos de bPanel, a traves de su interfaz
            '*Dtos*', // Dtos de otros módulos
            'Illuminate', // Laravel
            'Livewire',
            'Rappasoft\LaravelLivewireTables',
        ))
        ->because('no queremos que el módulo dependa de clases externas');

    $config->add($moduleClasses, ...$rules);
};