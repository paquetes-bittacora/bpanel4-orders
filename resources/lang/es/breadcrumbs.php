<?php

declare(strict_types=1);

return [
    'orders-admin' => 'Pedidos',
    'index' => 'Listado',
    'edit' => 'Editar',
];
