<?php

declare(strict_types=1);

return [
    'edit' => 'Editar',
    'order_status' => 'Estado del pedido',
    'details' => 'Detalles',
];
