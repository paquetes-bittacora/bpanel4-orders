@extends('bpanel4-public.layouts.regular-page')

@section('content')
    <div class="regular-page-container">
        <div class="d-flex flex-column justify-content-center align-items-center" style="min-height: 600px;">
            <h1 class="mb-5">{{ __('¡Pedido confirmado!') }}</h1>
            <p>{{ __('¡Gracias por su pedido! Revise su dirección de correo electrónico para ver los detalles de su pedido.') }}</p>
            @if(!$isGuest)
                <p class="mb-5">{{ __('También podrá verlos desde el apartado') }} <strong><a href="{{ route('my-account.index') }}">{{ __('Mis pedidos') }}</a></strong>,
                   {{ __('dentro de') }} <strong><a href="{{ route('my-account.index') }}">{{ __('Mi cuenta') }}</a></strong>.</p>
            @endif
            <a href="{{ route('home') }}"><button class="btn btn-primary">{{ __('Volver a la página de inicio') }}</button></a>
        </div>
    </div>
@endsection
