@extends('bpanel4-public.layouts.regular-page')

@section('content')
    <div class="regular-page-container">
        <div class="d-flex flex-column justify-content-center align-items-center" style="min-height: 600px;">
            <h1 class="mb-5">{{ __('Pedido cancelado') }}</h1>
            <p>{{ __('Se ha cancelado su pedido. Si no lo ha cancelado usted, la plataforma de pagos ha cancelado el pago y ha solicitado la cancelación del pedido.') }}</p>
            <p>{{ __('Si lo desea, puede volver a intentar hacer el pedido más tarde.') }}</p>
            <a href="{{ route('home') }}"><button class="btn btn-primary">{{ __('Volver a la página de inicio') }}</button></a>
        </div>
    </div>
@endsection
