<?php /** @var \Bittacora\Bpanel4\Orders\Models\Order\Order $order */ ?>
@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Edición de pedido: ' . $order->getId())

@section('content')
    <style>
        span.product-attribute-in-name {
            color: #6b6b6b;
        }
    </style>
    <div class="card bcard mb-5">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-orders::form.edit') }}</span>
            </h4>
        </div>
        <div class="p-3">
            <form action="{{ route('orders-admin.updateStatus', ['order' => $order]) }}" method="post">
                @method('put')
                @csrf
                @livewire('form::select', ['name' => 'order_status', 'labelText' =>
                __('bpanel4-orders::form.order_status'),
                'fieldWidth' => 7, 'required'=>true, 'allValues' => $orderStatus, 'selectedValues' =>
                [$order->order_status_id], 'helpText' => 'Si marca el pedido como "En espera", podrá modificarlo. Tenga en cuenta que al marcar el pedido como completado, se volverá a enviar la factura al cliente, con los datos actualizados.'])
                <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                    @livewire('form::save-button',['theme'=>'update'])
                    @livewire('form::save-button',['theme'=>'reset'])
                </div>
            </form>
        </div>
    </div>
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-orders::form.details') }}</span>
            </h4>
        </div>
        @livewire('bpanel4-orders::order-editor', [
            'order' => $order,
            'orderCanBeEdited' => $orderCanBeEdited,
            'invoiceNumber' => $invoiceNumber,
            'orderStatus' => $orderStatus,
            'invoice' => $invoice,
        ], key('order-editor-'. $order->getId()))
    </div>
@push('scripts')
    <script>
        /* Mostrar un aviso si el número de factura ya está en uso en otro pedido */
        const invoiceField = document.getElementById('invoiceNumber');

        if (null !== document.getElementById('invoiceNumber')) {
          invoiceField.addEventListener('keyup', function () {
            const invoiceNumber = invoiceField.value;
            const invoiceNumberStatus = document.getElementById('invoiceNumberStatus');
            let baseUrl = '{{ route('orders-admin.checkInvoiceNumber', [
            'order' => $order,
            'invoiceNumber' => ':invoiceNumber:'
          ]) }}';
            baseUrl = baseUrl.replace(':invoiceNumber:', invoiceNumber);
            fetch(baseUrl).then(function (response) {
              return response.json();
            }).then(function (response) {
              if (true === response) {
                // El número de factura ya está en uso
                invoiceNumberStatus.style.display = 'flex';
                return;
              }
              invoiceNumberStatus.style.display = 'none';
            }).catch(function (err) {
            });
          });
        }
    </script>
@endpush
@endsection
