<div>
    <div class="bpanel-order-details p-4">
        @if($orderCanBeEdited)
            <form method="post" action="{{ route('orders-admin.update', ['order' => $order]) }}">
                @endif
                <table style="width: 100%;" class="mb-4">
                    <tr>
                        <td class="p-0 align-top">
                            @if($orderCanBeEdited)
                                <h3>Detalles del pedido</h3>
                                @if(null !== $invoiceNumber)
                                    @livewire('form::input-date', ['required' => true, 'labelText' => 'Fecha de facturación', 'name' => 'invoice_date', 'idField' => 'invoice_date', 'enableTime' => false, 'fieldWidth' => 8, 'value' => $invoice->created_at->format('d/m/Y H:i')])
                                @endif
                                @livewire('form::input-number', ['name' => 'invoiceNumber', 'labelText' => __('Número de factura'), 'required'=>true, 'value' => old('invoiceNumber') ?? $invoiceNumber, 'labelWidth' => 3, 'fieldWidth' => 8])
                                <div class="form-group form-row text-danger"
                                     id="invoiceNumberStatus"
                                     style="display: none; margin-top: -10px;">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9 ">
                                        <i class="fas fa-exclamation-triangle"></i> El número de factura indicado ya está en uso
                                    </div>
                                </div>
                                @livewire('form::input-number', ['name' => '', 'labelText' => __('Número de pedido'), 'required'=>true, 'value' => str_pad($order->getId(), 6, '0', STR_PAD_LEFT), 'labelWidth' => 3, 'fieldWidth' => 8, 'readonly' => true ])
                                @if(config('orders.hide_client_details_on_order_edit')) <div style="display: none;">  @endif
                                @livewire('form::input-text', ['name' => 'clientName', 'labelText' => __('Nombre'), 'required'=> !config('orders.hide_client_details_on_order_edit'), 'value' => old('clientName') ?? $order->getClient()->getName(), 'labelWidth' => 3, 'fieldWidth' => 8])
                                @livewire('form::input-text', ['name' => 'clientSurname', 'labelText' => __('Apellidos'), 'required'=>!config('orders.hide_client_details_on_order_edit'), 'value' => old('clientSurname') ?? $order->getClient()->getSurname(), 'labelWidth' => 3, 'fieldWidth' => 8 ])
                                @livewire('form::input-text', ['name' => 'clientNif', 'labelText' => __('DNI/NIF/NIE'), 'required'=> !config('orders.hide_client_details_on_order_edit'), 'value' => old('clientNif') ?? $order->getClient()->getNif(), 'labelWidth' => 3, 'fieldWidth' => 8])
                                @livewire('form::input-text', ['name' => 'clientCompany', 'labelText' => __('Empresa'), 'required'=> !config('orders.hide_client_details_on_order_edit'), 'value' => old('clientCompany') ?? $order->getClient()->getCompany(), 'labelWidth' => 3, 'fieldWidth' => 8 ])
                                @livewire('form::input-text', ['name' => 'clientPhone', 'labelText' => __('Teléfono'), 'required'=>!config('orders.hide_client_details_on_order_edit'), 'value' => old('clientPhone') ?? $order->getClient()->getPhone(), 'labelWidth' => 3, 'fieldWidth' => 8 ])
                                @livewire('form::input-text', ['name' => 'clientEmail', 'labelText' => __('Email'), 'required'=>!config('orders.hide_client_details_on_order_edit'), 'value' => old('clientEmail') ?? $order->getClient()->getEmail(), 'labelWidth' => 3, 'fieldWidth' => 8 ])
                                @if(config('orders.hide_client_details_on_order_edit')) </div> @endif
                            @else
                                <table style="width: 95%;">
                                    <tr>
                                        <td colspan="2" class="p-0"><h3>Detalles de contacto</h3></td>
                                    </tr>
                                    @if(null !== $invoiceNumber)
                                        <tr>
                                            <td><strong>Fecha de factura:</strong></td>
                                            <td>{{ $invoice->created_at->format('d-m-Y') }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Número de factura:</strong></td>
                                            <td>{{ str_pad($invoiceNumber, 6, '0', STR_PAD_LEFT) }}</td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td><strong>Número de pedido:</strong></td>
                                        <td>{{ str_pad($order->getId(), 6, '0', STR_PAD_LEFT) }}</td>
                                    </tr>
                                    <tr @if(config('orders.hide_client_details_on_order_edit')) style="display:none;" @endif>
                                        <td><strong>Nombre: </strong></td>
                                        <td>
                                            <a href="{{ route('bpanel4-clients.edit', ['client' => $order->getClient()->getClientId() ])}}">
                                                {{ $order->getClient()->getName() }} {{ $order->getClient()->getSurname() }}
                                            </a>
                                        </td>
                                    </tr>
                                    <tr @if(config('orders.hide_client_details_on_order_edit')) style="display:none;" @endif>
                                        <td><strong>NIF/CIF:</strong></td>
                                        <td>{{ $order->getClient()->getNif() }}</td>
                                    </tr>
                                    @if('' != $order->getClient()->getCompany())
                                        <tr @if(config('orders.hide_client_details_on_order_edit')) style="display:none;" @endif>
                                            <td><strong>Empresa:</strong></td>
                                            <td>{{ $order->getClient()->getCompany() }}</td>
                                        </tr>
                                    @endif
                                    <tr @if(config('orders.hide_client_details_on_order_edit')) style="display:none;" @endif>
                                        <td><strong>Teléfono: </strong></td>
                                        <td>{{ $order->getClient()->getPhone() }}</td>
                                    </tr>
                                    <tr @if(config('orders.hide_client_details_on_order_edit')) style="display:none;" @endif>
                                        <td><strong>Email:</strong></td>
                                        <td>{{ $order->getClient()->getEmail() }}</td>
                                    </tr>
                                </table>
                            @endif
                        </td>
                        <td class="p-0 align-top">
                            @if($orderCanBeEdited)
                                <h3>Dirección de envío</h3>
                                @livewire('form::input-text', [
                                    'name' => 'clientShippingAddressName',
                                    'labelText' => __('Nombre'),
                                    'required'=>true,
                                    'value' => old('clientShippingAddressName') ?? $order->getShippingDetails()->getPersonName(),
                                    'labelWidth' => 2,
                                    'fieldWidth' => 9
                                ])
                                @livewire('form::input-text', [
                                    'name' => 'clientShippingAddressSurname',
                                    'labelText' => __('Apellidos'),
                                    'required'=>true,
                                    'value' => old('clientShippingAddressSurname') ?? $order->getShippingDetails()->getPersonSurname(),
                                    'labelWidth' => 2,
                                    'fieldWidth' => 9
                                ])
                                @livewire('form::input-text', [
                                    'name' => 'clientShippingAddressPhone',
                                    'labelText' => __('Teléfono'),
                                    'required'=>false,
                                    'value' => old('clientShippingAddressPhone') ?? $order->getShippingDetails()->getPersonPhone(),
                                    'labelWidth' => 2,
                                    'fieldWidth' => 9
                                ])
                                @livewire('form::input-text', ['name' => 'clientShippingAddress', 'labelText' => __('Dirección'), 'required'=>true, 'value' => old('clientShippingAddress') ?? $order->getShippingDetails()->getAddress(), 'labelWidth' => 2, 'fieldWidth' => 9 ])
                                <div class="form-group form-row">
                                    <div class="col-sm-2 col-form-label text-sm-right">
                                        <label for="id-form-field-1" class="mb-0  ">
                                            <span class="text-danger">*</span> País
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="m-b">
                                            @livewire('country-select', [
                                                'fieldName' => 'clientShippingCountry',
                                                'selectedCountry' => old('clientShippingCountry') ?? $order->getShippingDetails()->getCountryId() ?? 0,
                                                'class' => 'form-control'
                                            ], key('country-select-'. Str::random(10)))
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-row">
                                    <div class="col-sm-2 col-form-label text-sm-right">
                                        <label for="id-form-field-1" class="mb-0  ">
                                            <span class="text-danger">*</span> Provincia
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="m-b">
                                            @livewire('state-select', [
                                                'fieldName' => 'clientShippingState',
                                                'selectedCountry' => old('clientShippingCountry') ?? $order->getShippingDetails()->getCountryId() ?? 0,
                                                'selectedState' => old('clientShippingState') ?? $order->getShippingDetails()->getStateId() ?? 0,
                                                'class' => 'form-control w-100'
                                            ], key('state-select-' . Str::random(10)) )
                                        </div>
                                    </div>
                                </div>
                                @livewire('form::input-text', ['name' => 'clientShippingLocation', 'labelText' => __('Localidad'), 'required'=>true, 'value' => old('clientShippingLocation') ?? $order->getShippingDetails()->getLocation(), 'labelWidth' => 2, 'fieldWidth' => 9 ])
                                @livewire('form::input-text', ['name' => 'clientShippingPostalCode', 'labelText' => __('C.P.'), 'required'=>true, 'value' => old('clientShippingPostalCode') ?? $order->getShippingDetails()->getPostalCode(), 'labelWidth' => 2, 'fieldWidth' => 9 ])
                            @else
                                <table>
                                    <tr>
                                        <td colspan="2" class="p-0"><h3>Dirección de envío</h3></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Nombre:</strong></td>
                                        <td>{{ $order->getShippingDetails()->getPersonName() !== '' ? $order->getShippingDetails()->getPersonName() : $order->getClient()->getName() }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Apellidos:</strong></td>
                                        <td>{{ $order->getShippingDetails()->getPersonSurname() !== '' ? $order->getShippingDetails()->getPersonSurname() : $order->getClient()->getSurname() }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Teléfono:</strong></td>
                                        <td>{{ $order->getShippingDetails()->getPersonPhone() !== '' ? $order->getShippingDetails()->getPersonPhone() : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Dirección:</strong></td>
                                        <td>{{ $order->getShippingDetails()->getAddress() }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>País:</strong></td>
                                        <td>{{ $order->getShippingDetails()->getCountryName() }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Provincia:</strong></td>
                                        <td>{{ $order->getShippingDetails()->getStateName() }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Localidad:</strong></td>
                                        <td>{{ $order->getShippingDetails()->getLocation() }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Código postal:</strong></td>
                                        <td>{{ $order->getShippingDetails()->getPostalCode() }}</td>
                                    </tr>
                                </table>
                            @endif
                        </td>
                        <td class="align-top">
                            @if($orderCanBeEdited)
                                <h3>Dirección de facturacíon</h3>
                                @livewire('form::input-text', [
                                    'name' => 'clientBillingAddressName',
                                    'labelText' => __('Nombre'),
                                    'required'=>true,
                                    'value' => old('clientBillingAddressName') ?? $order->getBillingDetails()->getPersonName(),
                                    'labelWidth' => 2,
                                    'fieldWidth' => 9
                                ])
                                @livewire('form::input-text', [
                                    'name' => 'clientBillingAddressSurname',
                                    'labelText' => __('Apellidos'),
                                    'required'=>true,
                                    'value' => old('clientBillingAddressSurname') ?? $order->getBillingDetails()->getPersonSurname(),
                                    'labelWidth' => 2,
                                    'fieldWidth' => 9
                                ])
                                @livewire('form::input-text', [
                                    'name' => 'clientBillingAddressNif',
                                    'labelText' => __('DNI/NIF/NIE'),
                                    'required'=>true,
                                    'value' => old('clientBillingAddressNif') ?? $order->getBillingDetails()->getPersonNif(),
                                    'labelWidth' => 2,
                                    'fieldWidth' => 9
                                ])
                                @livewire('form::input-text', [
                                    'name' => 'clientBillingAddressPhone',
                                    'labelText' => __('Teléfono'),
                                    'required'=>false,
                                    'value' => old('clientBillingAddressPhone') ?? $order->getBillingDetails()->getPersonPhone(),
                                    'labelWidth' => 2,
                                    'fieldWidth' => 9
                                ])
                                @livewire('form::input-text', ['name' => 'clientBillingAddress', 'labelText' => __('Dirección'), 'required'=>true, 'value' => old('clientBillingAddress') ?? $order->getBillingDetails()->getAddress(), 'labelWidth' => 2, 'fieldWidth' => 9 ])
                                <div class="form-group form-row">
                                    <div class="col-sm-2 col-form-label text-sm-right">
                                        <label for="id-form-field-1" class="mb-0  ">
                                            <span class="text-danger">*</span> País
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="m-b">
                                            @livewire('country-select', [
                                            'fieldName' => 'clientBillingCountry',
                                            'selectedCountry' => old('clientBillingCountry') ?? $order->getBillingDetails()->getCountryId() ?? 0,
                                            'class' => 'form-control'
                                            ], key('country-select-'. Str::random(10)))
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-row">
                                    <div class="col-sm-2 col-form-label text-sm-right">
                                        <label for="id-form-field-1" class="mb-0  ">
                                            <span class="text-danger">*</span> Provincia
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="m-b">
                                            @livewire('state-select', [
                                            'fieldName' => 'clientBillingState',
                                            'selectedCountry' => old('clientBillingCountry') ?? $order->getBillingDetails()->getCountryId() ?? 0,
                                            'selectedState' => old('clientBillingState') ?? $order->getBillingDetails()->getStateId() ?? 0,
                                            'class' => 'form-control w-100'
                                            ], key('state-select-' . Str::random(10)) )
                                        </div>
                                    </div>
                                </div>
                                @livewire('form::input-text', ['name' => 'clientBillingLocation', 'labelText' => __('Localidad'), 'required'=>true, 'value' => old('clientBillingLocation') ?? $order->getBillingDetails()->getLocation(), 'labelWidth' => 2, 'fieldWidth' => 9 ])
                                @livewire('form::input-text', ['name' => 'clientBillingPostalCode', 'labelText' => __('C.P.'), 'required'=>true, 'value' => old('clientBillingPostalCode') ?? $order->getBillingDetails()->getPostalCode(), 'labelWidth' => 2, 'fieldWidth' => 9 ])
                            @else
                                <table>
                                    <tr>
                                        <td colspan="2" class="p-0"><h3>Dirección de facturación</h3></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Nombre:</strong></td>
                                        <td>{{ $order->getBillingDetails()->getPersonName() !== '' ? $order->getBillingDetails()->getPersonName() : $order->getClient()->getName() }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Apellidos:</strong></td>
                                        <td>{{ $order->getBillingDetails()->getPersonSurname() !== '' ? $order->getBillingDetails()->getPersonSurname() : $order->getClient()->getSurname() }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>DNI/NIF/NIE:</strong></td>
                                        <td>{{ $order->getBillingDetails()->getPersonNif() !== '' ? $order->getBillingDetails()->getPersonNif() : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Teléfono:</strong></td>
                                        <td>{{ $order->getBillingDetails()->getPersonPhone() !== '' ? $order->getBillingDetails()->getPersonPhone() : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Dirección:</strong></td>
                                        <td>{{ $order->getBillingDetails()->getAddress() }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>País:</strong></td>
                                        <td>{{ $order->getBillingDetails()->getCountryName() }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Provincia:</strong></td>
                                        <td>{{ $order->getBillingDetails()->getStateName() }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Localidad:</strong></td>
                                        <td>{{ $order->getBillingDetails()->getLocation() }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Código postal:</strong></td>
                                        <td>{{ $order->getBillingDetails()->getPostalCode() }}</td>
                                    </tr>
                                </table>
                            @endif

                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <strong>Forma de pago: </strong> {{ $order->payment_method_name }} &nbsp;
                            <strong>Método de envío:</strong> {{ $order->getShippingDetails()->getShippingMethodName() }}
                        </td>
                    </tr>
                </table>

                @if($orderCanBeEdited)
                    <h2>Observaciones</h2>
                    <div class="mb-3">
                        <textarea name="order_notes" class="form-control">{{ $order->getOrderNotes() }}</textarea>
                    </div>
                @endif

                @if($orderCanBeEdited)
                    <div>
                        @if($errors->any())
                            {!! implode('', $errors->all('<div class="text-danger text-center font-bold">:message</div>')) !!}
                        @endif
                    </div>
                    <div class="text-center">
                        <button class="btn btn-primary"><i class="far fa-save"></i> Guardar</button>
                    </div>
                @endif

                @if($orderCanBeEdited)
                    @csrf
                    @method('PUT')
            </form>
        @endif

        <h2>Productos</h2>
        <table class="products order-products-table mb-4 w-100">
            <thead>
                <tr>
                    <th class="text-md">Producto</th>
                    <th class="text-right text-md">Cantidad</th>
                    <th class="text-right text-md">Precio ud.</th>
                    <th class="text-right text-md">Total</th>
                    <th class="text-right text-md">@if($orderCanBeEdited)
                            Acciones
                        @endif</th>
                </tr>
            </thead>
            <tbody>
                @if (session()->has('order-editor-success'))
                    <div class="alert alert-success text-center alert-dismissible fade show" role="alert">
                        Producto actualizado
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @foreach($order->getProducts() as $product)
                    @if(in_array($product->getId(), $productsBeingEdited))
                        @include('bpanel4-orders::bpanel.livewire.partials.order-product-editor')
                    @else
                        @include('bpanel4-orders::bpanel.livewire.partials.order-product-row')
                    @endif
                @endforeach
                @foreach($order->getGiftedProducts() as $giftedProduct)
                        @include('bpanel4-orders::bpanel.livewire.partials.order-product-row', ['product' => $giftedProduct])
                @endforeach
                @foreach($order->getCoupons() as $coupon)
                    <tr>
                        <td colspan="3">Cupón descuento '{{ $coupon->getCode() }}'</td>
                        <td class="text-right">{{ $coupon->getDiscount() }}</td>
                    </tr>
                @endforeach
                @if($showAddProductForm)
                    <tr>
                        <td colspan="5">
                            @livewire('bpanel4-orders::order-product-creator', [
                                'orderId' => $order->getId(),
                            ], key('create-order-product-' . $order->getId()))
                        </td>
                    </tr>
                @else
                    @if($orderCanBeEdited)
                        <tr>
                            <td colspan="5" class="text-right order-product-row py-3">
                                <button class="btn btn-primary btn-sm" wire:click="$set('showAddProductForm', true)"><i
                                            class="fa fa-plus"></i> Añadir producto
                                </button>
                            </td>
                        </tr>
                    @endif
                @endif
                <tr style="border-top: 1px solid #555" class="order-summary-row">
                    <td colspan="3">Subtotal</td>
                    <td class="text-right">{{ \Bittacora\Bpanel4\Prices\Types\Price::fromInt($order->getSubtotal()) }}</td>
                    <td></td>
                </tr>
                <tr class="order-summary-row">
                    <td colspan="3">Gastos de envío</td>
                    <td class="text-right">{{ \Bittacora\Bpanel4\Prices\Types\Price::fromInt($order->getShippingCosts()) }}</td>
                    <td></td>
                </tr>
                <tr class="order-summary-row">
                    <td colspan="3">Impuestos</td>
                    <td class="text-right">{{ \Bittacora\Bpanel4\Prices\Types\Price::fromInt($order->getTotalTaxes()) }}</td>
                    <td></td>
                </tr>
                <tr class="order-summary-row total-row">
                    <td colspan="3">Total</td>
                    <td class="text-right">{{ \Bittacora\Bpanel4\Prices\Types\Price::fromInt($order->getOrderTotal()) }}</td>
                    <td></td>
                </tr>
                @if (null !== $order->getVatNumber())
                    <tr>
                        <td colspan="3"><strong>Nota: se usó el Nº de IVA
                                <em>{{ $order->getVatNumber() }}</em> desde la IP
                                <em>{{ $order->getIpAddress() }} ({{ $order->getCountryCode() }})</em></strong></td>
                        <td></td>
                    </tr>
                @endif
            </tbody>
        </table>
        @if(null !== $order->getOrderNotes() && !$orderCanBeEdited)
            <h2>Observaciones</h2>
            <div class="mb-3">
                {{ $order->getOrderNotes() }}
            </div>
        @endif
        <h2>Información adicional sobre el pago</h2>
        @if(null === $order->getPaymentStatus())
            <p>La pasarela de pago no ha remitido información adicional.</p>
        @else
            {{ $order->getPaymentStatus() }}
        @endif
    </div>
    <style>
        div.order-product-editor {
            --internal-spacing: 1.5rem;
            border: 1px solid #ddd;
            padding: var(--internal-spacing);
            margin: 1rem 0;
            border-radius: .5rem;

            .title {
                font-weight: bold;
                font-size: 1.1rem;
                margin-bottom: 1rem;
            }

            .fields {
                display: grid;
                grid-template-columns: repeat(7, 1fr);
                gap: var(--internal-spacing);
                margin-bottom: var(--internal-spacing);
            }

            .fields > div > label {
                display: block;

            }
        }

        .order-products-table {
            border-radius: .3rem;
            overflow: hidden;
        }

        .order-products-table thead {
            background: #2470bd;
            color: white;
        }

        .order-products-table thead th {
            padding: .5rem 1rem;
        }

        tr.order-product-row {
            background: #fcfcfc;
            border-bottom: 1px solid #ebebeb;
        }

        tr.order-product-row:first-child {
            border-top: 1px solid #ebebeb;
        }

        tr.order-product-row:nth-child(even) {
            background: #f7f7f7;
        }

        tr.order-product-row td {
            padding: 1rem;
        }

        tr.order-summary-row {
            background: #fafafa;
        }

        tr.order-summary-row td {
            padding: .5rem 1rem;
            font-size: 1.1rem;
        }

        tr.order-summary-row td:first-child {
            font-weight: bold;
            text-align: right;
        }

        tr.order-summary-row td:first-child:after {
            content: ':';
        }
    </style>
</div>