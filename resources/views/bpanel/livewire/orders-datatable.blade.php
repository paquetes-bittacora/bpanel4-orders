<?php /** @var \Bittacora\Bpanel4\Orders\Models\Order\Order $row */ ?>
<td>{{ $row->getClient()->getName() }}</td>
<td>{{ $row->getClient()->getSurname() }}</td>
<td class="text-center order-number">{{ $row->getId() }}</td>
<td class="text-center order-total">{{ \Bittacora\Bpanel4\Prices\Types\Price::fromInt($row->getOrderTotal()) }}</td>
<td class="text-center order-date">{{ $row->getDate()->format('d-m-Y H:i:s') }}</td>
<td class="order-list-status"><div style="color: {{ $row->getStatus()->color }};background: {{ $row->getStatus()->background_color }}">{{ $row->getStatus()->name }}</div></td>
<td class="text-center">
    @if(!\Bittacora\Bpanel4\Invoices\Models\Invoice::where('order_id', $row->getId())->whereNotNull('invoice_path')->exists())
        <a href="{{ URL::signedRoute('bpanel4-invoices.get-invoice', ['order' => $row->getId()]) }}"
           title="Generar factura">
            <i class="far fa-plus"></i>
        </a>
    @else
        <a href="{{ URL::signedRoute('bpanel4-invoices.get-invoice', ['order' => $row->getId()]) }}"
           title="Descargar factura">
            <i class="far fa-file-invoice-dollar"></i>
        </a>
    @endif
</td>
<td class="text-center">
    @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'orders-admin', 'model' =>
    $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'el pedido?'],
    key('order-buttons-'.$row->id))
</td>
