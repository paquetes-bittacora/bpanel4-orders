<div class="order-product-editor">
    <div class="title">Editar producto "{!! strip_tags($orderProduct->getName()) !!}"</div>
    <div class="fields">
        <div class="name">
            <label for="order-product-{{ $orderProduct->getId() }}-name">Nombre:</label>
            <input wire:model="name" id="order-product-{{ $orderProduct->getId() }}-name" maxlength="250" class="form-control">
        </div>
        <div class="reference">
            <label for="order-product-{{ $orderProduct->getId() }}-reference">Referencia:</label>
            <input wire:model="reference" id="order-product-{{ $orderProduct->getId() }}-reference" maxlength="250" class="form-control">
        </div>
        <div class="quantity">
            <label for="order-product-{{ $orderProduct->getId() }}-quantity">Cantidad:</label>
            <input wire:model="quantity"
                   type="number"
                   min="1"
                   step="1"
                   id="order-product-{{ $orderProduct->getId() }}-quantity"
                   class="form-control"
            >
        </div>
        <div class="unit-price">
            <label for="order-product-{{ $orderProduct->getId() }}-price">Precio unitario (sin impuestos):</label>
            <input wire:model="paidUnitPrice"
                   id="order-product-{{ $orderProduct->getId() }}-price"
                   type="number"
                   min="0"
                   step="0.01"
                   class="form-control"
            >
        </div>
        <div class="vat-rate">
            <label for="order-product-{{ $orderProduct->getId() }}-vat-rate">Tipo de IVA:</label>
            <select wire:model="vatRate" id="order-product-{{ $orderProduct->getId() }}-vat-rate" class="form-control">
                @foreach(\Bittacora\Bpanel4\Vat\Models\VatRate::whereActive(true)->get()->all() as $vatRate)
                   <option value="{{ $vatRate->getRate() }}">{{ $vatRate->getRate() }} %</option>
                @endforeach
            </select>
        </div>
        <div class="total">
            <label>Total (sin impuestos):</label>
            <input readonly wire:model="total" class="form-control">
        </div>
        <div class="total-with-taxes">
            <label>Total (con impuestos):</label>
            <input readonly wire:model="totalWithTaxes" class="form-control">
        </div>
    </div>
    <div class="text-right">
        <button class="btn btn-secondary" wire:click="cancel"><i class="fas fa-times"></i> Cancelar</button>
        <button class="btn btn-primary ml-3" wire:click="updateProduct"><i class="fas fa-save"></i> Guardar</button>
    </div>

</div>