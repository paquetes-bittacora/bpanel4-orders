<tr class="order-product-row">
    <td>
        @if(null !== $product->getProductId())
            <a href="{{ route('bpanel4-products.edit', ['model' => $product->getProductId()]) }}"
               class="d-flex"
               style="gap: 1rem">
                {!! $product->getName() !!}
            </a>
        @else
            <div class="product-name d-flex" style="gap: 1rem;">
                {!! $product->getName() !!}
            </div>
        @endif
        <div><small><em>Ref.: {{ $product->getReference() }}</em></small></div>
        <livewire:bpanel4-orders::livewire.order-product-meta-editor :orderProduct="$product"
                                                                     :enableEdit="$orderCanBeEdited"
                                                                     wire:key="order-product-meta-{{ $product->getId() }}"/>
    </td>
    <td class="text-right">{{ $product->getQuantity() }}</td>
    <td class="text-right">{{ $product->getPaidUnitPrice() }}</td>
    <td class="text-right">{{ $product->getTotal() }}</td>
    <td class="text-right">
        @if($orderCanBeEdited)
            <button class="btn btn-link" wire:click="editProduct({{ $product->getId() }})">
                <i class="far fa-pencil text-info mr-2" title="Editar producto"></i></button>
            <button class="btn btn-link"
                    wire:click="removeProduct({{ $product->getId() }})"
                    onclick="confirm('¿Confirma que desea eliminar el producto del pedido?') || event.stopImmediatePropagation()">
                <i class="far fa-trash-alt text-danger" title="Eliminar producto"></i>
            </button>
        @endif
    </td>
</tr>
