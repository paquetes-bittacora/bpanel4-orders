<tr>
    <td colspan="5" class="order-product-editor">
        @livewire('bpanel4-orders::order-product-editor', ['orderProduct' => $product], key('order-product-editor-' . $product->getId()))
    </td>
</tr>