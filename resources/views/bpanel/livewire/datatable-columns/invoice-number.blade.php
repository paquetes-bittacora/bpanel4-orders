@php use Bittacora\Bpanel4\Invoices\Models\Invoice; @endphp
@if(!Invoice::where('order_id', $row->getId())->whereNotNull('invoice_number')->exists())
    -
@else
    {{ Invoice::where('order_id', $row->getId())->whereNotNull('invoice_number')->firstOrFail()->getInvoiceNumber()}}
@endif
