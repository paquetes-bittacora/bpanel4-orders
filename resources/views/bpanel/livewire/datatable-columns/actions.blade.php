@livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'orders-admin', 'model' =>
$row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'el pedido?'],
key('order-buttons-'.$row->id))
