@if(!\Bittacora\Bpanel4\Invoices\Models\Invoice::where('order_id', $row->getId())->whereNotNull('invoice_path')->exists())
    <a href="{{ URL::signedRoute('bpanel4-invoices.get-invoice', ['order' => $row->getId()]) }}"
       title="Generar factura">
        <i class="far fa-plus"></i>
    </a>
@else
    <a href="{{ URL::signedRoute('bpanel4-invoices.get-invoice', ['order' => $row->getId()]) }}"
       title="Descargar factura">
        <i class="far fa-file-invoice-dollar"></i>
    </a>
@endif
