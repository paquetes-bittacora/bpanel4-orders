@extends('bpanel4-public.layouts.regular-page')
@section('title')
    Mis pedidos
@endsection
@livewireStyles
@section('content')
    <div class="regular-page-container">
        @include('bpanel4-clients::public.my-account.menu')
        <h1>{{ __('Mis pedidos') }}</h1>
        <div class="regular-page-container my-orders-page">
            <div class="row">
                <div class="col-12">
                    @livewire('bpanel4-orders::livewire.client-orders-table')
                </div>
            </div>
        </div>
    </div>
@endsection
