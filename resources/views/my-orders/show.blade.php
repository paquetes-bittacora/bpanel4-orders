<?php
/**
 * @var \Bittacora\Bpanel4\Orders\Models\Order\Order $order
 */
?>
@extends('bpanel4-public.layouts.regular-page')
@section('title')
    Detalles del pedido {{ $order->getId() }}
@endsection
@livewireStyles
@section('content')
    <div class="regular-page-container">
        @include('bpanel4-clients::public.my-account.menu')
        <h1>Detalles del pedido {{ $order->getId() }}</h1>
        <div class="regular-page-container my-order-details-page">
            <div class="row">
                <div class="col-12">
                    @include('bpanel4-orders::my-orders.order-details')
                </div>
            </div>
        </div>
    </div>
@endsection
