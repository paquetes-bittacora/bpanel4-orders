{{-- NOTA: Esta vista se usa tanto en los emails como en el apartado "Mi cuenta" --}}
<style>
    span.product-attribute-in-name {
        display: block;
        font-size: 0.75em;
        line-height: 1em;
    }
</style>
<table>
    <tr>
        <th>{{ __('Forma de pago') }}: {{ $order->payment_method_name }}</th>
    </tr>
    <tr class="order-details-row">
        <td>
            <table style="width: 95%;" class="contact-details-block">
                <tr>
                    <td colspan="2"><h3>{{ __('Detalles de contacto') }}</h3></td>
                </tr>
                <tr>
                    <td><strong>{{ __('Número de pedido') }}:</strong></td>
                    <td>{{ str_pad($clientDetails->getOrderId(), 6, '0', STR_PAD_LEFT) }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Nombre') }}: </strong></td>
                    <td>{{ $clientDetails->getName() }} {{ $clientDetails->getSurname() }}</td>
                </tr>
                <tr>
                    <td><strong>NIF/CIF:</strong></td>
                    <td>{{ $clientDetails->getNif() }}</td>
                </tr>
                @if('' != $clientDetails->getCompany())
                    <tr>
                        <td><strong>{{ __('Empresa') }}:</strong></td>
                        <td>{{ $clientDetails->getCompany() }}</td>
                    </tr>
                @endif
                <tr>
                    <td><strong>{{ __('Teléfono') }}: </strong></td>
                    <td>{{ $clientDetails->getPhone() }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Email') }}:</strong></td>
                    <td>{{ $clientDetails->getEmail() }}</td>
                </tr>
            </table>
        </td>
        <td>
            <table class="shipping-details-block" @if($shippingDetails->getAddress() !== $billingDetails->getAddress()) style="width: 95%" @endif >
                <tr>
                    <td colspan="2"><h3>{{ __('Dirección de envío') }}</h3></td>
                </tr>
                <tr>
                    <td><strong>{{ __('Nombre') }}:</strong></td>
                    <td>{{ $shippingDetails->getPersonName() !== '' ? $shippingDetails->getPersonName() : $clientDetails->getName() }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Apellidos') }}:</strong></td>
                    <td>{{ $shippingDetails->getPersonSurname() !== '' ? $shippingDetails->getPersonSurname() : $clientDetails->getSurname()}}</td>
                </tr>
                @if($shippingDetails->getAddress() === $billingDetails->getAddress())
                    <tr>
                        <td><strong>{{ __('NIF/CIF/NIE') }}:</strong></td>
                        <td>{{ $billingDetails->getPersonNif() !== '' ? $billingDetails->getPersonNif() : '' }}</td>
                    </tr>
                @endif
                <tr>
                    <td><strong>{{ __('Teléfono') }}:</strong></td>
                    <td>{{ $shippingDetails->getPersonPhone() !== '' ? $shippingDetails->getPersonPhone() : '' }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Dirección') }}:</strong></td>
                    <td>{{ $shippingDetails->getAddress() }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Localidad') }}:</strong></td>
                    <td>{{ $shippingDetails->getLocation() }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Código postal') }}:</strong></td>
                    <td>{{ $shippingDetails->getPostalCode() }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Provincia') }}:</strong></td>
                    <td>{{ $shippingDetails->getStateName() }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('País') }}:</strong></td>
                    <td>{{ $shippingDetails->getCountryName() }}</td>
                </tr>
            </table>
        </td>
        @if($shippingDetails->getAddress() !== $billingDetails->getAddress())
            <td>
                <table class="billing-details-block">
                    <tr>
                        <td colspan="2"><h3>{{ __('Dirección de facturación') }}</h3></td>
                    </tr>
                    <tr>
                        <td><strong>{{ __('Nombre') }}:</strong></td>
                        <td>{{ $billingDetails->getPersonName() !== '' ? $billingDetails->getPersonName() : $clientDetails->getName() }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __('Apellidos') }}:</strong></td>
                        <td>{{ $billingDetails->getPersonSurname() !== '' ? $billingDetails->getPersonSurname() : $clientDetails->getSurname()}}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __('NIF/CIF/NIE') }}:</strong></td>
                        <td>{{ $billingDetails->getPersonNif() !== '' ? $billingDetails->getPersonNif() : '' }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __('Teléfono') }}:</strong></td>
                        <td>{{ $billingDetails->getPersonPhone() !== '' ? $billingDetails->getPersonPhone() : '' }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __('Dirección') }}:</strong></td>
                        <td>{{ $billingDetails->getAddress() }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __('Localidad') }}:</strong></td>
                        <td>{{ $billingDetails->getLocation() }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __('Código postal') }}:</strong></td>
                        <td>{{ $billingDetails->getPostalCode() }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __('Provincia') }}:</strong></td>
                        <td>{{ $billingDetails->getStateName() }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __('País') }}:</strong></td>
                        <td>{{ $billingDetails->getCountryName() }}</td>
                    </tr>
                </table>
            </td>
        @endif
    </tr>
</table>

<h2 class="pt-4">{{ __('Productos') }}</h2>
<table class="products products-block my-orders-products">
    <tr class="table-header">
        <th style="text-align: left;">{{ __('Referencia') }}</th>
        <th style="text-align: left;">{{ __('Producto') }}</th>
        <th class="text-right">{{ __('Cantidad') }}</th>
        <th class="text-right">{{ __('Precio ud.') }}</th>
        <th class="text-right">{{ __('Total') }}</th>
    </tr>
    @foreach($products as $product)
        <tr>
            <td style="text-align: left;">{{ $product->getReference() }}</td>
            <td style="text-align: left;">{!! $product->getName() !!}</td>
            <td class="text-right">{{ $product->getQuantity() }}</td>
            <td class="text-right">{{ $product->getPaidUnitPrice()->toString() }}</td>
            <td class="text-right">{{ $product->getTotal()->toString() }}</td>
        </tr>
    @endforeach
    @foreach($giftedProducts as $giftedProduct)
        <tr>
            <td style="text-align: left;">{{ $giftedProduct->getReference() }}</td>
            <td style="text-align: left;">{!! $giftedProduct->getName() !!}</td>
            <td class="text-right">{{ $giftedProduct->getQuantity() }}</td>
            <td class="text-right"></td>
            <td class="text-right">{{ __('¡Gratis!') }}</td>
        </tr>
    @endforeach
    @foreach($order->getCoupons() as $coupon)
        <tr>
            <td>Cupón descuento "{{ $coupon->code }}"</td>
            <td></td>
            <td></td>
            <td></td>
            <td class="text-right">{{ $coupon->discount }}</td>
        </tr>
    @endforeach
    <tr class="totals-row first-totals-row">
        <td colspan="4" style="padding-top: 2rem;">{{ __('Subtotal') }}</td>
        <td style="padding-top: 2rem;" class="text-right">{{ \Bittacora\Bpanel4\Prices\Types\Price::fromInt($order->getSubtotalWithoutTaxes()) }}</td>
    </tr>
    <tr class="totals-row">
        <td colspan="4" >{{ __('Gastos de envío') }}</td>
        <td class="text-right">{{ \Bittacora\Bpanel4\Prices\Types\Price::fromInt($order->getShippingCosts()) }}</td>
    </tr>
    @if (null === $order->getVatNumber() && !empty($taxBreakdown))
        @foreach($taxBreakdown as $breakdown)
            <tr class="totals-row">
                <td colspan="4" >{{ $breakdown->getName() }}% de IVA de {{ $breakdown->getTotalWithoutTaxes() }}
                </td>
                <td class="text-right">{{ number_format($breakdown->getAppliedTaxes(), 2, ',', '.') }} €</td>
            </tr>
        @endforeach
    @endif
    <tr class="totals-row total">
        <td colspan="4">{{ __('Total') }}</td>
        <td class="text-right">{{ \Bittacora\Bpanel4\Prices\Types\Price::fromInt($order->getOrderTotal()) }}</td>
    </tr>
</table>

@if($order->getCoupons()->count() > 0)
    <div class="text-gray text-center mt-4">
    <em><small><strong>Nota:</strong> El importe de los cupones descuento se aplica proporcionalmente a todos los productos del pedido, de
        forma que tras aplicar impuestos el descuento total sea el indicado en el cupón.</small></em>
    </div>
@endif

@if (null !== $order->getPaymentMethodInstructions())
    <table>
        <tr>
            <td>
                <table style="">
                    <tr>
                        <td colspan="2"><h3>{{ __('Instrucciones para realizar el pago') }}</h3></td>
                    </tr>
                    <tr>
                        <td>{{ $order->getPaymentMethodInstructions() }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endif

@if (null !== $order->getOrderNotes())
    <table>
        <tr>
            <td>
                <table style="width: 95%;">
                    <tr>
                        <td colspan="2"><h3>{{ __('Observaciones') }}</h3></td>
                    </tr>
                    <tr>
                        <td>{{ $order->getOrderNotes() }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endif
