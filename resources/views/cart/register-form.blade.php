<?php /** @var \Bittacora\Bpanel4\Clients\Models\Client $client */ ?>
@extends('bpanel4-public.layouts.regular-page')
@livewireStyles
@section('content')
    <div class="main-container regular-page-container bgc-transparent">
        <div class="main-content minh-100 justify-content-center">
            <p>Para continuar con su pedido, necesitamos los siguientes datos. Por favor, rellene el formulario y haga click en continuar.</p>
            @if($errors->any())
                {!! implode('', $errors->all('<div>:message</div>')) !!}
            @endif
            <form method="POST" action="{{ route('cart.register') }}" class="registration-form cart-registration-form">
                @csrf

                <fieldset class="d-flex flex-wrap mt-3 pb-4 w-100">
                    <legend><i class="fa-fw fas fa-user"></i> <strong>Datos personales</strong></legend>
                    <div class="d-flex w-100">
                        <div class="col-md-6 pb-4 px-0 pr-4">
                            @livewire('form::input-text', ['fieldWidth' => 9, 'name' => 'name', 'labelText' =>
                            __('bpanel4-clients::client.name'),
                            'required'=>true, 'value' => old('name')])
                        </div>
                        <div class="col-md-6 pb-4 px-0">
                            @livewire('form::input-text', ['fieldWidth' => 9, 'name' => 'surname', 'labelText' =>
                            __('bpanel4-clients::client.surname'), 'required'=>true, old('surname')])
                        </div>
                    </div>
                    <div class="d-flex w-100">
                        <div class="col-md-6 pb-4 px-0 pr-4">
                            @livewire('form::input-text', ['fieldWidth' => 9, 'name' => 'dni', 'labelText' =>
                            __('bpanel4-clients::client.dni'),
                            'required'=>true, old('dni')])
                        </div>
                        <div class="col-md-6 pb-4 px-0">
                            @livewire('form::input-text', ['fieldWidth' => 9, 'name' => 'company', 'labelText' =>
                            __('bpanel4-clients::client.company-name-label'), 'required'=>false, old('company')])
                        </div>
                    </div>
                    <div class="d-flex w-100">
                        <div class="col-md-6 pb-4 px-0 pr-4">
                            @livewire('form::input-email', ['fieldWidth' => 9, 'name' => 'email', 'labelText' =>
                            __('bpanel4-clients::client.email'), 'required'=>true, old('email')])
                        </div>
                        <div class="col-md-6 pb-4 px-0">
                            @livewire('form::input-text', ['fieldWidth' => 9, 'name' => 'phone', 'labelText' =>
                            __('bpanel4-clients::client.phone'), 'required'=>true, old('phone')])
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend><i class="fa-fw fas fa-location-circle"></i> <strong>Dirección</strong></legend>
                    <div class="d-flex w-100">
                        <div class="col-md-12 pb-4 px-0 address-name">
                            @livewire('form::input-text', ['labelWidth' => 5, 'fieldWidth' => 7 , 'name' =>
                            'address[name]', 'labelText' =>
                            __('bpanel4-addresses::address.name'), 'required'=>true, 'value' => old('address.name'),
                            'placeholder' => __('bpanel4-addresses::address.name-placeholder')])
                        </div>
                    </div>
                    <div class="d-flex w-100">
                    {{-- Nombre de la persona de contacto --}}
                    <div class="col-md-6 pb-4 px-0 pr-4">
                        @livewire('form::input-text', [
                        'fieldWidth' => 9 ,
                        'name' =>  'address[person_name]',
                        'labelText' => __('bpanel4-addresses::address.person_name'),
                        'required'=>true,
                        'value' => old('person_name'),
                        'placeholder' => __('bpanel4-addresses::address.person_name_placeholder')
                        ], key('person_name-'. Str::random(10)))
                    </div>
                    {{-- Apellidos de la persona de contacto --}}
                    <div class="col-md-6 pb-4 px-0">
                        @livewire('form::input-text', [
                        'fieldWidth' => 9 ,
                        'name' => 'address[person_surname]',
                        'labelText' => __('bpanel4-addresses::address.person_surname'),
                        'required'=>true,
                        'value' => old('person_surname'),
                        'placeholder' => __('bpanel4-addresses::address.person_surname_placeholder')
                        ], key('person_surname-'. Str::random(10)))
                    </div>
                    </div>
                    <div class="d-flex w-100">
                        <div class="col-md-6 pb-4 px-0 pr-4 d-flex w-100 align-items-center country-field">
                            <div class="col-sm-2 col-form-label text-sm-right">
                                <label for="id-form-field-1" class="mb-0  ">
                                    <span class="text-danger">*</span> País
                                </label>
                            </div>
                            <div class="col-sm-10">
                                <div class="input-group m-b">
                                    @livewire('country-select', ['selectedCountry' => old('country') ?? config('bpanel4-clients.default_country_id', 0), 'required' => true])
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pb-4 px-0 d-flex form-group align-items-center">
                            <div class="col-sm-3 col-form-label text-sm-right">
                                <label for="id-form-field-1" class="mb-0  ">
                                    <span class="text-danger">*</span> Provincia
                                </label>
                            </div>
                            <div class="col-sm-9 ">
                                <div class="input-group m-b">
                                    @livewire('state-select', ['selectedCountry' => old('country') ?? config('bpanel4-clients.default_country_id', 0), 'selectedState' => old('state') ?? 0, 'required' => true])
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex w-100">
                        <div class="col-md-6 pb-4 px-0 pr-4 ">
                            @livewire('form::input-text', ['labelWidth' => 3, 'fieldWidth' => 9, 'name' => 'address[location]', 'labelText' =>
                            __('bpanel4-addresses::address.location'), 'required'=>true, 'value' => old('address.location')])
                        </div>
                        <div class="col-md-6 pb-4 pl-3 pr-0">
                            @livewire('form::input-text', ['labelWidth' => 4, 'fieldWidth' => 8, 'name' => 'address[postal_code]', 'labelText' =>
                            __('bpanel4-addresses::address.postal-code'), 'required'=>true, 'value' => old('address.postal_code')])
                        </div>
                    </div>
                    <div class="d-flex w-100 address-address">
                        <div class="col-md-12 pb-4 px-0">
                            @livewire('form::input-text', ['labelWidth' => 2, 'fieldWidth' => 10, 'name' => 'address[address]', 'labelText' =>
                            __('bpanel4-addresses::address.address'), 'required'=>true, 'value' =>
                            old('address.address') ])
                        </div>
                    </div>
                </fieldset>
                @livewire('form::input-hidden', ['name' => 'password', 'value' => $password])
                @livewire('form::input-hidden', ['name' => 'password_confirmation', 'value' => $password])

                <div class="registration-legal-checkbox">
                    @livewire('form::input-checkbox', [
                    'name' => 'accept-policies',
                    'value' => 1,
                    'checked' => false,
                    'labelText' => __('bpanel4-clients::my-account.accept-privacy-policy', [
                    'privacy-policy-link' => config('bpanel4-clients.privacy-policy-page'),
                    'basic-data-protection-link' => config('bpanel4-clients.basic-data-protection-link')
                    ]),
                    'required' => true,
                    'bpanelForm' => true]
                    )
                    @include(config('bpanel4-clients.data-protection-view'))
                </div>
                @if(config('captcha.secret'))
                    <div class="mt-3 d-flex align-items-end flex-column">
                        {!! NoCaptcha::renderJs() !!}
                        {!! NoCaptcha::display() !!}
                        @if ($errors->has('g-recaptcha-response'))
                            <div class="text-danger mt-3">
                                <strong>{{ __('El captcha es obligatorio') }}</strong>
                            </div>
                        @endif
                    </div>
                @endif
                <input type="hidden" name="client_type" value="particular" id="client_type">
                <div></div>
                <div class="d-flex justify-content-end mt-4">
                    <button class="btn btn-primary">
                        {{ __('bpanel4-clients::client.continue'), }}
                    </button>
                </div>
            </form>

        </div>
        <script defer>
          // Autocompleto nombre de la dirección con el del cliente por comodidad, pero permito que sean distintos
          const nameField = document.getElementById('name');
          const surnameField = document.getElementById('surname');
          const personNameField = document.querySelector('[name="address\[person_name\]"]');
          const personSurameField = document.querySelector('[name="address\[person_surname\]"]');

          function bindFields(mainField, secondaryField) {
            mainField.addEventListener('change', function () {
              secondaryField.value = mainField.value;
            });
          }

          bindFields(nameField, personNameField);
          bindFields(surnameField, personSurameField);
        </script>
    </div>
@endsection
