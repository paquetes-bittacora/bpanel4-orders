<div>
    <a href="{{ route('cart.show') }}" title="Ir al carrito">
        <div class="cart-icon">
            <i class="far fa-shopping-cart"></i>
            <div class="quantity">
                {{ $productCount }}
            </div>
        </div>
    </a>
    <script>
      window.addEventListener('cart-updated', event => {
        toastr.success('Carrito actualizado');
      })
    </script>
</div>
