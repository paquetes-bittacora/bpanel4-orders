@extends('bpanel4-public.layouts.regular-page')

@section('content')
    <div class="regular-page-container">
        <div class="regular-page-container">
            @livewire('bpanel4-orders::cart', [
                'shippingOption' => $shippingOption,
                'shippingAddress' => $shippingAddress,
                'billingAddress' => $billingAddress,
                'isPreview' => true
            ])
        </div>
    </div>
@endsection
