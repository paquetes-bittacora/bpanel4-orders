@extends('bpanel4-public.layouts.regular-page')

@section('content')
    <div class="regular-page-container">
        <form method="POST" action="{{ route('cart.preview') }}">
            @csrf
            @livewire('bpanel4-orders::cart-addresses', ['client' => $client])
            @livewire('bpanel4-orders::select-shipping', ['client' => $client, 'cart' => $cart])
        </form>
    </div>
@endsection
