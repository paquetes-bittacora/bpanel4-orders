@extends('bpanel4-public.layouts.regular-page')

@section('meta-title') {{ __('Carrito') }} @endsection
@section('meta-description'){{ __('Carrito de :name', ['name' => config('app.name')]) }}@endsection
@section('meta-keywords'){{ __('carrito') }}@endsection

@section('content')
    <div class="regular-page-container bootstrap">
        @livewire('bpanel4-orders::cart')
    </div>
@endsection
