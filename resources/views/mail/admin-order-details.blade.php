@extends('bpanel4-public.mail.base-mail')
<?php /**
 * @var \Bittacora\Bpanel4\Orders\Models\Order\OrderClientDetail $client
 * @var \Bittacora\Bpanel4\Orders\Models\Order\OrderProduct[] $products
 * @var \Bittacora\Bpanel4\Orders\Models\Order\Order $order
 */ ?>
@section('preheader')@endsection
@section('content')
    <br/>
    <p>Se ha recibido un nuevo pedido en {{ $shopName }} (esperando confirmación de pago)</p>
    @include('bpanel4-orders::mail.order-details')
@endsection
