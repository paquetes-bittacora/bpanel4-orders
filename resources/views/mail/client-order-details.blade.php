@extends('bpanel4-public.mail.base-mail')
<?php /**
 * @var \Bittacora\Bpanel4\Orders\Models\Order\OrderClientDetail $client
 * @var \Bittacora\Bpanel4\Orders\Models\Order\OrderProduct[] $products
 * @var \Bittacora\Bpanel4\Orders\Models\Order\Order $order
 */ ?>
@section('preheader')@endsection
@section('content')
    <p>¡Gracias por su pedido! A continuación tiene los detalles del mismo. También podrá ver sus pedidos desde el
    apartado "Mi cuenta" de la web.</p>
    <p>Empezaremos a procesar su pedido una vez hayamos recibido la confirmación de pago del mismo.</p>
    @include('bpanel4-orders::mail.order-details')
@endsection
