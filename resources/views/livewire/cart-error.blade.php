<div>
    @php
        $messages = [];

        if ($isShippingAddressError) {
           $messages[] = 'de envío';
        }

        if ($isBillingAddressError) {
           $messages[] = 'de facturación';
        }

        $message = implode(' y ', $messages);
    @endphp
    <div class="alert alert-danger mt-5">
        @if ($cartError === 'address-error')
            Su dirección {{ $message }} está incompleta o es incorrecta, por favor, diríjase al apartado <a href="{{ route('my-account.edit-address', ['address' => $cartErrorAddress]) }}">"Mis direcciones"</a> y corríjala.
        @endif
    </div>
</div>
