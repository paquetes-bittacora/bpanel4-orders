<div>
    <a href="#" class="change-quantity-button @if($quantity <= 1) disabled @endif" wire:click="decreaseQuantity" wire:key="decrease-qty-{{ $productId }}"><i class="far fa-minus"></i></a>
    {{ $quantity }}
    <a href="#" class="change-quantity-button" wire:click="increaseQuantity" wire:key="increase-qty-{{ $productId }}"><i class="far fa-plus"></i></a>
</div>
