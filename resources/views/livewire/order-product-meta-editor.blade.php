@php /** @var \Bittacora\Bpanel4\Orders\Models\Order\OrderProduct $orderProduct */ @endphp
<div>
    <div class="mb-2 mt-1">
        @if (session()->has('alert-success-order-product'))
            <div class="alert alert-success alert-dismissible fade show">
                {{ session('alert-success-order-product') }}
            </div>
        @endif
        @if($status === 'saved')
            @if(count($meta) > 0)
                <div class="mb-2">
                    @foreach($meta as $field)
                        <span class="mr-2"><strong>{{ $field['name'] }}:</strong> {{ $field['value'] }}</span>
                    @endforeach
                </div>
            @endif
            @if($enableEdit)
                <button class="btn btn-secondary btn-outline-grey btn-xs" wire:click="$set('status', 'editing')">
                    <i class="fas fa-pencil-alt fa-fw"></i> Editar detalles adicionales del producto
                </button>
            @endif
        @endif
        @if($status === 'editing')
            <div class="ccard h-100 flex-column px-4 py-3 mb-3">
                @foreach($meta as $key => $field)
                    <div class="order-product-meta-row d-flex">
                        <div class="mr-3 form-group form-row">
                            <label for="field-name-{{ $key }}" class="font-bold">Campo:</label>
                            <input id="field-name-{{ $key }}" wire:model="meta.{{$key}}.name" class="form-control">
                        </div>
                        <div class="mr-3 form-group form-row">
                            <label for="field-value-{{ $key }}" class="font-bold">Valor:</label>
                            <input id="field-value-{{ $key }}" wire:model="meta.{{$key}}.value" class="form-control">
                        </div>
                        <div>
                            <label>&nbsp;</label>
                            <div>
                                <button class="btn btn-danger" wire:click="removeField({{$key}})">
                                    <i class="fas fa-trash fa-fw"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="d-flex justify-content-between">
                    <button class="btn btn-info btn-xs" wire:click="addField()">
                        <i class="fas fa-plus fa-fw"></i> Añadir campo
                    </button>
                    <button class="btn btn-success btn-xs" wire:click="saveOrderProductMeta()">
                        <i class="fas fa-save fa-fw"></i> Guardar
                    </button>
                </div>
            </div>
        @endif
</div>
    @if (session()->has('alert-success-order-product'))
        <script>
            $(".alert").delay(4000).fadeOut(200, function() {
              $(this).alert('close');
            });
        </script>
    @endif
</div>