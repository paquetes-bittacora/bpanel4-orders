<?php /** * @var \Bittacora\Bpanel4\Shipping\Types\ShippingOption[] $options */ ?>
<div class="mt-4">
    <h2>{{ __('Métodos de envío') }}</h2>
    @if($this->noShippingOptionsAvailable || count($options) === 0)
        <div class="alert alert-danger">
            <p>{{ __('Lo sentimos, pero no hay ningún método de envío disponible para la dirección de envío seleccionada. Pruebe con otra dirección.') }}</p>
            <p style="margin-bottom: 0;">{{ __('Tenga en cuenta que algunos métodos de envío solo están disponibles a partir de cierto importe.') }}</p>
        </div>
    @else
        <div class="shipping-method-cart-options">
            @foreach($options as $option)
                <div class="shipping-method @if($option->getKey() === $selectedShippingMethod) selected @endif {{ $option->getKey() }}"
                     wire:click="setSelectedOption('{{ $option->getKey() }}')">
                    {{ $option->shippingMethod->getName() }}
                </div>
            @endforeach
        </div>
        <input type="hidden" name="selected_shipping_option" value="{{ $selectedShippingMethod }}">
    @endif
    @if($selectedShippingMethod !== '' and $selectedShippingMethod !== 'null-shipping-option')
        <div class="d-flex justify-content-end">
            <button class="btn btn-primary">{{ __('Continuar') }}</button>
        </div>
    @endif
</div>
