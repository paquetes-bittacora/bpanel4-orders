<?php /** @var \Bittacora\Bpanel4\Clients\Contracts\Client $client */ ?>
<div>
    <h2>{{ __('Dirección de envío') }}</h2>
    <select wire:model="selectedShippingAddress" name="selected_shipping_address" wire:change="updateCartShippingAddress" class="select-shipping">
        <option value="0">{{ __('Enviar este pedido a una dirección nueva') }}</option>
        @foreach($client->addresses()->get() as $address)
            <option
                    value="{{ $address->getId() }}"
                    @if ($selectedShippingAddress === $address->getId())
                        selected="selected"
                    @endif
            >{{ $address->getName() }}</option>
        @endforeach
    </select>
    @if ($selectedShippingAddress === 0)
        <h3 class="mt-4"><i class="fas fa-map-marker-alt"></i> {{ __('Nueva dirección de envío') }}</h3>
        <form id="new-shipping-address"  onsubmit="return window.dispatchEvent(new Event('submit-shipping-address-form')) && false">
            <div class="new-cart-address">
            @include('bpanel4-addresses::public.edit-address-fields', [
                'model' => new \Bittacora\Bpanel4\Addresses\Models\ModelAddress(),
            ])
            </div>
            <div class="d-flex justify-content-end">
                <button class="btn btn-primary mb-4">{{ __('Guardar') }}</button>
            </div>
        </form>
    @endif
    <label><input type="checkbox" wire:model="billingAddressIsTheSameAsShipping"> <span>{{__('La dirección de facturación es la misma que de envío')}}</span></label>
    @if (!$billingAddressIsTheSameAsShipping)
        <h2 class="mt-4"><i class="far fa-receipt"></i> {{ __('Dirección de facturación') }}</h2>
        <select wire:model="selectedBillingAddress" name="selected_billing_address" class="select-shipping">
            <option value="0">Facturar este pedido a una dirección nueva</option>
            @foreach($client->addresses()->get() as $address)
                <option
                        value="{{ $address->getId() }}"
                        @if ($selectedBillingAddress === $address->getId())
                            selected="selected"
                        @endif
                >{{ $address->getName() }}</option>
            @endforeach
        </select>
        @if($selectedBillingAddress === 0)
            <h3 class="mt-4">{{ __('Nueva dirección de facturación') }}</h3>
            <form id="new-billing-address"  onsubmit="return window.dispatchEvent(new Event('submit-billing-address-form')) && false">
                <div class="new-cart-address">
                @include('bpanel4-addresses::public.edit-address-fields', [
                    'model' => new \Bittacora\Bpanel4\Addresses\Models\ModelAddress(),
                ])
                </div>
                <div class="d-flex justify-content-end">
                    <button class="btn btn-primary mb-4">{{ __('Guardar') }}</button>
                </div>
            </form>
        @endif
    @else
        {{-- Si la dirección de envío y facturación son la misma, indico la dirección de facturación en un campo hidden --}}
        <input type="hidden" name="selected_billing_address" value="{{$selectedShippingAddress}}">
    @endif
    <script>
      function submitForm(elementId, method) {
        const form = document.getElementById(elementId);
        const formData = new FormData(form);
        this.Livewire.emit(method, JSON.stringify(Object.fromEntries(formData)));
        return false;
      }

      /*
       * No puedo usar componentes anidados para livewire tal y como tengo planteado ahora el módulo de direcciones,
       * así que envío así los datos de vuelta al componente para poder procesar el formulario.
       */
      window.addEventListener('submit-shipping-address-form', function(event) {
        event.stopImmediatePropagation();
        return submitForm('new-shipping-address', 'processShippingAddressForm');
      });

      window.addEventListener('submit-billing-address-form', function(event) {
        event.stopImmediatePropagation();
        return submitForm('new-billing-address', 'processBillingAddressForm');
      });

      window.addEventListener('address-saved', event => {
        toastr.success('La dirección se creo correctamente');
      });
      window.addEventListener('error-saving-address', event => {
        toastr.error('Error al crear la dirección');
      });
    </script>
</div>
