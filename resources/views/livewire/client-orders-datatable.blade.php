<?php /** @var \Bittacora\Bpanel4\Orders\Models\Order\Order $row */ ?>
<td class="order-number">{{ $row->getId() }}</td>
<td class="text-center order-total">{{ \Bittacora\Bpanel4\Prices\Types\Price::fromInt($row->getOrderTotal()) }}</td>
<td class="text-center order-date">{{ $row->getDate()->format('d-m-Y H:i:s') }}</td>
<td class="order-list-status"><div style="color: {{ $row->getStatus()->color }};background: {{ $row->getStatus()->background_color }}">{{ $row->getStatus()->name }}</div></td>
<td class="text-center">
    <a href="{{ route('my-account.view-order', ['order' => $row->getId()]) }}"><i class="far fa-eye"></i> Ver</a>
</td>
