<?php /**
 * @var \Bittacora\Bpanel4\Orders\Models\Cart\Cart $cart
 * @var \Bittacora\Bpanel4\Payment\Contracts\PaymentMethod[] $paymentMethods
 */ ?>

<div>
    @if ($cart->getProducts()->isEmpty())
        <div class="cart-is-empty">{{ __('Todavía no ha añadido ningún producto al carrito.') }}</div>
    @else
        <div class="cart-products">
            <div class="row heading">
                <div class="delete-column">{{-- Columna eliminar --}}</div>
                <div>{{-- Columna de las imágenes --}}</div>
                <div class="name">{{ __('Nombre') }}</div>
                <div>{{ __('Precio') }} <i class="far fa-info-circle" title="Precio sin IVA"></i></div>
                <div>{{ __('Cantidad') }}</div>
                <div>{{ __('Total prod.') }} <i class="far fa-info-circle" title="Total sin IVA"></i></div>
            </div>
            @foreach($cart->getProducts() as $product)
                <div class="row">
                    <div class="remove delete-column">
                        <a
                                onclick="confirm('¿Seguro que desea quitar el producto?') || event.stopImmediatePropagation()"
                                href="{{ route('cart.remove-product', ['cartRow' => $product->getCartableProductId()]) }}"
                        >
                            <i class="fas fa-times-circle"></i>
                        </a>
                    </div>
                    <div class="image">
                        @if (isset($product->getFeaturedImages()[0]))
                            <img src="{{ $product->getFeaturedImages()[0]->getUrl('cart-product-thumb') }}"
                                 alt="{{ $product->getName() }}" class="cart-product-thumb"/>
                        @else
                            <img src="/assets/img/product-placeholder.png" alt="" class="cart-product-thumb">
                        @endif
                    </div>
                    <div class="name">
                        @if(method_exists($product, 'getCartProductLink'))
                            <a href="{{ $product->getCartProductLink() }}">
                                {!! $product->getName() !!}
                            </a>
                        @else
                            <a href="{{ route('bpanel4-products-public.show', ['product' => $product->getProductId()]) }}">
                                {!! $product->getName() !!}
                            </a>
                        @endif

                        @if(null !== $product->getReference())
                            <div class="cart-reference">Ref: {{ $product->getReference() }}</div>
                        @endif</div>
                    <div class="unit-price">
                        {{ $product->getUnitPrice() }}
                        @if($product->getOriginalUnitPrice() > $product->getUnitPrice())
                            <div class="original-price">
                                {{ $product->getOriginalUnitPrice() }}
                            </div>
                        @endif
                    </div>
                    <div class="quantity">
                        @livewire('bpanel4-orders::product-quantity', [
                        'productId' => $product->getId(),
                        'quantity' => $product->getQuantity(),
                        ], key('product-quantity-' . $product->getId()))
                    </div>
                    <div>
                        {{ $product->getTotal() }}
                    </div>
                </div>
            @endforeach

            @foreach($cart->getGiftedProducts() as $product)
                <div class="row">
                    <div class="remove delete-column"></div>
                    <div class="image">
                        @if (isset($product->getFeaturedImages()[0]))
                            <img src="{{ $product->getFeaturedImages()[0]->getUrl('cart-product-thumb') }}"
                                 alt="{{ $product->getName() }}" class="cart-product-thumb"/>
                        @else
                            <img src="/assets/img/product-placeholder.png" alt="" class="cart-product-thumb">
                        @endif
                    </div>
                    <div class="name">
                        @if(method_exists($product, 'getCartProductLink'))
                            <a href="{{ $product->getCartProductLink() }}">
                                {!! $product->getName() !!}
                            </a>
                        @else
                            <a href="{{ route('bpanel4-products-public.show', ['product' => $product->getProductId()]) }}">
                                {!! $product->getName() !!}
                            </a>
                        @endif

                        @if(null !== $product->getReference())
                            <div class="cart-reference">Ref: {{ $product->getReference() }}</div>
                        @endif</div>
                    <div class="unit-price">
                        @if($product->getOriginalUnitPrice())
                            <div class="original-price">
                                {{ $product->getOriginalUnitPrice() }}
                            </div>
                        @endif
                    </div>
                    <div class="quantity">
                        @livewire('bpanel4-orders::product-quantity', [
                        'productId' => $product->getId(),
                        'quantity' => $product->getQuantity(),
                        ], key('product-quantity-' . $product->getId()))
                    </div>
                    <div>
                        <div class="gifted-product-price">{{ __('¡Gratis!') }}</div>
                        <div class="original-price">
                            {{ $product->getTotal() }}
                        </div>
                    </div>
                </div>
            @endforeach
            {{-- Cupones descuento --}}
            @foreach($cart->getCoupons() as $coupon)
                <div class="row">
                    <div class="remove">
                        <div class="remove">
                            <a
                                    onclick="confirm('¿Seguro que desea quitar el descuento?') || event.stopImmediatePropagation()"
                                    href="{{ route('bpanel4-coupons.public.remove-coupon-from-cart', ['coupon' => $coupon]) }}"
                            >
                                <i class="fas fa-times-circle"></i>
                            </a>
                        </div>
                    </div>
                    <div class="image"></div>
                    <div class="name">
                        {{ $coupon->getName() }}
                        <div class="cart-reference">
                            Código: {{ $coupon->getCode() }}
                        </div>
                    </div>
                    <div class="unit-price">
                    </div>
                    <div class="quantity"></div>
                    <div>
                        {{ $couponDiscountCalculator->calculateDiscountAsString($coupon, $cart) }}
                    </div>
                </div>
            @endforeach
            {{-- / Cupones descuento --}}
        </div>
        <div class="cart-summary mb-4">
            <div class="row">
                <div class="title">
                    {{ __('Subtotal') }}
                </div>
                <div class="amount">
                    {{ $subtotal }}
                </div>
            </div>
            {{--            <div class="row">--}}
            {{--                <div class="title">--}}
            {{--                    {{ __('Subtotal') }}--}}
            {{--                </div>--}}
            {{--                <div class="amount">--}}
            {{--                    {{ $subtotalWithTaxes }}--}}
            {{--                </div>--}}
            {{--            </div>--}}
            @if(!isset($isPreview) || $isPreview === false)
                {{-- Todavía no se ha seleccionado dirección ni método de envío --}}
                <div class="row">
                    <div class="title">
                        {{ __('Gastos de envío') }} <i class="fas fa-info-circle"
                                                       title="{{ __('Calculados para su dirección de envío. Tendrá que confirmar la dirección de envío en el siguiente paso.') }}"></i>
                    </div>
                    <div class="amount">
                        @if (null === $shippingCosts)
                            -
                        @elseif ( $shippingCosts->toInt() > 0)
                            {{ $shippingCosts }}
                        @elseif ($shippingCosts->toInt() === 0)
                            ¡Gratis!
                        @endif
                    </div>
                </div>
            @else
                {{-- Gastos de envío finales --}}
                <div class="row">
                    <div class="title">
                        {{ __('Gastos de envío') }}<br>
                        <small>{{ $shippingMethodName }}</small>
                    </div>
                    <div class="amount">
                        @if($shippingCostsAmount === null)
                            -
                        @elseif($shippingCostsAmount->toInt() > 0)
                            {{ $shippingCostsAmount }}
                        @elseif ($shippingCostsAmount->toInt() === 0)
                            {{ __('¡Gratis!') }}
                        @endif
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="title">
                    {{ __('IVA') }}
                </div>
                <div class="amount">
                    {{ $totalTaxes->toString() }}
                </div>
            </div>
            <div class="row">
                <div class="title">
                    {{ __('Total') }}
                </div>
                <div class="amount">
                    {{ $cartTotal }}
                </div>
            </div>
        </div>
        <div id="loading-indicator" class="cart-loading-indicator">
            {{ __('Actualizando totales...') }}
        </div>
        @if(!isset($isPreview) || $isPreview === false)
            @if($clientIsInTheEU && !config('bpanel4.disable_intracomunitary_vat'))
                @if($cart->getVatNumber() != '')
                    <div class="d-flex justify-content-end">
                        <div class="d-flex align-items-center">
                            <div><strong>{{ __('Nº IVA intracomunitario (opcional)') }}</strong></div>
                            <div>
                                <form method="get" action="{{ route('bpanel4-intracomunitary-vat.remove') }}" class="d-flex">
                                    <input readonly class="form-control mx-4 text-center align-self-stretch uppercase" value="{{$cart->getVatNumber()}}" style="max-width: 200px;">
                                    <button class="btn btn-secondary" type="submit">Quitar</button>
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                @else
                    {{-- Campo para iva intracomunitario --}}
                    <div class="d-flex justify-content-end">
                        <div class="d-flex align-items-center">
                            <div><strong>{{ __('Nº IVA intracomunitario (opcional)') }}</strong></div>
                            <div class="d-flex">
                                <form method="post" action="{{ route('bpanel4-intracomunitary-vat.apply') }}" class="d-flex">
                                    <input name="vat_number" class="form-control mx-4 text-center align-self-stretch uppercase" style="max-width: 200px;">
                                    <button class="btn btn-secondary" type="submit">Enviar</button>
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
            {{-- Formulario cupones descuento --}}
            <div class="my-4">
                <form method="post" action="{{ route('bpanel4-coupons.public.apply') }}">
                    <div class="d-flex align-items-center justify-content-end ">
                        <strong>{{ __('¿Tienes un cupón descuento? Introdúcelo aquí') }}</strong>
                        <input name="code" class="form-control mx-4 text-center align-self-stretch uppercase"
                               style="max-width: 150px;">
                        <button class="btn btn-secondary" type="submit">Enviar</button>
                    </div>
                    @csrf
                </form>
            </div>
            {{-- / Formulario cupones descuento --}}
            @guest
                <div class="d-flex justify-content-end">
                    <div class="d-flex align-items-center cart-login-register-row">
                        <div class="p-2">¿Tiene cuenta en {{ config('app.name') }}?</div>
                        <a href="{{ route('client-login') }}">
                            <button class="btn btn-primary">
                                <i class="fas fa-sign-in-alt"></i> inicie sesión
                            </button>
                        </a>
                        <div class="p-2">o</div>
                        @if(false !== config('orders.enable-guest-orders'))
                            <a href="{{ route('cart.show-registration') }}">
                                <button class="btn btn-primary"><i class="fas fa-user"></i> continuar como invitado</button>
                            </a>
                        @else
                            <a href="{{ route('register') }}">
                                <button class="btn btn-primary"><i class="fas fa-user"></i> regístrese
                                </button>
                            </a>
                        @endif                        
                    </div>
                </div>
            @endguest
            @auth
                {{-- Todavía no se ha seleccionado dirección ni método de envío --}}
                <div class="d-flex justify-content-end">
                    <a href="{{ route('cart.select-shipping') }}">
                        <button class="btn btn-primary">
                            {{ __('Seleccionar envío y forma de pago') }}
                        </button>
                    </a>
                </div>
            @endauth
        @else
            <div class="d-flex row mb-4">
                <div class="col-4">
                    <h3>{{ __('Dirección de envío') }}</h3>
                    <x-bpanel4-address :address="$cart->getShippingAddress()"/>
                </div>
                <div class="col-4">
                    <h3>{{ __('Dirección de facturación') }}</h3>
                    <x-bpanel4-address :address="$cart->getBillingAddress()"/>
                </div>
                <div class="col-4">
                    <h3>{{ __('Método de envío') }}</h3>
                    <div>
                        {{ $shippingMethodName }}
                    </div>
                </div>
            </div>
            <div class="mb-3">
                <h3>{{ __('Formas de pago') }}</h3>
                <div class="payment-method-cart-options">
                    @forelse($paymentMethods as $key => $paymentMethod)
                        <div class="payment-method @if($key === $selectedPaymentMethod) selected @endif"
                             wire:click="setSelectedPaymentMethod({{$key}})">
                            {{ $paymentMethod->getName() }}
                        </div>
                    @empty
                        <p>{{ __('No se encontró ningún método de pago.') }}</p>
                    @endforelse
                </div>
                @if (null !== $paymentMethodInstructions)
                    <div class="payment-method-instructions pt-3">
                        <i class="fal fa-info-circle"></i> {{ $paymentMethodInstructions }}
                    </div>
                @endif
            </div>
            <div>
                <h3>{{ __('Observaciones') }}</h3>
                <textarea wire:model.debounce.500ms="orderNotes" name="order_notes" class="form-control mb-3 p-2" rows="2" placeholder="Si desea dejarnos un comentario acerca de su pedido, por favor, escríbalo aquí."></textarea>
            </div>
            <div class="d-flex justify-content-end">
                @if($cartTotal->toInt() > 0)
                    {{-- En el onclick evito que se reenvíe el pedido si se hace doble click --}}
                    <a href="{{ route('order.confirm') }}" onClick="this.style.pointerEvents = 'none';">                    
                        <button class="btn btn-primary">
                            <i class="far fa-shopping-cart"></i> {{ __('Confirmar pedido') }}
                        </button>
                    </a>
                @else
                    <div class="alert-danger">
                        {{ __('No se puede completar el pedido porque el importe es 0. Por favor, póngase en contacto con
                         nosotros.') }}
                    </div>
                @endif
            </div>
        @endif
    @endif
    <script>
      window.addEventListener('cart-updated', event => {
        document.getElementById('loading-indicator').style.display = 'block';
      })
    </script>
</div>
