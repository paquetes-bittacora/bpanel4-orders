<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Database\Factories;

use Bittacora\Bpanel4\Orders\Models\Order\OrderGiftedProduct;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<OrderProduct>
 */
final class OrderGiftedProductFactory extends OrderProductFactory
{
    /**
     * @var class-string<OrderGiftedProduct>
     */
    protected $model = OrderGiftedProduct::class;
}
