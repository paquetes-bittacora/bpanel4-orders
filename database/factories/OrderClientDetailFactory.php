<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Database\Factories;

use Bittacora\Bpanel4\Orders\Models\Order\OrderClientDetail;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;

/**
 * @extends Factory<OrderClientDetail>
 */
final class OrderClientDetailFactory extends Factory
{
    /**
     * @var class-string<OrderClientDetail>
     */
    protected $model = OrderClientDetail::class;

    /**
     * @return array<string, Model>|array<string, string>
     */
    public function definition(): array
    {
        return [
            'client_id' => (new TestClientFactory())->createOne(),
            'name' => 'Nombre del cliente ' . $this->faker->numberBetween(),
            'surname' => $this->faker->lastName,
            'nif' => '00000000A',
            'company' => $this->faker->company,
            'phone' => $this->faker->phoneNumber,
            'email' => $this->faker->email,
        ];
    }
}
