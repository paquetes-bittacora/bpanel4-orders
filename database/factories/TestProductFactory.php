<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Database\Factories;

use Bittacora\Bpanel4\Orders\Tests\Integration\Models\TestProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<TestProduct>
 */
final class TestProductFactory extends Factory
{
    /**
     * @var class-string<TestProduct>
     */
    protected $model = TestProduct::class;

    /**
     * @throws InvalidPriceException
     */
    public function definition(): array
    {
        $reference = substr($this->faker->shuffle('abcdefghijklmnñopqrstuvwxyz-_1234567890'), 0, 12);
        return [
            'reference' => $reference,
            'name' => $this->faker->name(),
            'unit_price' => new Price($this->faker->numberBetween(30, 50)),
            'discounted_unit_price' => new Price($this->faker->numberBetween(10, 20)),
            'vat_rate_id' => 1,
        ];
    }
}
