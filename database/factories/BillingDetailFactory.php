<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Database\Factories;

use Bittacora\Bpanel4\Orders\Models\Order\OrderBillingDetail;
use Bittacora\Bpanel4\Orders\Models\Order\OrderShippingDetail;
use Bittacora\LivewireCountryStateSelector\Database\Factories\CountryFactory;
use Bittacora\LivewireCountryStateSelector\Database\Factories\StateFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;

/**
 * @extends Factory<OrderShippingDetail>
 */
final class BillingDetailFactory extends Factory
{
    /**
     * @var class-string<OrderShippingDetail>
     */
    protected $model = OrderBillingDetail::class;

    /**
     * @return array<string, Model>|array<string, string>
     */
    public function definition(): array
    {
        return [
            'country_id' => (new CountryFactory())->createOne(),
            'state_id' => (new StateFactory())->createOne(),
            'postal_code' => $this->faker->postcode(),
            'location' => $this->faker->name(),
            'name' => $this->faker->name(),
            'address' => $this->faker->address(),
            'person_name' => $this->faker->name(),
            'person_surname' => $this->faker->lastName(),
            'person_nif' => $this->faker->randomNumber(8) . $this->faker->randomLetter(),
        ];
    }
}
