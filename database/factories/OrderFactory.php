<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Database\Factories;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Order>
 */
final class OrderFactory extends Factory
{
    /**
     * @var class-string<\Bittacora\Bpanel4\Orders\Models\Order\Order>
     */
    protected $model = Order::class;

    /**
     * @return array<string, int>
     */
    public function definition(): array
    {
        return [
            'subtotal_without_taxes' => 10,
            'total_taxes' => 0,
            'subtotal' => 10,
            'shipping_costs' => 0,
            'order_total' => 10,
            'payment_method' => 'payment_method_class_name',
            'payment_method_name' => 'Nombre del método de envío',
            'order_status_id' => OrderStatusModel::create(['name' => 'Estado del pedido']),
            'shipping_costs_taxes' => 1,
        ];
    }

    public function withGiftedProducts(int $numberOfProducts = 3): self
    {
        return $this->has((new OrderGiftedProductFactory())->count($numberOfProducts), 'giftedProducts');
    }

    public function withProducts(int $numberOfProducts = 3): self
    {
        return $this->has((new OrderProductFactory())->count($numberOfProducts), 'products');
    }

    /**
     * @throws InvalidPriceException
     */
    public function getFullOrder(): Order
    {
        (new VatRateFactory())->withRate(10)->createOne();
        (new VatRateFactory())->withRate(21)->createOne();
        $order = $this
            ->has((new OrderClientDetailFactory()), 'client')
            ->has((new ShippingDetailFactory()), 'shippingDetails')
            ->has((new BillingDetailFactory()), 'billingDetails')
            ->has((new OrderProductFactory()), 'products')
            ->createOne();
        $product = $order->getProducts()->firstOrFail();
        $product->setUnitPrice(new Price(1000));
        $product->setQuantity(1);
        $product->setTaxRate(10);
        $product->setTaxAmount(new Price(100));
        $product->setTotal(new Price(1100));
        $product->save();

        $order->setSubtotalWithoutTaxes(10000000);
        $order->setTotalTaxes(1008673);
        $order->setSubtotal(10000000);
        $order->setShippingCosts(41300);
        $order->setShippingCostsTaxes(8673);
        $order->setOrderTotal(11049973);
        $order->save();

        return $order;
    }
}
