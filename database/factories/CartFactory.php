<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Database\Factories;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Addresses\Database\Factories\ModelAddressFactory;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Cart>
 */
final class CartFactory extends Factory
{
    /**
     * @var class-string<\Bittacora\Bpanel4\Orders\Models\Cart\Cart>
     */
    protected $model = Cart::class;

    /**
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $client = (new TestClientFactory())->has((new ModelAddressFactory())->count(3), 'addresses')->createOne();
        return [
            'client_id' => $client->id,
            'client_type' => $client::class,
        ];
    }
}
