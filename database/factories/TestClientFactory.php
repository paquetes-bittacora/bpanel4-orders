<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Database\Factories;

use Bittacora\Bpanel4\Clients\Contracts\Client;
use Bittacora\Bpanel4\Orders\Tests\Integration\Models\TestClient;
use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method TestClient create()
 * @method TestClient createOne($attributes = [])
 * @extends Factory<Client>
 */
final class TestClientFactory extends Factory
{
    /**
     * @var class-string<\Bittacora\Bpanel4\Clients\Models\Client>
     */
    protected $model = TestClient::class;

    /**
     * @return array<string, string>|array<string, UserFactory>
     */
    public function definition(): array
    {
        $user = (new UserFactory())->createOne();
        return [
            'name' => $this->faker->name(),
            'surname' => $this->faker->lastName(),
            'dni' => $this->faker->numberBetween('10000000', '999999999') . $this->faker->randomLetter(),
            'phone' => $this->faker->phoneNumber(),
            'user_id' => $user->id,
        ];
    }
}
