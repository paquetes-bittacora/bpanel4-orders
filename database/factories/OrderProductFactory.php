<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Database\Factories;

use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;

/**
 * @extends Factory<OrderProduct>
 */
class OrderProductFactory extends Factory
{
    /**
     * @var class-string<OrderProduct>
     */
    protected $model = OrderProduct::class;

    /**
     * @return array<string, Model>|array<string, string>
     * @throws InvalidPriceException
     */
    public function definition(): array
    {
        $product = (new TestProductFactory())->createOne();
        return [
            'product_id' => $product,
            'name' => $this->faker->name(),
            'unit_price' => new Price(10),
            'reference' => $product->reference,
            'quantity' => 1,
            'tax_rate' => '21.0',
            'tax_amount' => new Price(2.1),
            'total' => new Price(10),
        ];
    }

    public function withUnitPrice(float $price): self
    {
        return $this->state(fn (array $attributes): array => [
            'unit_price' => new Price($price),
        ]);
    }

    public function withTaxRate(float $taxRate): self
    {
        return $this->state(fn (array $attributes): array => [
            'tax_rate' => $taxRate,
        ]);
    }

    public function withTaxAmount(float $taxAmount): self
    {
        return $this->state(fn (array $attributes): array => [
            'tax_amount' => new Price($taxAmount),
        ]);
    }
}
