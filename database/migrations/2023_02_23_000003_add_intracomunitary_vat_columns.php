<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'orders';

    public function up(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->string('ip_address')->after('payment_method')->nullable();
            $table->string('vat_number')->after('ip_address')->nullable();
            $table->string('country_code')->after('ip_address')->nullable();
        });
    }

    public function down(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->dropColumn('ip_address');
            $table->dropColumn('vat_number');
            $table->dropColumn('country_code');
        });
    }
};
