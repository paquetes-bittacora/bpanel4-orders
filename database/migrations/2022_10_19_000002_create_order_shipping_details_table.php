<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'order_shipping_details';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('state_id');
            $table->string('postal_code');
            $table->string('location');
            $table->string('name');
            $table->string('address');

            $table->string('shipping_method_name');
            $table->string('shipping_method_type');
            $table->unsignedBigInteger('shipping_method_id');

            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('state_id')->references('id')->on('states');
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
