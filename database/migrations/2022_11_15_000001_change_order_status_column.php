<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'orders';

    public function up(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->dropColumn('status'); // Tengo que hacerlo en una llamada a Schema::table separada
        });

        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->unsignedBigInteger('order_status_id')->after('order_total')->nullable();
            $table->foreign('order_status_id')->references('id')->on('order_status');
        });
    }

    public function down(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->dropColumn('order_status_id');
        });
    }
};
