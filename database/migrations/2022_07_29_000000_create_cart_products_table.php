<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('cart_products', static function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('cartable_id');
            $table->string('cartable_type');
            $table->unsignedBigInteger('cart_id');
            $table->integer('quantity');

            $table->unique(['cartable_id', 'cartable_type', 'cart_id']);
        });
    }

    public function down(): void
    {
        Schema::drop('cart_products');
    }
};
