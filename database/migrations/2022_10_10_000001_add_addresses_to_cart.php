<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'carts';

    public function up(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->unsignedBigInteger('shipping_address_id')->nullable()
                ->after('selected_shipping_method');
            $table->unsignedBigInteger('billing_address_id')->nullable()
                ->after('shipping_address_id');

            $table->foreign('shipping_address_id')->references('id')->on('model_addresses');
            $table->foreign('billing_address_id')->references('id')->on('model_addresses');
        });
    }

    public function down(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->dropForeign('shipping_address_id');
            $table->dropForeign('billing_address_id');
            $table->dropColumn('shipping_address_id');
            $table->dropColumn('billing_address_id');
        });
    }
};
