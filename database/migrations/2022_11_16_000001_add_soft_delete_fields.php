<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table('orders', static function (Blueprint $table): void {
            $table->softDeletes();
        });
        Schema::table('order_billing_details', static function (Blueprint $table): void {
            $table->softDeletes();
        });
        Schema::table('order_client_details', static function (Blueprint $table): void {
            $table->softDeletes();
        });
        Schema::table('order_products', static function (Blueprint $table): void {
            $table->softDeletes();
        });
        Schema::table('order_shipping_details', static function (Blueprint $table): void {
            $table->softDeletes();
        });
    }

    public function down(): void
    {
        Schema::table('orders', static function (Blueprint $table): void {
            $table->dropSoftDeletes();
        });
        Schema::table('order_billing_details', static function (Blueprint $table): void {
            $table->dropSoftDeletes();
        });
        Schema::table('order_client_details', static function (Blueprint $table): void {
            $table->dropSoftDeletes();
        });
        Schema::table('order_products', static function (Blueprint $table): void {
            $table->dropSoftDeletes();
        });
        Schema::table('order_shipping_details', static function (Blueprint $table): void {
            $table->dropSoftDeletes();
        });
    }
};
