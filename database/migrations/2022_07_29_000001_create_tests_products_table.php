<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        /*
         * Esta migración solo se ejecuta mientras se ejecutan los tests, ya que
         * en un proyecto real, se usará una clase para los productos que en
         * este módulo es genérica (solo se exije que implemente la interfaz
         * CartableProduct.
         */
        if (App::runningUnitTests()) {
            Schema::create('test_products', static function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->integer('unit_price');
                $table->integer('discounted_unit_price');
                $table->string('reference');
                $table->unsignedBigInteger('vat_rate_id')->nullable();
                $table->timestamps();
            });
        }
    }
};
