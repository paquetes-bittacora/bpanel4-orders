<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {

    private const TABLE_NAME = 'cart_gifted_products';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('cartable_id');
            $table->string('cartable_type');
            $table->unsignedBigInteger('cart_id');
            $table->unsignedBigInteger('coupon_id');
            $table->integer('quantity');

            $table->unique(['cartable_id', 'cartable_type', 'cart_id']);
            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
