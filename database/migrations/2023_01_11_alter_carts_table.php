<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'carts';

    public function up(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table) {
            $table->string('client_type')->nullable()->change();
            $table->unsignedBigInteger('client_id')->nullable()->change();
        });
    }

    public function down(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table) {
            $table->string('client_type')->nullable(false)->change();
            $table->unsignedBigInteger('client_id')->nullable(false)->change();
        });
    }
};
