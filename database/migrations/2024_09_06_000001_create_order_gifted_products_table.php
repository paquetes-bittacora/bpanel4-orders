<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {

    private const TABLE_NAME = 'order_gifted_products';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('product_id')->nullable();
            $table->string('name');
            $table->bigInteger('unit_price');
            $table->bigInteger('discounted_unit_price')->nullable();
            $table->string('reference');
            $table->bigInteger('quantity');
            $table->float('tax_rate');
            $table->bigInteger('tax_amount');
            $table->bigInteger('total')->comment('Total pagado por el producto (sin imp.)');
            $table->json('meta')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
