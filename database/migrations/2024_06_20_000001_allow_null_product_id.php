<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'order_products';

    public function up(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            // Cuando me pidieron que se pudiesen añadir productos manualmente a las facturas, me di cuenta de que
            // no se podía añadir un producto "inventado", que es precisamente lo que querían, así que tengo que hacer
            // nullable product_id. Hay tests que cubren este caso, como por ejemplo CreateOrderProductTest.
            $table->unsignedBigInteger('product_id')->nullable()->change();
        });
    }

    public function down(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->unsignedBigInteger('product_id')->change();
        });
    }
};
