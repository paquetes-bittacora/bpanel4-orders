<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration;

use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Bittacora\Bpanel4\Orders\Exceptions\CartAlreadyExistsException;
use Bittacora\Bpanel4\Orders\Tests\Integration\Factories\CartableClientFactory;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateCartTest extends TestCase
{
    use RefreshDatabase;

    private CartableClientFactory $clientFactory;

    public function setUp(): void
    {
        parent::setUp();
        $this->clientFactory = $this->app->make(CartableClientFactory::class);

        $this->withoutExceptionHandling();
    }

    public function testUnClientePuedeTenerUnCarrito(): void
    {
        // Al llamar al endpoint que crea los carritos con un id de cliente
        // Veo en la bd un carrito para el cliente
        /** @var Authenticatable&CartableClient $client */
        $client = $this->clientFactory->create();
        $this->actingAs($client);
        $this->post('/create-cart');

        self::assertDatabaseHas('carts', [
            'client_id' => $client->getClientId(),
            'client_type' => get_class($client),
        ]);
    }

    public function testUnClienteNoPuedeTenerVariosCarritos(): void
    {
        $this->expectException(CartAlreadyExistsException::class);
        /** @var Authenticatable&CartableClient $client */
        $client = $this->clientFactory->create();
        $this->actingAs($client);
        $this->post('/create-cart');
        $this->post('/create-cart');
    }
}
