<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Services\Order\OrderService;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Bpanel4Users\Models\User;
use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\LaravelTestingHelpers\Views\VendorViewsDisabler;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery\MockInterface;
use Spatie\Permission\Models\Permission;
use Tests\TestCase;

final class MyOrdersPagesTest extends TestCase
{
    use RefreshDatabase;

    private OrderService $orderService;
    private UrlGenerator $urlGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->urlGenerator = $this->app->make(UrlGenerator::class);

        $this->mock('payment_method_class_name', static function (MockInterface $mock): void {
            $mock->shouldReceive('getUserInstructions')->andReturn('');
        });

        $this->orderService = $this->app->make(OrderService::class);

        Permission::create(['name' => 'my-orders.index']);
        Permission::create(['name' => 'my-orders.show']);
        $this->createShippingVatRate();
        VendorViewsDisabler::disableViews();
    }

    public function testCargaLaPaginaMisPedidos(): void
    {
        $this->actingAs($this->getUser());
        $response = $this->get($this->urlGenerator->route('my-account.orders'));
        $response->assertOk();
        $response->assertSeeLivewire('bpanel4-orders::livewire.client-orders-table');
    }

    public function testCargaLosDetallesDeUnPedido(): void
    {
        $order = (new OrderFactory())->getFullOrder();
        $this->actingAs($this->getClientForOrder($order)->getUser());
        $response = $this->get($this->urlGenerator->route('my-account.view-order', ['order' => $order->getId()]));
        $response->assertOk();
        $response->assertSee('Detalles del pedido');
    }

    public function testNoSePuedenVerLosDetallesDeUnPedidoDeOtroCliente(): void
    {
        $order = (new OrderFactory())->getFullOrder();
        $otherOrder = (new OrderFactory())->getFullOrder();
        $this->actingAs($this->getClientForOrder($order)->getUser());
        $response = $this->get(
            $this->urlGenerator->route(
                'my-account.view-order',
                ['order' => $otherOrder->getId()]
            )
        );
        $response->assertForbidden();
    }

    public function getClientForOrder(Order $order): Client
    {
        $client = $this->orderService->getClientForOrder($order);

        $client->getUser()->givePermissionTo('my-orders.index');
        $client->getUser()->givePermissionTo('my-orders.show');

        return $client;
    }

    /**
     * @throws InvalidPriceException
     */
    public function testSeMuestraElDniDeFacturacionSiLasDireccionesSonDistintas(): void
    {
        // Arrange
        $order = (new OrderFactory())->getFullOrder();
        $this->actingAs($this->getClientForOrder($order)->getUser());

        // Act
        $response = $this->get($this->urlGenerator->route('my-account.view-order', ['order' => $order->getId()]));

        // Assert
        $response->assertSee($order->getBillingDetails()->getPersonNif() ?? 'ERROR');
        $response->assertSee('billing-details-block');
    }

    /**
     * @throws InvalidPriceException
     */
    public function testSeMuestraElDniDeEnvioSiLasDireccionesSonIguales(): void
    {
        // Arrange (hago que las 2 direcciones sean iguales)
        $order = (new OrderFactory())->getFullOrder();
        $this->actingAs($this->getClientForOrder($order)->getUser());
        $orderBillingDetail = $this->makeBillingAndShippingAddressEqual($order);

        // Act
        $response = $this->get($this->urlGenerator->route('my-account.view-order', ['order' => $order->getId()]));

        // Assert
        $response->assertSee($orderBillingDetail->getPersonNif() ?? 'ERROR');
        $response->assertDontSee('billing-details-block');
    }

    private function getUser(): User
    {
        $user = (new ClientFactory())->createOne()->getUser();
        $user->givePermissionTo('my-orders.index');
        $user->givePermissionTo('my-orders.show');
        return $user;
    }

    private function createShippingVatRate()
    {
        if (VatRate::where('apply_to_shipping', 1)->count() === 0) {
            $vatRate = (new VatRateFactory())->createOne();
            $vatRate->setApplyToShipping(true);
            $vatRate->save();
        }
    }

    private function makeBillingAndShippingAddressEqual(Order $order): \Bittacora\Bpanel4\Orders\Models\Order\OrderBillingDetail
    {
        $orderBillingDetail = $order->getBillingDetails();
        $orderBillingDetail->setAddress($order->getShippingDetails()->getAddress());
        $orderBillingDetail->save();
        return $orderBillingDetail;
    }
}
