<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Services\Order;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Mail\ClientOrderSummaryMail;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Services\Order\ClientEmailSender;
use Bittacora\Bpanel4\ShopConfiguration\Tests\Helpers\ShopConfigurationTestHelper;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

final class ClientEmailSenderTest extends TestCase
{
    use RefreshDatabase;

    private ClientEmailSender $clientEmailSender;
    private Order $order;

    public function setUp(): void
    {
        parent::setUp();
        Mail::fake();
        ShopConfigurationTestHelper::createEmptyShopConfiguration();
        $this->clientEmailSender = $this->app->make(ClientEmailSender::class);
        $this->order = (new OrderFactory())->getFullOrder();
    }

    public function testEnviaElResumenDelPedidoAlCliente(): void
    {
        $this->clientEmailSender->send($this->order);
        Mail::assertSent(ClientOrderSummaryMail::class);
    }
}
