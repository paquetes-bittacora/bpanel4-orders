<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Controllers;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Bittacora\Bpanel4Users\Models\User;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

final class OrderAdminControllerProductEditionTest extends TestCase
{
    use RefreshDatabase;

    private Order $order;

    private TestResponse $response;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
        $this->renderDetailsPage();
    }

    public function testSiElEstadoDelPedidoEsEnEsperaSePuedenEditarLosProductos(): void
    {
        // Arrange
        $this->markOrderAsWaiting();

        // Act
        $response = $this->get(route('orders-admin.edit', ['order' => $this->order->getId()]));

        // Assert
        $response->assertSee('Editar producto');
        $response->assertSee('Eliminar producto');
    }

    public function testSiElEstadoDelPedidoNoEsEnEsperaNoSePuedenEditarLosProductos(): void
    {
        // Arrange
        $this->order->setStatus(OrderStatusModel::create(['name' => 'Completado']));

        // Act
        $response = $this->get(route('orders-admin.edit', ['order' => $this->order->getId()]));

        // Assert
        $response->assertDontSee('Eliminar producto');
    }

    public function testSeMuestraElComponenteDeLivewireParaEditarElPedido(): void
    {
        // Arrange
        $this->markOrderAsWaiting();

        // Act
        $response = $this->get(route('orders-admin.edit', ['order' => $this->order->getId()]));

        // Assert
        $response->assertSeeLivewire('bpanel4-orders::order-editor');
    }

    private function renderDetailsPage(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->withPermissions('orders-admin.edit')->createOne();
        $this->actingAs($user);
        $this->order = (new OrderFactory())->getFullOrder();

        $this->response = $this->get(route('orders-admin.edit', ['order' => $this->order->getId()]));
    }

    private function markOrderAsWaiting(): void
    {
        $orderStatus = OrderStatusModel::create(['name' => 'En espera']);
        $orderStatus->id = OrderStatus::WAITING->value;
        $orderStatus->save();
        $this->order->setStatus($orderStatus);
        $this->order->save();
    }
}
