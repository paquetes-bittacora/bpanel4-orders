<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Controllers;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Bittacora\Bpanel4Users\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\TestResponse;
use Spatie\Permission\Models\Permission;
use Tests\TestCase;

final class OrderAdminControllerTest extends TestCase
{
    use RefreshDatabase;

    private Order $order;
    private TestResponse $response;

    protected function setUp(): void
    {
        parent::setUp();
        $this->renderDetailsPage();
    }

    public function testMuestraLosDetallesDelCliente(): void
    {
        $this->response->assertSeeText($this->order->getClient()->getName());
    }

    public function testMuestraLosTotales(): void
    {
        $this->response->assertSeeText(Price::fromInt($this->order->getOrderTotal())->toString());
    }

    public function testMuestraLosDetallesDeEnvio(): void
    {
        $this->response->assertSeeText($this->order->getShippingDetails()->getLocation());
    }

    public function testMuestraLosProductos(): void
    {
        $this->response->assertSeeText($this->order->getProducts()[0]->getName());
    }

    private function renderDetailsPage(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->withPermissions('orders-admin.edit')->createOne();
        $this->actingAs($user);
        $this->order = (new OrderFactory())->getFullOrder();

        $this->response = $this->get(route('orders-admin.edit', ['order' => $this->order->getId()]));
    }
}
