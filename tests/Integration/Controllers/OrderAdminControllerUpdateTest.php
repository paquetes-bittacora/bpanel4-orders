<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Controllers;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderBillingDetail;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Bittacora\Bpanel4Users\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\TestResponse;
use Spatie\Permission\Models\Permission;
use Tests\TestCase;

final class OrderAdminControllerUpdateTest extends TestCase
{
    use RefreshDatabase;

    private Order $order;
    private TestResponse $response;
    private $route;

    protected function setUp(): void
    {
        parent::setUp();
        $this->prepareTestOrder();
        $this->route = route('orders-admin.update', ['order' => $this->order->getId()]);
        $this->withoutExceptionHandling();
    }

    public function testActualizaElIvaDeFacturacion(): void
    {
        // Arrange (en setUp)
        $newPersonNif = random_int(10000000,99999999) . 'A';
        $data = $this->getValidData(nif: $newPersonNif);
        $data['clientBillingAddressNif'] = $newPersonNif;

        // Act
        $this->put($this->route, $data);

        // Assert
        $this->assertDatabaseHas(OrderBillingDetail::class, [
            'order_id' => $this->order->id,
            'person_nif' => $newPersonNif,
        ]);
    }

    private function prepareTestOrder(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->withPermissions('orders-admin.edit')->createOne();
        $this->actingAs($user);
        $this->order = (new OrderFactory())->getFullOrder();
    }

    private function getValidData(?string $nif = null): array
    {
        return [
            'clientName' => 'Nombre del cliente',
            'clientSurname' => 'Apellidos del cliente',
            'clientPhone' => '600123321',
            'clientEmail' => 'prueba@prueba.com',
            'clientNif' => random_int(10000000, 99999999) . 'Z',
            'clientBillingAddressNif' => $nif ?? '00000000A',
            'clientShippingAddressName' => 'Nombre de la dirección',
            'clientShippingAddressSurname' => 'Apellidos direccion',
            'clientShippingAddressPhone' => '600000000',
            'clientShippingAddress' => 'Dirección',
            'clientShippingCountry' => $this->order->getShippingDetails()->country_id,
            'clientShippingState' => $this->order->getShippingDetails()->state_id,
            'clientShippingLocation' => 'Localidad',
            'clientShippingPostalCode' => '01001',
            'clientBillingAddressNif' => $nif ?? '00000000A',
            'clientBillingAddressName' => 'Nombre de la dirección',
            'clientBillingAddressSurname' => 'Apellidos direccion',
            'clientBillingAddressPhone' => '600000000',
            'clientBillingAddress' => 'Dirección',
            'clientBillingCountry' => $this->order->getBillingDetails()->country_id,
            'clientBillingState' => $this->order->getBillingDetails()->state_id,
            'clientBillingLocation' => 'Localidad',
            'clientBillingPostalCode' => '01001',
        ];
    }
}
