<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Models;

use Bittacora\Bpanel4\Clients\Contracts\Client;
use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Addresses\Traits\ModelWithAddressesTrait;
use Bittacora\Bpanel4Users\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Mockery;
use RuntimeException;

final class TestClient extends Model implements Client, CartableClient
{
    use ModelWithAddressesTrait;

    protected $table = 'clients';

    protected $guarded = [];

    /*
     * Hacer migración y factory para este modelo falso
     * */
    public function getType(): string
    {
        return 'client-type';
    }

    public function getShippingAddress(): ModelAddress
    {
        /** @var ?ModelAddress $shippingAddress */
        $shippingAddress = $this->shippingAddress()->first();

        return $shippingAddress ?? $this->addresses()->get()->firstOrFail();
    }

    public function getUser(): User
    {
        /** @var User $user */
        $user = $this->user()->firstOrFail();

        return $user;
    }

    public function getClientId(): int
    {
        return $this->id;
    }

    public function shippingAddress(): BelongsTo
    {
        return $this->belongsTo(ModelAddress::class);
    }

    public function getBillingAddress(): ModelAddress
    {
        /** @var ?ModelAddress $billingAddress */
        $billingAddress = $this->billingAddress()->first();
        return $billingAddress ?? $this->getShippingAddress();
    }

    public function billingAddress(): BelongsTo
    {
        return $this->belongsTo(ModelAddress::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLastName(): string
    {
        return 'Apellidos';
    }

    public function getNif(): string
    {
        return 'NIF';
    }

    public function getCompany(): string
    {
        return 'Empresa';
    }

    public function getPhone(): string
    {
        return '600000000';
    }

    public function getEmail(): string
    {
        return 'email@email.com';
    }

    /**
     * @throws Exception
     */
    public function getClient(): CartableClient
    {
        /** @phpstan-ignore-next-line  */
        return Mockery::mock(CartableClient::class);
    }
}
