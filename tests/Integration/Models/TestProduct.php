<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Models;

use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Prices\Casts\PriceCast;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Bpanel4\Vat\Traits\HasVatRate;
use Eloquent;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Clase que solo se usará en los tests
 *
 * @property int $id
 * @property Price $unit_price
 * @property Price $discounted_unit_price
 * @property string $name
 * @property string $reference
 * @property-read VatRate|null $vatRate
 * @method static Builder|TestProduct newModelQuery()
 * @method static Builder|TestProduct newQuery()
 * @method static Builder|TestProduct query()
 * @mixin Eloquent
 */
final class TestProduct extends Model implements CartableProduct
{
    use HasVatRate;

    /**
     * @var array<string, class-string<PriceCast>>|array<string, string>
     */
    protected $casts = [
        'id' => 'int',
        'unit_price' => PriceCast::class,
        'discounted_unit_price' => PriceCast::class,
        'size' => 'float',
        'stock' => 'integer',
        'active' => 'boolean',
    ];

    /**
     * @var mixed[]
     */
    public $guarded = [];

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getOriginalUnitPrice(): Price
    {
        return $this->unit_price;
    }

    public function getDiscountedUnitPrice(): Price
    {
        return $this->discounted_unit_price;
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function getImages(): Collection
    {
        return new Collection();
    }

    public function getQuantity(): int
    {
        /** @phpstan-ignore-next-line  */
        return $this->pivot->quantity;
    }

    public function getCartableProductId(): int
    {
        return $this->pivot->id;
    }

    public function getUnitPrice(): Price
    {
        return $this->getOriginalUnitPrice();
    }

    public function getTotal(): Price
    {
        return new Price($this->getQuantity() * $this->getUnitPrice()->toFloat());
    }

    public function getUnitPriceWithoutVat(): Price
    {
        return $this->getUnitPrice();
    }

    public function getOriginalUnitPriceWithoutVat(): Price
    {
        return $this->getOriginalUnitPrice();
    }

    public function getTotalWithoutVat(): Price
    {
        return $this->getTotal();
    }

    public function getProductId(): int
    {
        return $this->id;
    }

    /**
     * @throws Exception
     */
    public function getStock(): int
    {
        return random_int(1, 100);
    }
}
