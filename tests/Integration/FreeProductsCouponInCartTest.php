<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration;

use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Coupons\Database\Factories\FreeProductsCouponFactory;
use Bittacora\Bpanel4\Coupons\Exceptions\CartAmountOutsideCouponLimitsException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasExpiredException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasReachedUsageLimitException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponIsAlreadyAppliedException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponIsNotAvailableIfCartHasDiscountedProductsException;
use Bittacora\Bpanel4\Coupons\Models\FreeProductsCoupon;
use Bittacora\Bpanel4\Coupons\Services\CouponApplier;
use Bittacora\Bpanel4\Orders\Actions\Cart\RemoveCartRow;
use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Events\ProductQuantityHasBeenUpdated;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Models\Cart\CartRow;
use Bittacora\Bpanel4\Orders\Tests\Integration\Helpers\InitializeBasicCartForClient;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Models\CartProductFactory;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\Products\Presenters\ProductPresenterFactory;
use Bittacora\LaravelTestingHelpers\Views\VendorViewsDisabler;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Safe\Exceptions\FilesystemException;
use Tests\TestCase;

final class FreeProductsCouponInCartTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        VendorViewsDisabler::disableViews();
    }

    /**
     * @throws CouponHasReachedUsageLimitException
     * @throws CouponIsAlreadyAppliedException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws CouponHasExpiredException
     * @throws CartAmountOutsideCouponLimitsException
     * @throws UserNotLoggedInException
     */
    public function testAlQuitarUnCuponDeProductosRegaloSeQuitanLosProductosRegalados(): void
    {
        // Arrange
        $cart = $this->prepareCart();
        $giftedProducts = $this->getGiftedProducts();
        $coupon = $this->applyCouponWithGiftedProducts($cart->getProducts()->all(), $giftedProducts);
        $this->checkThatGiftedProductsArePresentInCart($giftedProducts);

        // Act
        $this->removeCouponFromCart($coupon);
        $result = $this->get('carrito');

        // Assert
        foreach ($giftedProducts as $giftedProduct) {
            $result->assertDontSee($giftedProduct['product']->name, false);
        }
    }

    /**
     * @throws CouponHasReachedUsageLimitException
     * @throws CouponIsAlreadyAppliedException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws CouponHasExpiredException
     * @throws UserNotLoggedInException
     * @throws CartAmountOutsideCouponLimitsException|FilesystemException
     */
    public function testSiSeDejanDeCumplirLasCondicionesDeUnCuponProductosRegaloSeQuitanLosProductosRegalados(): void
    {
        // Arrange
        $cart = $this->prepareCart();
        $giftedProducts = $this->getGiftedProducts();
        $this->applyCouponWithGiftedProducts($cart->getProducts()->all(), $giftedProducts);
        $this->checkThatGiftedProductsArePresentInCart($giftedProducts);
        $result = $this->get('carrito');

        // Act
        $this->removeRequiredProductsFromCart($cart);
        $result = $this->get('carrito');

        // Assert
        foreach ($giftedProducts as $giftedProduct) {
            $result->assertDontSee($giftedProduct['product']->name, false);
        }
    }

    /**
     * @throws CouponHasReachedUsageLimitException
     * @throws CouponIsAlreadyAppliedException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws CouponHasExpiredException
     * @throws UserNotLoggedInException
     * @throws FilesystemException
     * @throws CartAmountOutsideCouponLimitsException
     */
    public function testSiSeCambiaLaCantidadDeUnProductoSeQuitanLosProductosRegalados(): void
    {
        // Arrange
        $cart = $this->prepareCart();
        $giftedProducts = $this->getGiftedProducts();
        $this->applyCouponWithGiftedProducts($cart->getProducts()->all(), $giftedProducts);
        $this->checkThatGiftedProductsArePresentInCart($giftedProducts);
        $result = $this->get('carrito');

        // Act
        $this->changeRequiredProductQuantityInCart($cart);
        $result = $this->get('carrito');

        // Assert
        foreach ($giftedProducts as $giftedProduct) {
            $result->assertDontSee($giftedProduct['product']->name, false);
        }
    }

    /** @return array<array{product: Product, quantity:int}> */
    private function getGiftedProducts(): array
    {
        $output = [];
        $products = (new ProductFactory())->count(2)->create();

        foreach ($products as $product) {
            $output[] = ['product' => $product, 'quantity' => 1];
        }

        return $output;
    }

    /**
     * @throws UserNotLoggedInException
     */
    private function getCartableProduct(): CartableProduct
    {
        $cartProductFactory = new CartProductFactory($this->app->make(ClientService::class),
            $this->app->make(ProductPresenterFactory::class));
        $cartProductFactory->setProduct((new ProductFactory())->createOne());
        return $cartProductFactory->make();
    }

    /**
     * @throws CouponHasReachedUsageLimitException
     * @throws CouponIsAlreadyAppliedException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws CouponHasExpiredException
     * @throws CartAmountOutsideCouponLimitsException
     * @throws UserNotLoggedInException
     */
    private function applyCouponWithGiftedProducts(array $requiredProducts, array $giftedProducts): FreeProductsCoupon
    {
        $aux = [];

        foreach ($requiredProducts as $requiredProduct) {
            $aux[] = ['product' => Product::whereId($requiredProduct->getProductId())->firstOrFail(), 'quantity' => 3];
        }

        $coupon = (new FreeProductsCouponFactory())
            ->withRequiredProducts($aux)
            ->withGiftedProducts($giftedProducts)
            ->createOne();

        $couponApplier = $this->app->make(CouponApplier::class);
        $couponApplier->apply($coupon->code);
        return $coupon;
    }

    /**
     * @throws UserNotLoggedInException
     */
    private function prepareCart(): Cart
    {
        [
            'cartService' => $cartService,
            'client' => $client
        ] = (new InitializeBasicCartForClient())->execute($this->app);
        $this->actingAs($client->getUser());
        $cart = $cartService->getCart();
        $cart->addProduct($this->getCartableProduct(), 3);
        return $cart;
    }

    private function checkThatGiftedProductsArePresentInCart(array $giftedProducts): void
    {
        $result1 = $this->get('carrito');
        foreach ($giftedProducts as $giftedProduct1) {
            $result1->assertSee($giftedProduct1['product']->name, false);
        }
    }

    private function removeCouponFromCart(FreeProductsCoupon $coupon): void
    {
        $this->get('cupones-descuento/' . $coupon->getId() . '/quitar');
    }

    private function removeRequiredProductsFromCart(Cart $cart): void
    {
        $removeCartRow = $this->app->make(RemoveCartRow::class);
        $cartRows = CartRow::where('cart_id', $cart->getId())->get()->all();
        foreach ($cartRows as $cartRow) {
            $this->get('carrito/' . $cartRow->id . '/quitar');
        }
    }

    private function changeRequiredProductQuantityInCart(Cart $cart): void
    {
        $products = $cart->getProducts();
        --$products[0]->pivot->quantity;
        $cart->save();
        // Este evento se dispara automáticamente desde livewire pero aquí lo simulo porque no estoy probando el
        // componente sino el carrito.
        ProductQuantityHasBeenUpdated::dispatch($products[0]->id, $products[0]->pivot->quantity);
    }
}