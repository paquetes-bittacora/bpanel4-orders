<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Factories;

use App\Models\User;
use Bittacora\Bpanel4\Addresses\Database\Factories\ModelAddressFactory;
use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

final class CartableClientFactory
{
    /**
     * @phpstan-return CartableClient&Model&Authenticatable
     */
    public function create(): CartableClient
    {
        return new class () extends User implements CartableClient {
            public function __construct()
            {
                parent::__construct();
                $user = User::factory()->create();
                foreach (get_object_vars($user) as $key => $value) {
                    $this->$key = $value;
                }

                $this->table = 'users';
            }

            public function getClientId(): int
            {
                return $this->id;
            }

            public function getClient(): Model&CartableClient
            {
                /** @phpstan-ignore-next-line  */
                return $this;
            }

            public function getName(): string
            {
                return 'Nombre';
            }

            public function getLastName(): string
            {
                return 'Last Name';
            }

            public function getShippingAddress(): ModelAddress
            {
                return (new ModelAddressFactory())->createOne();
            }

            public function getBillingAddress(): ModelAddress
            {
                return new ModelAddress();
            }

            public function getNif(): string
            {
                return '00000000A';
            }

            public function getCompany(): string
            {
                return 'Empresa';
            }

            public function getPhone(): string
            {
                return '600000000';
            }

            public function getEmail(): string
            {
                return 'email@email.com';
            }
        };
    }
}
