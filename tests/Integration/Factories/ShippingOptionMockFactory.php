<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Factories;

use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethodPriceCalculator;
use Bittacora\Bpanel4\Shipping\Types\ShippingOption;
use Mockery;
use Mockery\MockInterface;

final class ShippingOptionMockFactory
{
    private readonly MockInterface|ShippingOption $mock;

    public function __construct()
    {
        $this->mock = Mockery::mock(ShippingOption::class, [
            Mockery::mock(ShippingMethod::class),
            Mockery::mock(ShippingMethodPriceCalculator::class),
        ])->makePartial();
        $this->mock->shouldReceive('getKey')->andReturn('option-key')->byDefault();
    }

    public function getMock(): MockInterface|ShippingOption
    {
        return $this->mock;
    }

    public function withKey(string $key): self
    {
        $this->mock->shouldReceive('getKey')->andReturn($key);
        return $this;
    }

    public static function new(): self
    {
        return new self();
    }
}
