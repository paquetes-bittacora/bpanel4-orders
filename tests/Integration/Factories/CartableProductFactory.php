<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Factories;

use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Tests\Integration\Models\TestProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Faker\Factory;

final class CartableProductFactory
{
    /**
     * @throws InvalidPriceException
     * @throws \Exception
     */
    public function create(): CartableProduct&TestProduct
    {
        $faker = Factory::create('es_ES');
        $vatRate = new VatRate();
        $vatRate->name = 'test';
        $vatRate->rate = 2;
        $vatRate->active = true;
        $vatRate->save();
        return TestProduct::create([
            'name' => $faker->name(),
            'unit_price' => new Price($faker->numberBetween(50, 100)),
            'discounted_unit_price' => new Price($faker->numberBetween(20, 49)),
            'reference' => bin2hex(random_bytes(4)),
            'vat_rate_id' => $vatRate->getId(),
        ]);
    }
}
