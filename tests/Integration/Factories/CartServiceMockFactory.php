<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Factories;

use Bittacora\Bpanel4\Coupons\Services\CartDiscountCalculator;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Shipping\Services\ShippingCostsCalculator;
use Bittacora\Bpanel4\Vat\Services\ShippingVatService;
use Bittacora\Bpanel4\Vat\Services\VatRegionService;
use Bittacora\Bpanel4\Vat\Services\VatService;
use Mockery;
use Mockery\MockInterface;

final class CartServiceMockFactory
{
    private readonly MockInterface|CartService $mock;

    public function __construct()
    {
        $this->mock = Mockery::mock(CartService::class);
    }

    public static function withCart(Cart $cart): CartService|MockInterface
    {
        $cartDiscountCalculator = Mockery::mock(CartDiscountCalculator::class);
        $cartDiscountCalculator->shouldIgnoreMissing();
        $mock = Mockery::mock(CartService::class, [
            $cart,
            Mockery::mock(ShippingCostsCalculator::class),
            Mockery::mock(VatService::class),
            $cartDiscountCalculator,
            Mockery::mock(ShippingVatService::class),
        ])->makePartial();
        $mock->shouldAllowMockingProtectedMethods();
        return $mock;
    }

    public function getMock(): MockInterface|CartService
    {
        return $this->mock;
    }

    public static function new(): self
    {
        return new self();
    }
}
