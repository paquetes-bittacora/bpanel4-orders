<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Factories;

use Bittacora\Bpanel4\Orders\Actions\Cart\CreateCart;
use Bittacora\Bpanel4\Orders\Exceptions\CartAlreadyExistsException;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Models\VatRate;

final class CartFactory
{
    public function __construct(
        private readonly CartableClientFactory $clientFactory,
        private readonly CreateCart $createCart,
        private readonly CartableProductFactory $productFactory,
    ) {
    }

    /**
     * @throws CartAlreadyExistsException
     */
    public function createCart(): Cart
    {
        $client = $this->clientFactory->create();
        $this->createCart->execute($client);

        /** @var Cart $cart */
        $cart = Cart::whereRelation('client', 'id', $client->getClientId())->firstOrFail();
        $cart->setVatNumber(null);
        return $cart;
    }

    /**
     * @throws InvalidPriceException|CartAlreadyExistsException
     */
    public function getCartWith2Products(): Cart
    {
        $cart = $this->createCart();
        $product = $this->productFactory->create();
        $product->unit_price = Price::fromInt(50000);
        $product->setVatRate($this->getVatRate(21));
        $product->save();

        $product2 = $this->productFactory->create();
        $product2->unit_price = Price::fromInt(20000);
        $product2->setVatRate($this->getVatRate(4));
        $product2->save();

        $cart->addProduct($product, 3);
        $cart->addProduct($product2, 2);
        return $cart;
    }

    /**
     * @throws InvalidPriceException|CartAlreadyExistsException
     */
    public function getCartWithAProductWithPriceAndVat(int $quantity, float $price, int $vatRate): Cart
    {
        $cart = $this->createCart();
        $product = $this->productFactory->create();
        $product->unit_price = new Price($price);
        $product->setVatRate($this->getVatRate($vatRate));
        $product->save();

        $cart->addProduct($product, $quantity);
        return $cart;
    }

    private function getVatRate(int $rate): VatRate
    {
        $vatRate = new VatRate();
        $vatRate->setRate($rate);
        $vatRate->setName((string) $rate);
        $vatRate->setActive(true);
        if (21 === $rate) {
            $vatRate->setApplyToShipping(true);
        }
        $vatRate->save();
        return $vatRate;
    }
}
