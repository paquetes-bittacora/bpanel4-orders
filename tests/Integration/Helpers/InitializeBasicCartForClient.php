<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Helpers;

use Bittacora\Bpanel4\Addresses\Database\Factories\ModelAddressFactory;
use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Orders\Database\Factories\CartFactory;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Payment\Models\PaymentMethodRolePermission;
use Bittacora\Bpanel4\Payment\Models\PaymentMethodRow;
use Bittacora\Bpanel4\Payment\WireTransfer\Models\WireTransferConfig;
use Bittacora\Bpanel4\Payment\WireTransfer\PaymentMethods\WireTransfer;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Illuminate\Foundation\Application;
use Spatie\Permission\Models\Role;

final class InitializeBasicCartForClient
{
    /**
     * @return array{cartService: CartService, client: Client}
     */
    public function execute(Application $app): array
    {
        $this->createDefaultCountryAndState();
        $client = (new ClientFactory())->createOne();
        $cartService = $app->make(CartService::class, [
            'cart' => (new CartFactory())->for($client)->createOne()
        ]);
        $role = Role::create(['name' => 'registered-user']);
        $client->getUser()->assignRole($role);

        $paymentMethodRow = new PaymentMethodRow();
        $paymentMethodRow->payment_method = WireTransfer::class;
        $paymentMethodRow->save();

        WireTransferConfig::create();
        (new ModelAddressFactory())->for($client, 'addressable')->createOne();

        (new VatRateFactory())->forShipping()->createOne();

        foreach ($client->getUser()->roles as $role) {
            PaymentMethodRolePermission::create([
                'role_id' => $role->id,
                'payment_method_id' => $paymentMethodRow->id,
            ]);
        }

        return [
            'cartService' => $cartService,
            'client' => $client,
        ];
    }

    private function createDefaultCountryAndState(): void
    {
        $country = Country::firstOrCreate(['name' => 'España'], []);
        $state = State::whereName('Badajoz')->first();

        if (null === $state) {
            $state = new State();
            $state->country_id = $country->id;
            $state->name = 'Badajoz';
            $state->save();
        }
    }
}