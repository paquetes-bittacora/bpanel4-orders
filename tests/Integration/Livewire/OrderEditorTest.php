<?php

declare(strict_types=1);

namespace Integration\Livewire;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Http\Livewire\OrderEditor;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Tests\TestCase;

final class OrderEditorTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    public function testSePuedeRenderizarElcomponente(): void
    {
        // Arrange
        $order = (new OrderFactory())->getFullOrder();

        // Act
        $result = Livewire::test(OrderEditor::class, ['order' => $order, 'orderCanBeEdited' => false]);

        // Assert
        $result->assertSee('Productos');
    }

    public function testSePuedenEditarLosProductos(): void
    {
        // Arrange
        $order = (new OrderFactory())->getFullOrder();

        // Act
        $result = Livewire::test(OrderEditor::class, ['order' => $order, 'orderCanBeEdited' => true]);

        // Assert
        $result->assertSee('Editar producto');
        $result->assertSee('Eliminar producto');
    }

    public function testAlHacerClicEnEliminarProductoSeEliminaUnProducto(): void
    {
        // Arrange
        $order = (new OrderFactory())->getFullOrder();
        $orderProduct = $order->getProducts()->firstOrFail();

        // Act
        $component = Livewire::test(OrderEditor::class, ['order' => $order, 'orderCanBeEdited' => true]);
        $component->assertSee($orderProduct->getName(), false);
        $component->call('deleteProduct', $orderProduct->getId());

        // Assert
        $component->assertDontSee($orderProduct->getName());
    }

    public function testAlHacerClicEnEditarProductoSeMuestranLasOpcionesParaEditarlo(): void
    {
        // Arrange
        $order = (new OrderFactory())->getFullOrder();
        $orderProduct = $order->getProducts()->firstOrFail();

        // Act
        $component = Livewire::test(OrderEditor::class, ['order' => $order, 'orderCanBeEdited' => true]);
        $component->assertDontSeeHtml('Editar producto "'. $orderProduct->getName() .'"');
        $component->call('editProduct', $orderProduct->getId());

        // Assert
        $component->assertSeeHtml('Editar producto "'. $orderProduct->getName() . '"');
    }

    public function testCuandoSeCancelaLaEdicionDeUnProductoSeOculta(): void
    {
        $order = (new OrderFactory())->getFullOrder();
        $orderProduct = $order->getProducts()->firstOrFail();

        // Act
        $component = Livewire::test(OrderEditor::class, ['order' => $order, 'orderCanBeEdited' => true]);
        $component->call('editProduct', $orderProduct->getId());
        $this->assertCount(1, $component->get('productsBeingEdited'));
        $component->emit('close-order-product-edit', $orderProduct->getId());

        // Assert
        $this->assertCount(0, $component->get('productsBeingEdited'));
    }

    public function testSePuedeEliminarUnProducto(): void
    {
        // Arrange
        $order = (new OrderFactory())->getFullOrder();
        $orderProductId = $order->getProducts()->firstOrFail()->getId();

        // Act
        $component = Livewire::test(OrderEditor::class, ['order' => $order, 'orderCanBeEdited' => true]);
        $component->call('removeProduct', $orderProductId);

        // Assert
        $this->assertFalse(OrderProduct::whereId($orderProductId)->exists());
    }

    public function testSePuedeAnadirUnProductoAlPedido(): void
    {
        // Arrange
        $order = (new OrderFactory())->getFullOrder();

        // Act
        $component = Livewire::test(OrderEditor::class, [
            'order' => $order,
            'orderCanBeEdited' => true,
            'showAddProductForm' => true
        ]);

        // Assert
        $component->assertSee('Añadir producto');
    }

    /**
     * @throws InvalidPriceException
     */
    public function testSePuedenQuitarProductosRegaloDeUnPedidoAlEditarlo(): void
    {
        // Arrange
        $order = (new OrderFactory())->withGiftedProducts()->getFullOrder();
        $giftedProducts = $order->getGiftedProducts();

        // Act
        $component = Livewire::test(OrderEditor::class, [
            'order' => $order,
            'orderCanBeEdited' => true,
            'showAddProductForm' => true
        ]);
        $component->assertSee($giftedProducts[1]->getName(), false);
        $component->call('removeGiftedProduct', $giftedProducts[1]->getId());

        // Assert
        $component->assertDontSee($giftedProducts[1]->getName(), false);
    }

    public function testSeMuestraElNifDeFacturacionEnModoVer(): void
    {
        // Arrange
        $order = (new OrderFactory())->getFullOrder();

        // Act
        $result = Livewire::test(OrderEditor::class, ['order' => $order, 'orderCanBeEdited' => false]);

        // Assert
        $result->assertSee($order->getBillingDetails()->getPersonNif());
    }

    public function testSeMuestraElNifDeFacturacionEnModoEditar(): void
    {
        // Arrange
        $order = (new OrderFactory())->getFullOrder();

        // Act
        $result = Livewire::test(OrderEditor::class, ['order' => $order, 'orderCanBeEdited' => true]);

        // Assert
        $result->assertSee($order->getBillingDetails()->getPersonNif());
    }
}