<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Livewire;

use Bittacora\Bpanel4\Coupons\Database\Factories\FreeProductsCouponFactory;
use Bittacora\Bpanel4\Orders\Http\Livewire\Cart;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Orders\Tests\Integration\Helpers\InitializeBasicCartForClient;
use Bittacora\Bpanel4\Products\Database\Factories\CartProductFactory;
use Bittacora\LaravelTestingHelpers\Views\VendorViewsDisabler;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Tests\TestCase;

final class GiftedProductsCartComponentTest extends TestCase
{
    use RefreshDatabase;

    private CartService $cartService;

    public function setUp(): void
    {
        parent::setUp();
        $initializeBasicCartForClient = $this->app->make(InitializeBasicCartForClient::class);
        ['cartService' => $this->cartService, 'client' => $client] = $initializeBasicCartForClient->execute($this->app);
        $this->cartService->getCart()->addProduct((new CartProductFactory())->createOne(), 1);
        $this->actingAs($client->getUser());
    }

    /**
     * @throws BindingResolutionException
     */
    public function testSeMuestranLosProductosRegaloEnElCarrito(): void
    {
        // Arrange
        VendorViewsDisabler::disableViews();
        $product = (new CartProductFactory())->createOne();
        $this->cartService->getCart()->addGiftedProduct(
            (new FreeProductsCouponFactory())->createOne(),
            $product,
            1
        );

        // Act
        $component = Livewire::test(Cart::class);

        // Assert
        $component->assertSee($product->getName());
    }

    public function testElPrecioSeMuestraComoRegalo(): void
    {
        // Arrange
        VendorViewsDisabler::disableViews();
        $product = (new CartProductFactory())->createOne();
        $this->cartService->getCart()->addGiftedProduct(
            (new FreeProductsCouponFactory())->createOne(),
            $product,
            1
        );

        // Act
        $component = Livewire::test(Cart::class);

        // Assert
        $component->assertSeeHtml('gifted-product-price');
    }

    public function testSiNoHayProductosRegaloNoSeMuestraElApartado(): void
    {
        // Arrange
        VendorViewsDisabler::disableViews();
        $product = (new CartProductFactory())->createOne();

        // Act
        $component = Livewire::test(Cart::class);

        // Assert
        $component->assertDontSeeHtml('gifted-product-price');
    }
}