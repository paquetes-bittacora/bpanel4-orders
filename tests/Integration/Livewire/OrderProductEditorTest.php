<?php

declare(strict_types=1);

namespace Integration\Livewire;

use Bittacora\Bpanel4\Orders\Actions\Order\UpdateOrderProduct;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Http\Livewire\OrderProductEditor;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Mockery;
use Random\RandomException;
use Tests\TestCase;

final class OrderProductEditorTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    public function testSePuedeRenderizarElcomponente(): void
    {
        // Arrange
        $orderProduct = $this->getProduct();

        // Act
        $component = Livewire::test(OrderProductEditor::class, ['orderProduct' => $orderProduct]);

        // Assert
        $component->assertSee('Editar producto "' . $orderProduct->getName() . '"', false);
    }

    /**
     * @throws RandomException
     */
    public function testSePuedeEditarElProducto(): void
    {
        // Arrange
        $orderProduct = $this->getProduct();
        $quantity = random_int(1, 50);
        $unitPrice = random_int(1000, 9999) / 100;
        $this->app->instance(
            UpdateOrderProduct::class,
            $this->getProductUpdateCallMock($orderProduct, $quantity, $unitPrice)
        );

        // Act
        $component = Livewire::test(OrderProductEditor::class, ['orderProduct' => $orderProduct]);
        $component->set('name', 'Nombre editado');
        $component->set('reference', 'referencia-editada');
        $component->set('quantity', $quantity);
        $component->set('paidUnitPrice', $unitPrice);
        $component->call('updateProduct');

        // Assert
        $component->assertEmitted('order-product-updated', $orderProduct->getId());
    }

    public function testEmiteUnEventoCuandoSePulsaElBotonDeCancelar(): void
    {
        // Arrange
        $orderProduct = $this->getProduct();

        // Act
        $component = Livewire::test(OrderProductEditor::class, ['orderProduct' => $orderProduct]);
        $component->call('cancel');

        // Assert
        $component->assertEmitted('close-order-product-edit', $orderProduct->getId());
    }

    private function getProduct(): OrderProduct
    {
        $order = (new OrderFactory())->getFullOrder();
        return $order->getProducts()->firstOrFail();
    }

    private function getProductUpdateCallMock(
        OrderProduct $orderProduct,
        int $quantity,
        float $unitPrice
    ): UpdateOrderProduct|Mockery\MockInterface|Mockery\LegacyMockInterface {
        $updateOrderProductMock = Mockery::mock(UpdateOrderProduct::class);
        $updateOrderProductMock->shouldReceive('execute')->once()
            ->withArgs(function (...$args) use ($orderProduct, $quantity, $unitPrice) {
                return $args[0]->getId() === $orderProduct->getId() &&
                    $args[1] === 'Nombre editado' &&
                    $args[2] === 'referencia-editada' &&
                    $args[3] === $quantity &&
                    $args[4]->toFloat() === $unitPrice;
            });
        return $updateOrderProductMock;
    }

}