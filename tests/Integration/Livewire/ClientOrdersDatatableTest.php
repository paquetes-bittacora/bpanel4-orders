<?php

/** @noinspection PhpRedundantVariableDocTypeInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Livewire;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Http\Livewire\ClientOrdersDatatable;
use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Clients\Models\Client;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;

use function random_int;

use Tests\TestCase;

final class ClientOrdersDatatableTest extends TestCase
{
    use RefreshDatabase;

    public function testListaLosPedidosDelCliente(): void
    {
        $client = (new ClientFactory())->createOne();
        $this->actingAs($client->getUser());

        $this->createNOrdersForClient($client, 4);

        $component = Livewire::test(ClientOrdersDatatable::class);
        self::assertEquals(4, $component->get('orderCount'));
    }

    /**
     * @throws Exception
     */
    public function testNoListaLosPedidosDeOtrosClientes(): void
    {
        $numberOfClientOrders = random_int(1, 10);
        $client = (new ClientFactory())->createOne();
        $this->actingAs($client->getUser());
        $this->createNOrdersForClient($client, $numberOfClientOrders);

        $this->createOrdersForOtherClients();

        $component = Livewire::test(ClientOrdersDatatable::class);
        self::assertEquals($numberOfClientOrders, $component->get('orderCount'));
    }

    private function createNOrdersForClient(Client $client, int $numberOfOrders): void
    {
        for ($i = 0; $i < $numberOfOrders; ++$i) {
            $order = (new OrderFactory())->getFullOrder();
            $order->client->client_id = $client->getClientId();
            $order->client->save();
        }
    }

    /**
     * @throws Exception
     */
    private function createOrdersForOtherClients(): void
    {
        $numberOfOrders = random_int(5, 10);
        for ($i = 0; $i < $numberOfOrders; ++$i) {
            (new OrderFactory())->getFullOrder();
        }
    }
}
