<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Livewire;

use Bittacora\Bpanel4\Addresses\Database\Factories\ModelAddressFactory;
use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Coupons\Services\CartDiscountCalculator;
use Bittacora\Bpanel4\Orders\Database\Factories\TestClientFactory;
use Bittacora\Bpanel4\Orders\Http\Livewire\Cart;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart as CartModel;
use Bittacora\Bpanel4\Orders\Tests\Integration\Models\TestClient;
use Bittacora\Bpanel4\Orders\Tests\Integration\Models\TestProduct;
use Bittacora\Bpanel4\Payment\Payment;
use Bittacora\Bpanel4\Payment\Tests\Factories\PaymentMethodMockFactory;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Tests\Feature\ObjectMothers\VatRateObjectMother;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Livewire;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

final class CartComponentTest extends TestCase
{
    use RefreshDatabase;

    private Livewire\Testing\TestableLivewire $cartComponent;

    protected function setUp(): void
    {
        parent::setUp();
        $client = (new TestClientFactory())->has((new ModelAddressFactory()), 'addresses')->createOne();
        $this->actingAs($client->getUser());
        $this->mockClientService($client);
        $this->mockPaymentModule();

        $this->mock(CartDiscountCalculator::class, function ($mock) {
            $mock->shouldReceive('getDiscountByUnit')->andReturn([]);
            $mock->shouldReceive('getTotalDiscount')->andReturn(0);
        });

        $this->cartComponent = Livewire::test(Cart::class, [
            'shippingOption' => 'null-shipping-option',
            'shippingAddress' => $client->getShippingAddress()->getId(),
            'billingAddress' => $client->getBillingAddress()->getId(),
        ]);
    }

    /**
     * Selecciono distintos métodos de pago y compruebo que se muestren como seleccionados
     */
    public function testSePuedeSeleccionarUnaFormaDePago(): void
    {
        $client = $this->actingAsARandomClient();

        /** @var ClientService&MockInterface $clientService */
        $clientService = resolve(ClientService::class);
        /** @phpstan-ignore-next-line */
        $clientService->getClientCart()->shouldReceive('setPaymentMethod')->times(3); // 2 de este test + 1 desde mount

        /** @var Cart&Livewire\LivewireManager $cartComponent */
        $cartComponent = $this->getCartComponentInPreviewMode($client);

        $cartComponent->call('setSelectedPaymentMethod', 1);
        $cartComponent->assertSee('Formas de pago');
        $cartComponent->assertSeeHtmlInOrder(['payment-method', 'payment-method  selected']);

        $cartComponent->call('setSelectedPaymentMethod', 0);
        $cartComponent->assertSee('Formas de pago');
        $cartComponent->assertSeeHtmlInOrder(['payment-method  selected', 'payment-method']);
    }

    public function testMuestraLasInstruccionesDelMetodoDePago(): void
    {
        $client = $this->actingAsARandomClient();

        /** @var Cart&Livewire\LivewireManager $cartComponent */
        $cartComponent = $this->getCartComponentInPreviewMode($client);

        $cartComponent->call('setSelectedPaymentMethod', 1);
        $cartComponent->assertSee('Instrucciones de la forma de pago');
    }

    public function testMuestraBotonParaSeleccionarMetodoDeEnvioSiNoEsPasoVistaPrevia(): void
    {
        $this->cartComponent->set('isPreview', false);
        $this->cartComponent->assertSee('Seleccionar envío y forma de pago');
    }

    public function testMuestraBotonParaConfirmarPedidoSiEsPasoVistaPrevia(): void
    {
        $this->cartComponent->set('isPreview', true);
        $this->cartComponent->assertSee('Confirmar pedido');
    }

    public function testMuestraBotonesCorrectosSiElUsuarioNoHaIniciadoSesion(): void
    {
        Auth::logout();
        $cartComponent = Livewire::test(Cart::class, [
            'shippingOption' => 'null-shipping-option',
        ]);
        $cartComponent->assertSee('continuar como invitado');
    }

    private function actingAsARandomClient(): TestClient
    {
        $client = (new TestClientFactory())->has((new ModelAddressFactory()), 'addresses')->createOne();
        $this->actingAs($client->getUser());
        return $client;
    }

    private function mockClientService(TestClient $client): void
    {
        $this->mock(ClientService::class, static function (MockInterface $mock) use ($client): void {
            /** @var MockInterface&Cart $cart */
            $cart = Mockery::mock(CartModel::class)->shouldIgnoreMissing();
            $cart->shouldReceive('getShippingAddress')->andReturn($client->getShippingAddress());
            $cart->shouldReceive('getBillingAddress')->andReturn($client->getShippingAddress());
            $cart->shouldReceive('getSelectedShippingMethod')->andReturn('');
            $cart->shouldReceive('getSubtotalWithTaxes')->andReturn(new Price(10));
            $cart->shouldReceive('getCoupons')->andReturn(new Collection([]));

            /** @var MockInterface&Product $product */
            $product = Mockery::mock(TestProduct::class)->shouldIgnoreMissing();
            $product->shouldReceive('getProductId')->andReturn(1);
            $product->shouldReceive('getUnitPrice')->andReturn(new Price(10));
            $product->shouldReceive('getVatRate')->andReturn((new VatRateObjectMother())->createVatRate());
            $product->shouldReceive('getQuantity')->andReturn(1);
            $product->shouldReceive('getCartableProductId')->andReturn(1);
            $product->shouldReceive('getImages')->andReturn(new Collection());
            $collection = new Collection([$product]);
            $cart->shouldReceive('products->get')->andReturn($collection);
            $cart->shouldReceive('getProducts')->andReturn($collection);
            $cart->shouldReceive('getGiftedProducts')->andReturn(new Collection([]));
            $mock->shouldReceive('getClientCart')->andReturn($cart);
            $mock->shouldReceive('getCurrentClient')->andReturn($client);
        });
    }

    private function mockPaymentModule(): void
    {
        $this->mock(Payment::class, static function (MockInterface $mock): void {
            $mock->shouldReceive('getPaymentMethodsForUser')->andReturn([
                PaymentMethodMockFactory::new()->getMock(),
                PaymentMethodMockFactory::new()->getMock(),
            ]);
        });
    }

    /**
     * @phpstan-return Livewire\Testing\TestableLivewire&Cart
     */
    private function getCartComponentInPreviewMode(TestClient $client): Livewire\Testing\TestableLivewire
    {
        return Livewire::test(Cart::class, [
            'shippingOption' => 'null-shipping-option',
            'isPreview' => true,
            'shippingAddress' => $client->getShippingAddress()->getId(),
            'billingAddress' => $client->getBillingAddress()->getId(),
        ]);
    }
}
