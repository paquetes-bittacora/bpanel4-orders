<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Livewire;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Http\Livewire\OrderProductCreator;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Livewire;
use Random\RandomException;
use Tests\TestCase;
use function random_int;

final class OrderProductCreatorTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @throws RandomException
     */
    public function testSeCreaUnOrderProduct(): void
    {
        // Arrange
        $order = (new OrderFactory())->createOne();
        $suffix = Str::random(4);
        $price = random_int(1, 1000) * 1000; // Creo el precio así para evitar errores de precisión en el assert
        $quantity = random_int(1, 100);
        $vatRate = 10.00;
        (new VatRateFactory())->withRate((int)$vatRate)->createOne();
        $taxAmount = (int)(($price * $quantity) * $vatRate / 100);

        // Act
        $component = Livewire::test(OrderProductCreator::class);
        $component->set('orderId', $order->getId());
        $component->set('name', 'Producto creado ' . $suffix);
        $component->set('reference', 'referencia-' . $suffix);
        $component->set('unitPrice', $price / 10000);
        $component->set('quantity', $quantity);
        $component->set('taxRate', $vatRate);
        $component->call('createProduct');

        // Assert
        $this->assertDatabaseHas(OrderProduct::class, [
            'order_id' => $order->getId(),
            'name' => 'Producto creado ' . $suffix,
            'reference' => 'referencia-' . $suffix,
            'unit_price' => $price,
            'quantity' => $quantity,
            'tax_rate' => $vatRate,
            'tax_amount' => $taxAmount,
            'total' => $price * $quantity,
        ]);
        $component->assertEmitted('order-product-created');
    }
}