<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Livewire;

use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Orders\Database\Factories\TestClientFactory;
use Bittacora\Bpanel4\Orders\Http\Livewire\CartAddresses;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Tests\Integration\Models\TestClient;
use Bittacora\Bpanel4\Addresses\Database\Factories\ModelAddressFactory;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use JsonException;
use Livewire;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

final class CartAddressesTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function testMuestraElFormularioParaCrearUnaNuevaDireccionDeEnvio(): void
    {
        $this->getLivewireComponent()->set('selectedShippingAddress', 0)->assertSee('Nueva dirección de envío');
    }

    public function testNoMuestraElFormularioParaCrearDireccionDeEnvioPorDefecto(): void
    {
        $this->getLivewireComponent()->assertDontSee('Nueva dirección de envío');
    }

    public function testNoMuestraElFormularioDeDireccionDeFacturacionSiSeraLaMismaQueLaDeEnvio(): void
    {
        $this->getLivewireComponent()
            ->set('billingAddressIsTheSameAsShipping', true)
            ->assertDontSeeHtml('Dirección de facturación</h2>');
    }

    public function testMuestraElFormularioDeDireccionDeFacturacionSiNoEsLaMismaQueLaDeEnvio(): void
    {
        $this->getLivewireComponent()
            ->set('billingAddressIsTheSameAsShipping', false)
            ->assertSeeHtml('Dirección de facturación</h2>');
    }

    public function testNoMuestraElFormularioParaCrearDireccionDeFacturacionPorDefecto(): void
    {
        $this->getLivewireComponent()
            ->set('billingAddressIsTheSameAsShipping', false)
            ->assertDontSeeHtml('Nueva dirección de facturación</h3>');
    }

    public function testMuestraElFormularioParaCrearUnaNuevaDireccionDeFacturacion(): void
    {
        $this->getLivewireComponent()
            ->set('billingAddressIsTheSameAsShipping', false)
            ->set('selectedBillingAddress', 0)
            ->assertSeeHtml('Nueva dirección de facturación</h3>');
    }

    /**
     * Hago esta prueba porque al probar manualmente he visto que fallaba.
     */
    public function testSePuedeCrearUnaNuevaDireccionDeEnvioYOtraDeFacturacionALaVez(): void
    {
        $this->getLivewireComponent()
            ->set('billingAddressIsTheSameAsShipping', false)
            ->set('selectedBillingAddress', 0)
            ->set('selectedShippingAddress', 0)
            ->assertSeeHtml('Nueva dirección de envío</h3>')
            ->assertSeeHtml('Nueva dirección de facturación</h3>');
    }

    public function testMuestraUnaNotificacionSiNoPuedeCrearDireccionDeEnvio(): void
    {
        $this->getLivewireComponent()
            ->call('processShippingAddressForm', 'json inválido')
            ->assertDispatchedBrowserEvent('error-saving-address');
    }

    /**
     * @throws JsonException
     * @throws Exception
     */
    public function testCreaUnaNuevaDireccionDeEnvioYLaEstableceComoSeleccionada(): void
    {
        $this->getLivewireComponent()
            ->call('processShippingAddressForm', json_encode([
                'country' => 1,
                'state' => 1,
                'name' => 'Prueba',
                'address' => 'Prueba',
                'location' => 'Prueba',
                'postal_code' => '01011',
            ], JSON_THROW_ON_ERROR))
            ->assertDispatchedBrowserEvent('address-saved');
    }

    private function getClient(): TestClient
    {
        return (new TestClientFactory())->has((new ModelAddressFactory())->count(2), 'addresses')->create();
    }

    private function getLivewireComponent(): Livewire\Testing\TestableLivewire
    {
        $this->mock(ClientService::class, static function (MockInterface $mock): void {
            $cartMock = Mockery::mock(Cart::class);
            $cartMock->shouldReceive('getShippingAddress->getId')->andReturn(1);
            $cartMock->shouldReceive('getBillingAddress->getId')->andReturn(1);
            $mock->shouldReceive('getClientCart')->andReturn($cartMock);
        });
        return Livewire::test(CartAddresses::class, ['client' => $this->getClient()]);
    }
}
