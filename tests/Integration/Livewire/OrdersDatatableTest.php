<?php

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Livewire;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Http\Livewire\OrdersDatatable;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Livewire\Testing\TestableLivewire;
use Tests\TestCase;

final class OrdersDatatableTest extends TestCase
{
    use RefreshDatabase;

    public function testListaTodosLosPedidos(): void
    {
        $orders = $this->getOrders(5);

        /** @var TestableLivewire&OrdersDatatable $component */
        $component = Livewire::test(OrdersDatatable::class);

        foreach ($orders as $order) {
            $component->assertSee($order->getClient()->getName());
        }
    }

    public function testMuestraElNombreYLosApellidosDelCliente(): void
    {
        $orders = $this->getOrders(1);

        /** @var TestableLivewire&OrdersDatatable $component */
        $component = Livewire::test(OrdersDatatable::class);

        $component->assertSee($orders[0]->getClient()->getName());
        $component->assertSee($orders[0]->getClient()->getSurname());
    }

    public function testMuestraElNumeroDePedido(): void
    {
        $orders = $this->getOrders(1);

        /** @var TestableLivewire&OrdersDatatable $component */
        $component = Livewire::test(OrdersDatatable::class);

        $component->assertSeeHtml($orders[0]->getId());
    }

    /**
     * @throws InvalidPriceException
     */
    public function testMuestraElImporteTotal(): void
    {
        $orders = $this->getOrders(1);

        /** @var TestableLivewire&OrdersDatatable $component */
        $component = Livewire::test(OrdersDatatable::class);

        $component->assertSeeHtml(Price::fromInt($orders[0]->getOrderTotal()));
    }

    public function testMuestraLaFechaDelPedido(): void
    {
        $orders = $this->getOrders(1);

        /** @var TestableLivewire&OrdersDatatable $component */
        $component = Livewire::test(OrdersDatatable::class);

        $component->assertSeeHtml($orders[0]->getDate()->format('d-m-Y H:i:s'));
    }

    /* No consigo que funcione con livewire tables v2
    public function testFiltraPorNombreDelCliente(): void
    {
        $orders = $this->getOrders(5);

        /** @var TestableLivewire&OrdersDatatable $component *\/
        $component = Livewire::test(OrdersDatatable::class);
        $name = $orders[0]->getClient()->getName();
        $component->call('setSearch', $name);

        $component->assertSeeHtml($name);
        $component->assertDontSeeHtml($orders[1]->getClient()->getName());
    }*/

    public function testMuestraElEstadoDelPedido(): void
    {
        $this->getOrders(1);

        /** @var TestableLivewire&OrdersDatatable $component */
        $component = Livewire::test(OrdersDatatable::class);

        $component->assertSeeHtml('Estado del pedido');
    }

    public function testMuestraLasAccionesDelPedido(): void
    {
        $orders = $this->getOrders(5);

        /** @var TestableLivewire&OrdersDatatable $component */
        $component = Livewire::test(OrdersDatatable::class);

        for ($i = 0; $i < 5; $i++) {
            $id = $orders[$i]->getId();
            $component->assertSeeHtml('bpanel/pedidos/' . $id . '/editar"');
            $component->assertSeeHtml('bpanel/pedidos/' . $id . '/eliminar');
        }
    }

    /**
     * @return Order[]
     */
    private function getOrders(int $numberOfOrders): array
    {
        /** @var Order[] $orders */
        $orders = [];

        for ($i = 0; $i < $numberOfOrders; $i++) {
            $orders[] = (new OrderFactory())->getFullOrder();
        }
        return $orders;
    }
}
