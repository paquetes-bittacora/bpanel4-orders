<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Livewire;

use Bittacora\Bpanel4\Orders\Database\Factories\CartFactory;
use Bittacora\Bpanel4\Orders\Http\Livewire\SelectShipping;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethodPriceCalculator;
use Bittacora\Bpanel4\Shipping\Services\ShippingCostsCalculator;
use Bittacora\Bpanel4\Shipping\Types\ShippingOption;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

final class SelectShippingTest extends TestCase
{
    use RefreshDatabase;

    private Livewire\Testing\TestableLivewire $component;

    public function setUp(): void
    {
        parent::setUp();

        $cart = (new CartFactory())->createOne();
        $cart->selected_shipping_method = '';

        $this->mock(ShippingCostsCalculator::class, function (MockInterface $mock): void {
            $mock->shouldReceive('getAvailableOptions')->andReturn([
                new ShippingOption(
                    $this->getFreeShippingMock(),
                    Mockery::mock(ShippingMethodPriceCalculator::class)
                ),
                new ShippingOption(
                    $this->getFixedPriceMock(),
                    Mockery::mock(ShippingMethodPriceCalculator::class)
                ),
            ]);
        });

        $this->component = Livewire::test(SelectShipping::class, ['cart' => $cart]);
    }

    public function testMuestraLasOpcionesDeEnvioDisponibles(): void
    {
        $this->component->assertSee('Envío gratis');
        $this->component->assertSee('Precio fijo');
    }

    /** @return MockInterface&ShippingMethod */
    private function getFreeShippingMock(): MockInterface
    {
        $freeShippingMock = Mockery::mock(ShippingMethod::class);
        $freeShippingMock->shouldReceive('getName')->andReturn('Envío gratis');
        $freeShippingMock->shouldIgnoreMissing();
        return $freeShippingMock;
    }

    /** @return MockInterface&ShippingMethod */
    private function getFixedPriceMock(): MockInterface
    {
        $freeShippingMock = Mockery::mock(ShippingMethod::class);
        $freeShippingMock->shouldReceive('getName')->andReturn('Precio fijo');
        $freeShippingMock->shouldIgnoreMissing();
        return $freeShippingMock;
    }
}
