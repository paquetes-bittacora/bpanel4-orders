<?php

/** @noinspection PhpRedundantVariableDocTypeInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Livewire;

use Bittacora\Bpanel4\Addresses\Database\Factories\ModelAddressFactory;
use Bittacora\Bpanel4\Clients\Contracts\Client;
use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Coupons\Services\CartDiscountCalculator;
use Bittacora\Bpanel4\Orders\Database\Factories\TestClientFactory;
use Bittacora\Bpanel4\Orders\Http\Livewire\Cart;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart as CartModel;
use Bittacora\Bpanel4\Orders\Tests\Integration\Models\TestProduct;
use Bittacora\Bpanel4\Payment\Payment;
use Bittacora\Bpanel4\Payment\Tests\Factories\PaymentMethodMockFactory;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Tests\Feature\ObjectMothers\VatRateObjectMother;
use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Livewire\Testing\TestableLivewire;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

final class FreeOrderCartComponentTest extends TestCase
{
    use RefreshDatabase;

    private ?Client $client = null;

    public function testNoPermiteTerminarElPedidoSiElImporteTotalEs0(): void
    {
        $this->mockClientService(0);
        $this->actingAs((new ClientFactory())->createOne()->getUser());
        $this->mockPaymentModule();

        $this->mock(CartModel::class, function ($mock) {
            $mock->shouldReceive('getCoupons')->andReturn(new Collection([]));
        });

        $this->mock(CartDiscountCalculator::class, function ($mock) {
            $mock->shouldReceive('getDiscountByUnit')->andReturn([]);
            $mock->shouldReceive('getTotalDiscount')->andReturn(0);
        });

        /** @var TestableLivewire $cartComponent */
        $cartComponent = Livewire::test(Cart::class, [
            'shippingOption' => 'null-shipping-option',
            'shippingAddress' => $this->getClient()->getShippingAddress()->getId(),
            'billingAddress' => $this->getClient()->getBillingAddress()->getId(),
        ]);

        $cartComponent->set('isPreview', true);
        $cartComponent->assertSee('No se puede completar el pedido porque el importe es 0');
        $cartComponent->assertDontSee(route('order.confirm'));
    }

    protected function getClient(): Client
    {
        if (null !== $this->client) {
            return $this->client;
        }
        $this->client = (new TestClientFactory())->has((new ModelAddressFactory()), 'addresses')
            ->createOne();
        return $this->client;
    }

    private function mockClientService(float $amount = 10): void
    {
        $client = $this->getClient();
        $this->mock(ClientService::class, static function (MockInterface $mock) use ($client, $amount): void {
            /** @var MockInterface&Cart $cart */
            $cart = Mockery::mock(CartModel::class)->shouldIgnoreMissing();
            $cart->shouldReceive('getShippingAddress')->andReturn($client->getShippingAddress());
            $cart->shouldReceive('getBillingAddress')->andReturn($client->getShippingAddress());
            $cart->shouldReceive('getSelectedShippingMethod')->andReturn('');
            $cart->shouldReceive('getSubtotalWithTaxes')->andReturn(new Price(0));
            $cart->shouldReceive('getCoupons')->andReturn(new Collection([]));

            /** @var MockInterface&Product $product */
            $product = Mockery::mock(TestProduct::class)->shouldIgnoreMissing();
            $product->shouldReceive('getProductId')->andReturn(1);
            $product->shouldReceive('getUnitPrice')->andReturn(new Price($amount));
            $product->shouldReceive('getVatRate')->andReturn((new VatRateObjectMother())->createVatRate());
            $product->shouldReceive('getQuantity')->andReturn(1);
            $product->shouldReceive('getCartableProductId')->andReturn(1);
            $product->shouldReceive('getImages')->andReturn(new Collection());
            $collection = new Collection([$product]);
            $cart->shouldReceive('products->get')->andReturn($collection);
            $cart->shouldReceive('getProducts')->andReturn($collection);
            $cart->shouldReceive('getGiftedProducts')->andReturn(new Collection([]));
            $mock->shouldReceive('getClientCart')->andReturn($cart);
            $mock->shouldReceive('getCurrentClient')->andReturn($client);
        });
    }

    private function mockPaymentModule(): void
    {
        $this->mock(Payment::class, static function (MockInterface $mock): void {
            $mock->shouldReceive('getPaymentMethodsForUser')->andReturn([
                PaymentMethodMockFactory::new()->getMock(),
                PaymentMethodMockFactory::new()->getMock(),
            ]);
        });
    }
}
