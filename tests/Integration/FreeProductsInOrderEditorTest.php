<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4Users\Tests\Helpers\AdminHelper;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Comprueba la integración de los cupones tipo "productos regalo" con la vista de edición de pedidos.
 */
final class FreeProductsInOrderEditorTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    /**
     * @throws InvalidPriceException
     */
    public function testSeMuestranLosProductosRegaloAlEditarUnPedido(): void
    {
        // Arrange
        $order = $this->getOrderWithGiftedProducts();
        $giftedProducts = $order->getGiftedProducts();

        // Act
        $result = $this->get(route('orders-admin.edit', $order->id));

        // Assert
        foreach ($giftedProducts as $giftedProduct) {
            $result->assertSee($giftedProduct->name, false);
        }
    }

    /**
     * @throws InvalidPriceException
     */
    private function getOrderWithGiftedProducts(): Order
    {
        (new AdminHelper())->actingAsAdminWithPermissions(['orders-admin.edit']);
        return (new OrderFactory())->withGiftedProducts()->getFullOrder();
    }
}