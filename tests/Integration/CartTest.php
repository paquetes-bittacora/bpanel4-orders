<?php

/** @noinspection UnnecessaryAssertionInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration;

use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Database\Factories\TestClientFactory;
use Bittacora\Bpanel4\Orders\Exceptions\CartAlreadyExistsException;
use Bittacora\Bpanel4\Orders\Tests\Integration\Factories\CartableProductFactory;
use Bittacora\Bpanel4\Orders\Tests\Integration\Factories\CartFactory;
use Bittacora\Bpanel4\Orders\Tests\Integration\Models\TestProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Addresses\Database\Factories\ModelAddressFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class CartTest extends TestCase
{
    use RefreshDatabase;

    private CartFactory $cartFactory;
    private CartableProductFactory $productFactory;

    public function setUp(): void
    {
        parent::setUp();

        $this->cartFactory = $this->app->make(CartFactory::class);
        $this->productFactory = $this->app->make(CartableProductFactory::class);
        config([
            'orders.product_class' => TestProduct::class,
        ]);
    }

    /**
     * @throws CartAlreadyExistsException
     */
    public function testSePuedenObtenerLosDatosDelCliente(): void
    {
        $cart = $this->cartFactory->createCart();
        /** @phpstan-ignore-next-line  */
        self::assertInstanceOf(CartableClient::class, $cart->getClient());
    }

    /**
     * @throws InvalidPriceException
     * @throws CartAlreadyExistsException
     */
    public function testSePuedenAnadirProductosAlCarrito(): void
    {
        $cart = $this->cartFactory->createCart();
        $product = $this->productFactory->create();

        $cart->addProduct($product, 1);

        $products = $cart->getProducts();

        self::assertInstanceOf(CartableProduct::class, $products->first());
    }


    /**
     * @throws InvalidPriceException
     * @throws CartAlreadyExistsException
     */
    public function testSiSeAnadeElMismoProductoSeSumaCantidad(): void
    {
        $cart = $this->cartFactory->createCart();
        $product = $this->productFactory->create();

        $cart->addProduct($product, 1);
        $cart->addProduct($product, 4);

        self::assertEquals(5, $cart->getProducts()[0]->pivot->quantity);
    }

    /**
     * @throws InvalidPriceException
     * @throws CartAlreadyExistsException
     */
    public function testSePuedeCambiarLaCantidadDeUnProducto(): void
    {
        $cart = $this->cartFactory->createCart();
        $product = $this->productFactory->create();

        $cart->addProduct($product, 1);
        self::assertEquals(1, $cart->getProducts()[0]->pivot->quantity); // Compruebo la cant. inicial

        $cart->setProductQuantity($product, 3);
        self::assertEquals(3, $cart->getProducts()[0]->pivot->quantity);
    }

    /**
     * @throws InvalidPriceException
     * @throws CartAlreadyExistsException
     */
    public function testCalculaLaCantidadDeProductos(): void
    {
        $cart = $this->cartFactory->createCart();
        $product = $this->productFactory->create();

        $cart->addProduct($product, 1);
        $cart->addProduct($product, 4);

        self::assertEquals(5, $cart->getItemCount());
    }

    /**
     * @throws CartAlreadyExistsException
     */
    public function testSePuedeEstablecerLaDireccionDeEnvio(): void
    {
        $cart = $this->cartFactory->createCart();

        $address = (new ModelAddressFactory())->for((new TestClientFactory()), 'addressable')->createOne();
        $addressId = $address->getId();

        $cart->setShippingAddress($address);
        $cart->refresh();

        self::assertEquals($addressId, $cart->getShippingAddress()->getId());
    }

    /**
     * @throws CartAlreadyExistsException
     */
    public function testSePuedeEstablecerLaDireccionDeFacturacion(): void
    {
        $cart = $this->cartFactory->createCart();

        $address = (new ModelAddressFactory())->for((new TestClientFactory()), 'addressable')->createOne();
        $addressId = $address->getId();

        $cart->setBillingAddress($address);
        $cart->refresh();

        self::assertEquals($addressId, $cart->getBillingAddress()->getId());
    }
}
