<?php

/** @noinspection UnnecessaryAssertionInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration;

use Bittacora\Bpanel4\Addresses\Database\Factories\ModelAddressFactory;
use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Coupons\Services\CartDiscountCalculator;
use Bittacora\Bpanel4\Orders\Actions\Cart\AddProductToCart;
use Bittacora\Bpanel4\Orders\Exceptions\CartAlreadyExistsException;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Orders\Tests\Integration\Factories\CartableProductFactory;
use Bittacora\Bpanel4\Orders\Tests\Integration\Factories\CartFactory;
use Bittacora\Bpanel4\Orders\Tests\Integration\Factories\CartServiceMockFactory;
use Bittacora\Bpanel4\Orders\Tests\Integration\Factories\ShippingOptionMockFactory;
use Bittacora\Bpanel4\Orders\Tests\Integration\Helpers\InitializeBasicCartForClient;
use Bittacora\Bpanel4\Orders\Tests\Integration\Models\TestProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Models\CartProduct;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\Products\Presenters\ProductPresenterFactory;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethodPriceCalculator;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Exceptions\ShippingOptionNotFoundException;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FreeShipping;
use Bittacora\Bpanel4\Shipping\Services\ShippingCostsCalculator;
use Bittacora\Bpanel4\Shipping\Types\ShippingOption;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Bpanel4\Vat\Services\ShippingVatService;
use Bittacora\Bpanel4\Vat\Services\VatRegionService;
use Bittacora\Bpanel4\Vat\Services\VatService;
use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Bittacora\Bpanel4\Products\Database\Factories\CartProductFactory;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Mockery;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Random\RandomException;
use Tests\TestCase;
use function random_int;

final class CartServiceTest extends TestCase
{
    use RefreshDatabase;
    use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

    private CartFactory $cartFactory;
    private CartableProductFactory $productFactory;
    private ShippingCostsCalculator $shippingCostsCalculator;
    private VatService $vatService;

    public function setUp(): void
    {
        parent::setUp();
        $this->cartFactory = $this->app->make(CartFactory::class);
        $this->shippingCostsCalculator = $this->app->make(ShippingCostsCalculator::class);
        $this->vatService = $this->app->make(VatService::class);
        $this->productFactory = $this->app->make(CartableProductFactory::class);
        $spain = Country::create(['name' => 'España']);
        State::create(['country_id' => $spain->id, 'name' => 'Badajoz']);

        /** @phpstan-ignore-next-line */
        $this->app->make('config')->set(['orders.product_class' => TestProduct::class]);
    }

    /**
     * @throws InvalidPriceException
     * @throws CartAlreadyExistsException
     */
    public function testCalculaElTotalDeImpuestos(): void
    {
        $cart = $this->cartFactory->getCartWith2Products();
        $cartService = new CartService(
            $cart,
            $this->shippingCostsCalculator,
            $this->vatService,
            $this->getCartDiscountCalculator(),
            $this->getShippingVatServiceMock(),
        );
        self::assertEquals(33100, $cartService->getTotalTaxes()->toInt());
    }

    /**
     * Test de regresión
     * @throws InvalidPriceException
     * @throws CartAlreadyExistsException
     */
    public function testCalculaElTotalDeImpuestosRedondeandoAlAlzaSiElUltimoDecimalEs5(): void
    {
        $cart = $this->cartFactory->getCartWithAProductWithPriceAndVat(1, 93.55, 10);
        $cartService = new CartService(
            $cart,
            $this->shippingCostsCalculator,
            $this->vatService,
            $this->getCartDiscountCalculator(),
            $this->getShippingVatServiceMock(),
        );
        self::assertEquals('9.36 €', $cartService->getTotalTaxes()->toString());
    }

    /**
     * @throws CartAlreadyExistsException
     * @throws InvalidPriceException
     */
    public function testElSubtotalEsIgualASubtotalSinIvaMasElIvaTotalCalculado(): void
    {
        $cart = $this->cartFactory->getCartWith2Products();
        $cartService = new CartService(
            $cart,
            $this->shippingCostsCalculator,
            $this->vatService,
            $this->getCartDiscountCalculator(),
            $this->getShippingVatServiceMock(),
        );

        self::assertEquals(
            $cartService->getSubtotalWithTaxes()->toInt(),
            $cartService->getSubtotal()->toInt() + $cartService->getTotalTaxes()->toInt()
        );
    }

    /**
     * @throws InvalidPriceException
     * @throws CartAlreadyExistsException
     */
    public function testCalculaElTotal(): void
    {
        $cart = $this->cartFactory->createCart();
        $product = $this->productFactory->create();
        $product2 = $this->productFactory->create();

        $cart->addProduct($product, 1);
        $cart->addProduct($product2, 2);

        $cartService = new CartService(
            $cart,
            $this->shippingCostsCalculator,
            $this->vatService,
            $this->getCartDiscountCalculator(),
            $this->getShippingVatServiceMock(),
        );

        self::assertEquals(
            $product->getOriginalUnitPrice()->toInt() + 2 * $product2->getOriginalUnitPrice()->toInt(),
            $cartService->getSubtotal()->toInt()
        );
    }

    /**
     * @throws CartAlreadyExistsException
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function testDelegaLaObtencionDeOpcionesDeEnvioDisponiblesEnShippingCostsCalculator(): void
    {
        $cart = $this->cartFactory->createCart();
        /** @var ShippingCostsCalculator&LegacyMockInterface $shippingCostsCalculator */
        $shippingCostsCalculator = Mockery::mock(ShippingCostsCalculator::class);
        $shippingCostsCalculator->shouldReceive('getAvailableOptions')->once();
        $cartService = new CartService(
            $cart,
            $shippingCostsCalculator,
            $this->vatService,
            $this->getCartDiscountCalculator(),
            $this->getShippingVatServiceMock(),
        );
        $cartService->getAvailableShippingOptions();
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws ShippingOptionNotFoundException
     */
    public function testDelegaElCalculoDeLosGastosDeEnvioEnShippingCostsCalculator(): void
    {
        $shippingCostsCalculator = Mockery::mock(ShippingCostsCalculator::class);
        $shippingCostsCalculator->shouldReceive('calculateShippingCosts')->once()->andReturnNull();
        $cart = Mockery::mock(Cart::class)->makePartial();
        $cartService = Mockery::mock(
            CartService::class,
            [
                $cart,
                $shippingCostsCalculator,
                Mockery::mock(VatService::class),
                $this->getCartDiscountCalculator(),
                $this->getShippingVatServiceMock(),
            ],
        );
        $cartService->makePartial();
        $cartService->shouldReceive('getSelectedShippingMethod')->andReturn(new FreeShipping());
        $cartService->getShippingCosts();
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws ShippingOptionNotFoundException
     * @throws InvalidPriceException
     */
    public function testObtieneElTotalDelCarrito(): void
    {
        $mock = Mockery::mock(Cart::class);
        $mock->shouldIgnoreMissing();
        $cartService = Mockery::mock(CartService::class, [
            $mock,
            Mockery::mock(ShippingCostsCalculator::class),
            Mockery::mock(VatService::class),
            $this->getCartDiscountCalculator(),
            $this->getShippingVatServiceMock(),
        ])->makePartial();
        $cartService->shouldReceive('getSubtotalWithTaxes')->andReturn(new Price(20));
        $cartService->shouldReceive('getShippingCosts')->andReturn(new Price(10));
        /** @var CartService $cartService */
        self::assertEquals(30, $cartService->getCartTotal()->toFloat());
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function testObtieneElMetodoDeEnvioSeleccionado(): void
    {
        /** @var MockInterface|Cart $cartMock */
        $cartMock = Mockery::mock(Cart::class)->shouldIgnoreMissing();

        /** @var CartService|MockInterface $cartService */
        $cartService = $this->getCartServiceMock($cartMock);

        $option = $cartService->getAvailableShippingOptions()[1]->shippingMethod::class;

        $cartMock->shouldReceive('getSelectedShippingMethod')
            ->andReturn($option. '-0');

        self::assertEquals(
            $option,
            $cartService->getSelectedShippingMethod()::class
        );
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function testDevuelveunMetodoDeEnvioPorDefecto(): void
    {
        /** @var MockInterface|Cart $cartMock */
        $cartMock = Mockery::mock(Cart::class)->shouldIgnoreMissing();
        $cartMock->shouldReceive('getSelectedShippingMethod')->andReturn('');

        /** @var CartService|MockInterface $cartService */
        $cartService = $this->getCartServiceMock($cartMock);

        self::assertEquals(
            $cartService->getAvailableShippingOptions()[0]->shippingMethod::class,
            $cartService->getSelectedShippingMethod()::class
        );
    }

    /**
     * Los tests de esta parte los hice después del código y por eso son un jaleo.
     * Lo que se hace es llamar a getSelectedShippingMethod cuando la opción marcada
     * como seleccionada está disponible, y se comprueba que la devuelve, y después
     * se quita esa opción, que sigue estando marcada como seleccionada, y se comprueba
     * que como ya no existe, se devuelve la primera opción.
     */
    public function testSeleccionaElMetodoDeEnvioPorDefectoSiDejanDeCumplirseLasCondicionesDelMetodoSeleccionado(): void
    {
        /** @var MockInterface|Cart $cartMock */
        $cartMock = Mockery::mock(Cart::class)->shouldIgnoreMissing();
        $cartMock->shouldReceive('getSelectedShippingMethod')->andReturn('2');

        /** @var CartService|MockInterface $cartService */
        $cartService = $this->getCartServiceMock($cartMock)->makePartial();

        $defaultShippingMethod = ShippingOptionMockFactory::new()->withKey('0')->getMock();
        $selectedShippingMethod = ShippingOptionMockFactory::new()->withKey('2')->getMock();

        $shippingOptions = [
            $defaultShippingMethod,
            ShippingOptionMockFactory::new()->withKey('1')->getMock(),
            $selectedShippingMethod,
        ];
        $cartService->shouldReceive('getAvailableShippingOptions')->andReturn($shippingOptions)
            ->byDefault();
        $shippingMethod = $cartService->getSelectedShippingMethod();
        self::assertEquals($selectedShippingMethod->shippingMethod, $shippingMethod);

        unset($shippingOptions[2]);
        $cartService->shouldReceive('getAvailableShippingOptions')->andReturn($shippingOptions);
        $shippingMethod = $cartService->getSelectedShippingMethod();
        self::assertEquals($defaultShippingMethod->shippingMethod, $shippingMethod);
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function testSePuedenEstablecerLasOpcionesDeEnvio(): void
    {
        // Arrange
        $cartMock = Mockery::mock(Cart::class);
        $cartMock->shouldReceive(
            'setSelectedShippingMethod',
            'setShippingAddress',
            'setBillingAddress',
            'refresh'
        )->once();

        /** @var CartService|MockInterface $cartService */
        $cartService = CartServiceMockFactory::withCart($cartMock);

        (new ModelAddressFactory())->for((new UserFactory())->createOne(), 'addressable')->createOne();

        $cartService->shouldReceive('getShippingOption')->andReturn(
            ShippingOptionMockFactory::new()->getMock()
        );

        // Act y assert
        $cartService->setShippingOptions('option-key', 1, 1);
    }

    public function testDetectaSiHayProductosConDescuento(): void
    {
        $cartMock = Mockery::mock(Cart::class);
        $cartService = new CartService(
            $cartMock,
            Mockery::mock(ShippingCostsCalculator::class),
            Mockery::mock(VatService::class),
            Mockery::mock(CartDiscountCalculator::class),
            $this->getShippingVatServiceMock(),
        );

        $cartMock->shouldReceive('getProducts')->andReturn(new Collection([
            (new CartProductFactory())->withDiscount()->createOne(),
        ]));

        self::assertTrue($cartService->hasDiscountedProducts());
    }

    public function testDetectaSiNoHayProductosConDescuento(): void
    {
        $cartMock = Mockery::mock(Cart::class);
        $cartService = new CartService(
            $cartMock,
            Mockery::mock(ShippingCostsCalculator::class),
            Mockery::mock(VatService::class),
            Mockery::mock(CartDiscountCalculator::class),
            $this->getShippingVatServiceMock(),
        );

        $cartMock->shouldReceive('getProducts')->andReturn(new Collection([
            (new CartProductFactory())->createOne(),
        ]));

        self::assertFalse($cartService->hasDiscountedProducts());
    }

    /**
     * Este test hay que refactorizarlo al estilo clásico porque prácticamente lo único que se usa son mocks, pero
     * necesitaba cobertura para \Bittacora\Bpanel4\Orders\Services\CartService::getShippingCostsTaxes y no puedo
     * pararme ahora mismo a hacerlo de otra forma.
     *
     * @throws RandomException
     * @throws InvalidPriceException
     */
    public function testCalculaElIvaDeLosGastosDeEnvio(): void
    {
        // Arrange
        $shippingCosts = random_int(10,100);
        $cartMock = Mockery::mock(Cart::class);
        $cartMock->shouldReceive('getSelectedShippingMethod')->andReturn('1');
        $shippingCostsCalculator = Mockery::mock(ShippingCostsCalculator::class);
        $shippingCostsCalculator->shouldReceive('getAvailableOptions')->andReturn([]);
        $shippingCostsCalculator->shouldReceive('calculateShippingCosts')->once()->andReturn(new Price($shippingCosts));
        $shippingVatService = Mockery::mock(ShippingVatService::class);
        $vatRate = new VatRate();
        $vatRate->setRate(10);
        $shippingVatService->shouldReceive('getVatRateForShipping')->once()->andReturn($vatRate);
        $cartService = new CartService(
            $cartMock,
            $shippingCostsCalculator,
            $this->app->make(VatService::class),
            Mockery::mock(CartDiscountCalculator::class),
            $shippingVatService
        );

        // Act
        $taxes = $cartService->getShippingCostsTaxes();

        // Assert
        $this->assertEqualsWithDelta(
            (new Price($shippingCosts))->toFloat() * .1,
            $taxes->toFloat(),
            .001
        );
    }

    /**
     * Test de regresión.
     *
     * En algunos casos, el total del carrito descuadra por un céntimo con respecto al esperado. Un ejemplo para
     * reproducirlo sería crear un producto con IVA 10%, y precio sin IVA 0,1235. Añadir al carrito 101 unidades del
     * producto. El total debería ser 13,72, pero ahora mismo sale 13,73 (incorrecto). Este es el caso que se reproduce
     * en este test, para evitar que se reproduzca en el futuro.
     *
     * @throws InvalidPriceException
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws ShippingOptionNotFoundException
     * @throws UserNotLoggedInException
     */
    public function testNoDescuadraElTotalPorUnCentimo(): void
    {
        // Arrange
        Config::set('orders.product_class', CartProduct::class);
        ['cartService' => $cartService] = (new InitializeBasicCartForClient())->execute($this->app);
        $cart = $cartService->getCart();
        $product = (new ProductFactory())->createOne();
        $product->setPrice(new Price(.1235));
        $product->setVatRate((new VatRateFactory())->withRate(10)->createOne());
        $product->save();
        $cartProductFactory = new \Bittacora\Bpanel4\Products\Models\CartProductFactory(
            $this->app->make(ClientService::class),
            $this->app->make(ProductPresenterFactory::class),
        );
        $cartProductFactory->setProduct($product);
        $cartProduct = $cartProductFactory->make();
        (new AddProductToCart())->execute($cart, $cartProduct, 101);

        // Act
        $total = $cartService->getCartTotal();

        // Assert
        $this->assertEquals(12.4735, $cartService->getSubtotal()->toFloat());
        $this->assertEqualsWithDelta(1.24735, $cartService->getTotalTaxes()->toFloat(), .001);
        $this->assertEqualsWithDelta(13.72, $total->toFloat(), .001);
    }

    public function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }

    private function getCartServiceMock(Cart $cartMock): CartService|MockInterface
    {
        /** @var MockInterface|CartService $cartService */
        $cartService = Mockery::mock(CartService::class, [
            $cartMock,
            Mockery::mock(ShippingCostsCalculator::class),
            Mockery::mock(VatService::class),
            $this->getCartDiscountCalculator(),
            $this->getShippingVatServiceMock(),
        ])->makePartial();
        $shippingMethod = Mockery::mock(ShippingMethod::class);
        $shippingMethod->shouldIgnoreMissing();
        /** @var ShippingMethod&MockInterface $secondShippingMethod */
        $secondShippingMethod = Mockery::mock('SegundoMetodoDeEnvio', ShippingMethod::class);
        $secondShippingMethod->shouldIgnoreMissing();
        $cartService->shouldReceive('getAvailableShippingOptions')->andReturn([
            new ShippingOption(
                $shippingMethod,
                Mockery::mock(ShippingMethodPriceCalculator::class)
            ),
            new ShippingOption(
                $secondShippingMethod,
                Mockery::mock(ShippingMethodPriceCalculator::class)
            ),
        ])->byDefault();
        return $cartService;
    }

    private function getCartDiscountCalculator(): LegacyMockInterface&CartDiscountCalculator
    {
        $mock = Mockery::mock(CartDiscountCalculator::class);
        $mock->shouldIgnoreMissing();
        return $mock;
    }

    private function getShippingVatServiceMock(): ShippingVatService|MockInterface
    {
        $shippingVatService = Mockery::mock(ShippingVatService::class);
        $shippingVatService->shouldReceive('getVatRateForShipping')->andReturn((new VatRateFactory())->createOne());
        return $shippingVatService;
    }

}
