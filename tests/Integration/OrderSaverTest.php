<?php

/** @noinspection DynamicInvocationViaScopeResolutionInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration;

use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Database\Factories\TestClientFactory;
use Bittacora\Bpanel4\Orders\Database\Factories\TestProductFactory;
use Bittacora\Bpanel4\Orders\Events\OrderConfirmed;
use Bittacora\Bpanel4\Orders\Exceptions\OrderNotSavedException;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderBillingDetail;
use Bittacora\Bpanel4\Orders\Models\Order\OrderGiftedProduct;
use Bittacora\Bpanel4\Orders\Models\Order\OrderShippingDetail;
use Bittacora\Bpanel4\Orders\Services\OrderSaver;
use Bittacora\Bpanel4\Payment\Tests\Models\TestPaymentMethod;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\FixedAmount\Database\Factories\FixedAmountFactory;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Bittacora\Bpanel4\ShopConfiguration\Tests\Helpers\ShopConfigurationTestHelper;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Bittacora\Bpanel4\Addresses\Database\Factories\ModelAddressFactory;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use stdClass;
use Tests\TestCase;
use Throwable;

final class OrderSaverTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var array<string, string|ModelAddress|CartableClient|Collection<int, Product>|Price>
     */
    private array $orderData;
    private OrderSaver $orderSaver;

    /**
     * @throws InvalidPriceException
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->loginAsUser();
        $this->orderSaver = $this->app->make(OrderSaver::class);
        $this->orderData = $this->getValidOrderData();
        $this->artisan('bpanel4-orders:create-default-order-statuses');

        ShopConfigurationTestHelper::createEmptyShopConfiguration();

        $this->createShippingVat();
    }

    /**
     * @throws InvalidPriceException|Throwable
     */
    public function testGuardaLosDatosBasicosDelPedidoEnLaBaseDeDatos(): void
    {
        self::assertDatabaseMissing('orders', [
            'order_total' => $this->orderData['orderTotal']->toInt(),
            'shipping_costs' => $this->orderData['shippingCosts']->toInt(),
            'total_taxes' => $this->orderData['totalTaxes']->toInt(),
        ]);

        $this->orderSaver->save(...$this->orderData);

        self::assertDatabaseHas('orders', [
            'subtotal_without_taxes' => $this->orderData['subtotalWithoutTaxes']->toInt(),
            'total_taxes' => $this->orderData['totalTaxes']->toInt(),
            'subtotal' => $this->orderData['subtotal']->toInt(),
            'shipping_costs' => $this->orderData['shippingCosts']->toInt(),
            'order_total' => $this->orderData['orderTotal']->toInt(),
        ]);
    }

    /**
     * @throws Throwable
     * @throws OrderNotSavedException
     */
    public function testGuardaLosDatosDelCliente(): void
    {
        self::assertDatabaseMissing('order_client_details', [
            'client_id' => $this->orderData['client']->getClientId(),
            'name' => $this->orderData['client']->getName(),
            'surname' => $this->orderData['client']->getLastName(),
        ]);

        $order = $this->orderSaver->save(...$this->orderData);

        self::assertDatabaseHas('order_client_details', [
            'order_id' => $order->getId(),
            'client_id' => $this->orderData['client']->getClientId(),
            'name' => $this->orderData['client']->getName(),
            'surname' => $this->orderData['client']->getLastName(),
        ]);
    }

    /**
     * @throws Throwable
     */
    public function testGuardaLosDatosDeEnvio(): void
    {
        self::assertDatabaseMissing('order_shipping_details', [
            'address' => $this->orderData['shippingAddress']->getAddress(),
            'country_id' => $this->orderData['shippingAddress']->getCountry()->getId(),
            'postal_code' => $this->orderData['shippingAddress']->getPostalCode(),
        ]);

        $order = $this->orderSaver->save(...$this->orderData);

        self::assertDatabaseHas('order_shipping_details', [
            'order_id' => $order->getId(),
            'address' => $this->orderData['shippingAddress']->getAddress(),
            'country_id' => $this->orderData['shippingAddress']->getCountry()->getId(),
            'postal_code' => $this->orderData['shippingAddress']->getPostalCode(),
            'shipping_method_id' => $this->orderData['shippingMethod']->getId(),
            'shipping_method_type' => $this->orderData['shippingMethod']::class,
        ]);

        /** @phpstan-ignore-next-line Por tipos siempre será true, pero la dejo para asegurarme */
        self::assertInstanceOf(OrderShippingDetail::class, $order->getShippingDetails());
    }

    /**
     * @throws OrderNotSavedException
     * @throws Throwable
     */
    public function testGuardaLosProductos(): void
    {
        /** @var CartableProduct $product */
        $product = $this->orderData['products'][0];
        self::assertDatabaseMissing('order_products', [
            'product_id' => $product->getProductId(),
        ]);

        $order = $this->orderSaver->save(...$this->orderData);

        self::assertDatabaseHas('order_products', [
            'product_id' => $product->getProductId(),
            'order_id' => $order->getId(),
        ]);
    }

    /**
     * @throws Throwable
     * @throws OrderNotSavedException
     */
    public function testGuardaLosDatosDeFacturacion(): void
    {
        self::assertDatabaseMissing('order_billing_details', [
            'address' => $this->orderData['billingAddress']->getAddress(),
            'country_id' => $this->orderData['billingAddress']->getCountry()->getId(),
            'postal_code' => $this->orderData['billingAddress']->getPostalCode(),
        ]);

        $order = $this->orderSaver->save(...$this->orderData);

        self::assertDatabaseHas('order_billing_details', [
            'order_id' => $order->getId(),
            'address' => $this->orderData['billingAddress']->getAddress(),
            'country_id' => $this->orderData['billingAddress']->getCountry()->getId(),
            'postal_code' => $this->orderData['billingAddress']->getPostalCode(),
        ]);

        /** @phpstan-ignore-next-line */
        self::assertInstanceOf(OrderBillingDetail::class, $order->getBillingDetails());
    }

    /**
     * @throws Throwable
     * @throws OrderNotSavedException
     */
    public function testGuardaLosDnisDeLasDirecciones(): void
    {
        // Arrange

        // Act
        $order = $this->orderSaver->save(...$this->orderData);

        // Assert
        self::assertDatabaseHas('order_billing_details', [
            'order_id' => $order->getId(),
            'person_nif' => $this->orderData['billingAddress']->getNif(),
        ]);

        self::assertDatabaseHas('order_shipping_details', [
            'order_id' => $order->getId(),
            'person_nif' => $this->orderData['shippingAddress']->getNif(),
        ]);
    }

    /**
     * @throws OrderNotSavedException
     * @throws Throwable
     */
    public function testLanzaElEventoOrderConfirmed(): void
    {
        Event::fake();
        $this->orderSaver->save(...$this->orderData);
        Event::assertDispatched(OrderConfirmed::class);
    }

    /**
     * @throws OrderNotSavedException
     * @throws Throwable
     */
    public function testAsignaUnEstadoPorDefectoAlPedido(): void
    {
        $this->orderSaver->save(...$this->orderData);

        $order = Order::whereId(1)->firstOrFail();
        self::assertEquals('Creado', $order->getStatus()->name);
    }

    /**
     * @throws Throwable
     */
    public function testGuardaLosProductosRegalo(): void
    {
        // Arrange

        // Act
        $orderData = $this->orderData;
        $giftedProducts = $this->getGiftedProducts();
        $orderData['giftedProducts'] = $giftedProducts;
        $order = $this->orderSaver->save(...$orderData);

        // Assert
        foreach ($giftedProducts as $product) {
            $this->assertDatabaseHas(OrderGiftedProduct::class, [
                'order_id' => $order->getId(),
                'product_id' => $product->getId(),
                'quantity' => $product->getQuantity(),
                'unit_price' => 0,
                'total' => 0,
            ]);
        }
    }

    /**
     * @return array<string, string|ModelAddress|CartableClient|Collection<int, Product>|Price>
     * @throws InvalidPriceException
     */
    private function getValidOrderData(): array
    {
        $products = (new TestProductFactory())->count(5)->create();
        $pivot = new stdClass();
        $pivot->quantity = 1;
        /** @phpstan-ignore-next-line  */
        $products->each(fn (CartableProduct $product): stdClass => $product->pivot = $pivot);
        $vatRate = (new VatRateFactory())->createOne();
        /** @phpstan-ignore-next-line  */
        $products->each(fn (CartableProduct $product) => $product->vat_rate_id = $vatRate->getId());

        return [
            'shippingMethod' => (new FixedAmountFactory())->createOne(),
            'shippingAddress' => (new ModelAddressFactory())->createOne(),
            'billingAddress' => (new ModelAddressFactory())->createOne(),
            'client' => (new TestClientFactory())->createOne(),
            'products' => $products,
            'subtotalWithoutTaxes' => new Price(82.6446),
            'totalTaxes' => new Price(21),
            'subtotal' => new Price(100),
            'shippingCosts' => new Price(5),
            'orderTotal' => new Price(105),
            'paymentMethod' => new TestPaymentMethod(),
            'shippingCostsTaxes' => new Price(0),
        ];
    }

    private function loginAsUser(): void
    {
        $this->actingAs((new TestClientFactory())->createOne()->getUser());
    }

    private function createShippingVat()
    {
        if (VatRate::where('apply_to_shipping', 1)->count() === 0) {
            $vatRate = (new VatRateFactory())->createOne();
            $vatRate->setApplyToShipping(true);
            $vatRate->save();
        }
    }

    private function getGiftedProducts(): Collection
    {
        $products = (new TestProductFactory())->count(2)->create();
        $pivot = new stdClass();
        $pivot->quantity = 1;
        $products->each(fn (CartableProduct $product): stdClass => $product->pivot = $pivot);
        $vatRate = (new VatRateFactory())->createOne();
        /** @phpstan-ignore-next-line  */
        $products->each(fn (CartableProduct $product) => $product->vat_rate_id = $vatRate->getId());

        return $products;
    }
}
