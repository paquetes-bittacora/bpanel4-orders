<?php

declare(strict_types=1);

namespace Integration\Actions;

use Bittacora\Bpanel4\Orders\Actions\Order\UpdateOrderProduct;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Random\RandomException;
use Tests\TestCase;

final class UpdateOrderProductTest extends TestCase
{
    use RefreshDatabase;

    private UpdateOrderProduct $updateOrderProduct;

    protected function setUp(): void
    {
        parent::setUp();
        $this->updateOrderProduct = $this->app->make(UpdateOrderProduct::class);
    }

    /**
     * @throws InvalidPriceException
     * @throws RandomException
     */
    public function testSeActualizaElOrderProduct(): void
    {
        // Arrange
        $orderProduct = $this->getExistingOrderProduct();
        $suffix = bin2hex(random_bytes(3));
        $name = 'Nuevo nombre ' . $suffix;
        $reference = 'nueva-referencia-' . $suffix;
        $quantity = random_int(1, 100);
        $price = Price::fromInt(random_int(1000, 100000));
        $vatRate = random_int(0, 21);
        (new VatRateFactory())->withRate($vatRate)->createOne();

        // Act
        $this->updateOrderProduct->execute($orderProduct, $name, $reference, $quantity, $price, $vatRate);
        $orderProduct = OrderProduct::whereId($orderProduct->id)->firstOrFail();

        // Assert
        $this->assertEquals($name, $orderProduct->getName());
        $this->assertEquals($reference, $orderProduct->getReference());
        $this->assertEquals($quantity, $orderProduct->getQuantity());
        $this->assertEquals($price->toString(), $orderProduct->getPaidUnitPrice()->toString());
        $this->assertEqualsWithDelta($price->toFloat() * $quantity, $orderProduct->getTotal()->toFloat(), 0.0001);
        $this->assertEquals($vatRate, $orderProduct->getTaxRate());
    }

    private function getExistingOrderProduct(): OrderProduct
    {
        $order = (new OrderFactory())->getFullOrder();
        return $order->getProducts()->firstOrFail();
    }
}