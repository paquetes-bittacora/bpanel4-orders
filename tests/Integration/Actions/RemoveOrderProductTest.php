<?php

declare(strict_types=1);

namespace Integration\Actions;

use Bittacora\Bpanel4\Orders\Actions\Order\RecalculateOrderTotals;
use Bittacora\Bpanel4\Orders\Actions\Order\RemoveOrderProduct;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;

final class RemoveOrderProductTest extends TestCase
{
    use RefreshDatabase;

    private RemoveOrderProduct $removeOrderProduct;

    protected function setUp(): void
    {
        parent::setUp();
        $this->removeOrderProduct = new RemoveOrderProduct($this->getRecalculateOrderTotalsMock());
    }

    public function testSePuedeEliminarUnOrderProduct(): void
    {
        // Arrange
        $order = (new OrderFactory())->getFullOrder();
        $orderProduct = $order->getProducts()->firstOrFail();
        $orderProductId = $orderProduct->getId();
        $this->assertTrue($orderProduct->exists());

        // Act
        $this->removeOrderProduct->execute($orderProduct);

        // Assert
        $this->assertFalse(OrderProduct::whereId($orderProductId)->exists());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    private function getRecalculateOrderTotalsMock(): Mockery\MockInterface&RecalculateOrderTotals
    {
        $mock = Mockery::mock(RecalculateOrderTotals::class);
        $mock->shouldReceive('execute')->once();
        return $mock;
    }
}