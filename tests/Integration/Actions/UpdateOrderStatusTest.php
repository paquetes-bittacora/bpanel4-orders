<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Actions;

use Bittacora\Bpanel4\Invoices\Database\Factories\InvoiceFactory;
use Bittacora\Bpanel4\Invoices\Exceptions\CouldNotCreateInvoiceException;
use Bittacora\Bpanel4\Invoices\Mail\ClientInvoiceMail;
use Bittacora\Bpanel4\Orders\Actions\Order\UpdateOrderStatus;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\ShopConfigurations\Database\Factories\ShopConfigurationFactory;
use Bittacora\Bpanel4\Vat\Tests\Feature\ObjectMothers\VatRateObjectMother;
use Bittacora\OrderStatus\Database\Factories\OrderStatusModelFactory;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use Doctrine\DBAL\Driver\Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Mail;
use Tests\TestCase;
use Throwable;

final class UpdateOrderStatusTest extends TestCase
{
    use RefreshDatabase;

    private Order $order;

    protected function setUp(): void
    {
        parent::setUp();
        (new ShopConfigurationFactory())->createOne();
        $this->order = (new OrderFactory())->getFullOrder();
        $vatRate = (new VatRateObjectMother())->createVatRate();
        $vatRate->appliesToShipping();
        $vatRate->save();
    }

    public function testCambiaElEstadoDelPedido(): void
    {
        $newOrderStatus = (new OrderStatusModelFactory())->createOne();

        /** @var UpdateOrderStatus $updateOrder */
        $updateOrder = $this->app->make(UpdateOrderStatus::class);
        $updateOrder->handle($this->order, $newOrderStatus);
        $this->order->refresh();

        self::assertEquals($newOrderStatus->id, $this->order->getStatus()->id);
    }

    /**
     * @throws CouldNotCreateInvoiceException
     * @throws Throwable
     * @throws Exception
     */
    public function testSeReenviaLaFacturaPorCorreoSiElEstadoEsCompletadoYEstaHabilitadaLaOpcion(): void
    {
        // Arrange
        $this->setOrderStatus(OrderStatus::WAITING);
        Config::set('orders.resend_invoice_after_modification', true);
        Config::set('bpanel4-invoices.send_invoice_to_email_when_generated', true);
        Mail::fake();

        // Act
        $newOrderStatus = (new OrderStatusModelFactory())->createOne();
        $newOrderStatus->id = OrderStatus::COMPLETED->value;
        $newOrderStatus->save();
        /** @var UpdateOrderStatus $updateOrder */
        $updateOrder = $this->app->make(UpdateOrderStatus::class);
        $updateOrder->handle($this->order, $newOrderStatus);

        // Assert
        Mail::assertSent(ClientInvoiceMail::class);
    }

    /**
     * @throws CouldNotCreateInvoiceException
     * @throws Throwable
     * @throws Exception
     */
    public function testNoSeReenviaLaFacturaPorCorreoSiElEstadoNoEsCompletadoAunqueEsteHabilitadaLaOpcion(): void
    {
        // Arrange
        $this->setOrderStatus(OrderStatus::WAITING);
        Config::set('orders.resend_invoice_after_modification', true);
        Mail::fake();

        // Act
        $newOrderStatus = (new OrderStatusModelFactory())->createOne();
        $newOrderStatus->id = OrderStatus::CANCELLED->value;
        $newOrderStatus->save();
        /** @var UpdateOrderStatus $updateOrder */
        $updateOrder = $this->app->make(UpdateOrderStatus::class);
        $updateOrder->handle($this->order, $newOrderStatus);

        // Assert
        Mail::assertNotSent(ClientInvoiceMail::class);
    }

    public function testSeNoReenviaLaFacturaPorCorreoSiElEstadoAnteriorNoEraEnEsperaYYaExistiaUnaFactura(): void
    {
        // Arrange
        $this->setOrderStatus(OrderStatus::CREATED);
        Config::set('orders.resend_invoice_after_modification', true);
        $existingInvoice = (new InvoiceFactory())->createOne();
        $existingInvoice->order_id = $this->order->id;
        $existingInvoice->save();
        Mail::fake();

        // Act
        $newOrderStatus = (new OrderStatusModelFactory())->createOne();
        $newOrderStatus->id = OrderStatus::COMPLETED->value;
        $newOrderStatus->save();
        /** @var UpdateOrderStatus $updateOrder */
        $updateOrder = $this->app->make(UpdateOrderStatus::class);
        $updateOrder->handle($this->order, $newOrderStatus);

        // Assert
        Mail::assertNotSent(ClientInvoiceMail::class);
    }

    private function setOrderStatus(OrderStatus $orderStatus): void
    {
        if (!OrderStatusModel::where('id', $orderStatus->value)->exists()) {
            $orderStatusModel = (new OrderStatusModelFactory())->createOne();
            $orderStatusModel->id = $orderStatus->value;
            $orderStatusModel->save();
        }
        $this->order->order_status_id = $orderStatus->value;
        $this->order->save();
        $this->order->refresh();
    }
}
