<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Actions;

use Bittacora\Bpanel4\Orders\Actions\Order\CreateOrderProduct;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Random\RandomException;
use Tests\TestCase;

final class CreateOrderProductTest extends TestCase
{
    use RefreshDatabase;
    private CreateOrderProduct $createOrderProduct;

    protected function setUp(): void
    {
        parent::setUp();
        $this->createOrderProduct = $this->app->make(CreateOrderProduct::class);
    }

    /**
     * @throws RandomException
     * @throws InvalidPriceException
     */
    public function testCreaUnOrderProduct(): void
    {
        // Arrange
        $order = (new OrderFactory())->createOne();
        $suffix = Str::random(3);
        $price = new Price(random_int(100,10000) / 100);
        $quantity = random_int(1, 100);
        $vatRateValue = random_int(0, 50);
        $vatRate = $this->getVatRate($vatRateValue);
        $taxAmount = new Price($vatRateValue / 100 * ($price->toFloat() * $quantity));

        // Act
        $this->createOrderProduct->execute(
            $order->getId(),
            'Producto creado ' . $suffix,
            'referencia-' . $suffix,
            $quantity,
            $price,
            $vatRateValue,
        );

        // Assert
        $this->assertDatabaseHas(OrderProduct::class, [
            'order_id' => $order->getId(),
            'name' => 'Producto creado ' . $suffix,
            'reference' => 'referencia-' . $suffix,
            'unit_price' => $price->toInt(),
            'quantity' => $quantity,
            'tax_rate' => $vatRate->getRate(),
            'tax_amount' => $taxAmount->toInt(),
            'total' => $price->toInt() * $quantity,
        ]);
    }

    private function getVatRate(int $vatRateValue): VatRate
    {
        $vatRate = new VatRate();
        $vatRate->setName('IVA');
        $vatRate->setRate($vatRateValue);
        return $vatRate;
    }
}