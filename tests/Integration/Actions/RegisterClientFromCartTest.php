<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Actions;

use Bittacora\Bpanel4\Clients\Contracts\ClientRepository;
use Bittacora\Bpanel4\Orders\Actions\Cart\RegisterClientFromCart;
use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Bittacora\Bpanel4\Orders\Services\ClientCartService;
use Bittacora\Bpanel4\Orders\Tests\Integration\Factories\CartableProductFactory;
use Bittacora\Bpanel4\Orders\Tests\Integration\Models\TestProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Products\Tests\Acceptance\SetsUpApplication;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

final class RegisterClientFromCartTest extends TestCase
{
    use SetsUpApplication;
    use RefreshDatabase;
    use WithFaker;

    private ClientCartService $clientCartService;
    private ClientRepository $clientRepository;
    private RegisterClientFromCart $registerClientFromCart;

    /**
     * @throws InvalidPriceException
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->registerClientFromCart = $this->app->make(RegisterClientFromCart::class);
        $this->clientCartService = $this->app->make(ClientCartService::class);
        $this->clientRepository = $this->app->make(ClientRepository::class);
        $this->createCountryAndState();
        $this->createRoles();
        /** @var Repository $config */
        $config = $this->app->make(Repository::class);
        $config->set(['orders.product_class' => TestProduct::class]);
        $this->createAnonymousCartWithProducts();
    }

    public function testRegistraUnClienteComoParticular(): void
    {
        $data = $this->getClientdata();
        $this->registerClientFromCart->execute($data);
        $this->assertDatabaseHas('users', ['email' => $data['email']]);
    }

    public function testCopiaLosProductosDelCarritoDeInvitadoAlCarritoDelCliente(): void
    {
        $data = $this->getClientdata();
        $anonymousCart = clone $this->clientCartService->getAnonymousCart();

        $user = $this->registerClientFromCart->execute($data);

        /** @var CartableClient $client */
        $client = $this->clientRepository->getByUserId($user->getId());
        $clientCart = $this->clientCartService->get($client);
        self::assertEquals(
            $anonymousCart->getProducts()[0]?->getId(),
            $clientCart->getProducts()[0]?->getId(),
        );
    }

    /**
     * @throws InvalidPriceException
     */
    private function createAnonymousCartWithProducts(): void
    {
        $product = (new CartableProductFactory())->create();
        $cart = $this->clientCartService->getAnonymousCart();
        $cart->addProduct($product, 2);
        $cart->save();
    }

    /**
     * @return array<string, mixed>
     */
    private function getClientdata(): array
    {
        return [
            'name' => $this->faker->name,
            'surname' => $this->faker->lastName,
            'email' => $this->faker->email,
            'password' => '12341234',
            'password_confirmation' => '12341234',
            'dni' => $this->faker('es_ES')->dni(),
            'phone' => $this->faker->phoneNumber,
            'client_type' => 'particular',
            'country' => 1,
            'state' => 1,
            'location' => $this->faker->city,
            'postal_code' => $this->faker->numberBetween(10000, 99999),
            'company' => '',
            'address' => [
                'person_name' => $this->faker->name,
                'person_surname' => $this->faker->lastName,
                'name' => $this->faker->name,
                'location' => $this->faker->name,
                'address' => $this->faker->name,
                'postal_code' => '12345',
            ],
        ];
    }

    private function createRoles(): void
    {
        Role::create(['name' => 'registered-user']);
        Role::create(['name' => 'any-user']);
    }
}
