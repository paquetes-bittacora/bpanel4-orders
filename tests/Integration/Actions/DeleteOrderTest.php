<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Actions;

use Bittacora\Bpanel4\Orders\Actions\Order\DeleteOrder;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class DeleteOrderTest extends TestCase
{
    use RefreshDatabase;

    public function testBorraUnPedidoConTodosLosModelosRelacionados(): void
    {
        $order = $this->createAndDeleteOrder();

        $this->assertDatabaseMissing('orders', ['id' => $order->getId(), 'deleted_at' => null]);
        $this->assertDatabaseMissing('order_billing_details', ['order_id' => $order->getId(), 'deleted_at' => null]);
        $this->assertDatabaseMissing('order_client_details', ['order_id' => $order->getId(), 'deleted_at' => null]);
        $this->assertDatabaseMissing('order_products', ['order_id' => $order->getId(), 'deleted_at' => null]);
        $this->assertDatabaseMissing('order_shipping_details', ['order_id' => $order->getId(), 'deleted_at' => null]);
    }

    public function testElPedidoSeQuedaComoSoftDeletedEnLaBd(): void
    {
        $order = $this->createAndDeleteOrder();
        $this->assertOrderDetailsExist($order);
    }

    private function createAndDeleteOrder(): Order
    {
        $order = (new OrderFactory())->getFullOrder();

        $this->assertOrderDetailsExist($order);

        /** @var DeleteOrder $deleteOrder */
        $deleteOrder = $this->app->make(DeleteOrder::class);

        $deleteOrder->handle($order);
        return $order;
    }

    private function assertOrderDetailsExist(Order $order): void
    {
        $this->assertDatabaseHas('orders', ['id' => $order->getId()]);
        $this->assertDatabaseHas('order_billing_details', ['order_id' => $order->getId()]);
        $this->assertDatabaseHas('order_client_details', ['order_id' => $order->getId()]);
        $this->assertDatabaseHas('order_products', ['order_id' => $order->getId()]);
        $this->assertDatabaseHas('order_shipping_details', ['order_id' => $order->getId()]);
    }
}
