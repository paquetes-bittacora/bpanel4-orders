<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Actions;

use Bittacora\Bpanel4\Orders\Actions\Order\RecalculateOrderTotals;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Random\RandomException;
use Tests\TestCase;

final class RecalculateOrderTotalsTest extends TestCase
{
    use RefreshDatabase;

    private RecalculateOrderTotals $recalculateOrderTotals;

    protected function setUp(): void
    {
        parent::setUp();
        $this->recalculateOrderTotals = $this->app->make(RecalculateOrderTotals::class);
        $vatRate = (new VatRateFactory())->createOne();
        $vatRate->setRate(10);
        $vatRate->setApplyToShipping(false);
        $vatRate->save();
        (new VatRateFactory())->createOne(); // 21
    }

    /**
     * @throws RandomException|InvalidPriceException
     */
    public function testRecalculaElSubtotalDelPedido(): void
    {
        // Arrange
        $order = (new OrderFactory())->getFullOrder();
        $previousSubtotal = $order->getSubtotal();
        $priceModification = random_int(10, 1000);
        $product = $order->getProducts()->firstOrFail();
        $expectedTotal = $previousSubtotal + $priceModification * 10000;

        // Act (cambiar el total de un producto y actualizar el pedido)
        $product->setUnitPrice(new Price($product->getPaidUnitPrice()->toFloat() + $priceModification));
        $product->save();
        $this->recalculateOrderTotals->execute($order);
        $order->refresh();

        // Assert
        $this->assertEquals($expectedTotal, Price::fromInt($order->getSubtotalWithoutTaxes())->toInt());
    }

    /**
     * @throws RandomException
     * @throws InvalidPriceException
     */
    public function testRecalculaElTotalDelPedido(): void
    {
        // Arrange
        $order = (new OrderFactory())->getFullOrder();
        $newShippingCosts = random_int(10, 1000);
        $newShippingCostsTaxes = .21 * $newShippingCosts;
        $order->setShippingCosts((new Price($newShippingCosts))->toInt());
        $order->setShippingCostsTaxes((new Price($newShippingCostsTaxes))->toInt());
        $order->save();
        $previousSubtotal = $order->refresh()->getSubtotalWithoutTaxes();
        $previousTaxes = $previousSubtotal * .1;
        $priceModification = random_int(10, 1000);
        $taxIncrement = .1 * $priceModification;
        $product = $order->getProducts()->firstOrFail();
        $expectedTotal = (int)($previousSubtotal + $previousTaxes + 10000 * (
            $priceModification + $taxIncrement + $newShippingCosts + $newShippingCostsTaxes
        ));

        // Act
        $newProductPrice = $product->getUnitPrice()->toFloat() + $priceModification;
        $product->setUnitPrice(new Price($newProductPrice));
        $product->setTaxAmount(new Price($newProductPrice * .1));
        $product->save();
        $this->recalculateOrderTotals->execute($order);

        // Assert
        $this->assertEquals($expectedTotal, Price::fromInt($order->getOrderTotal())->toInt());
    }
}