<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Integration\Mail;

use Bittacora\Bpanel4\Invoices\Services\OrderInvoiceGenerator;
use Bittacora\Bpanel4\Invoices\Services\TaxBreakdownCalculator;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Mail\ClientOrderSummaryMail;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Bittacora\Bpanel4\Vat\Services\ShippingVatService;
use Bittacora\Bpanel4\Vat\Services\VatService;
use Bittacora\LaravelTestingHelpers\Views\VendorViewsDisabler;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;

final class ClientOrderSummaryMailTest extends TestCase
{
    use RefreshDatabase;

    private Order $order;

    /**
     * @throws InvalidPriceException
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->order = (new OrderFactory())->getFullOrder();
        VendorViewsDisabler::disableViews();
    }

    public function testElEmailContieneLosDetallesDelPedido(): void
    {
        $taxBreakdownCalculator = $this->getTaxBreakdownCalculatorMock();
        $orderInvoiceGenerator = $this->getOrderInvoiceGeneratorMock();

        $mailer = new ClientOrderSummaryMail(
            $this->order,
            $taxBreakdownCalculator,
            $orderInvoiceGenerator,
        );
        $this->mockPaymentMethodWithInstructions('');
        $mailer->assertSeeInHtml($this->order->getClient()->getName());
        $mailer->assertSeeInHtml($this->order->getClient()->getSurname());
        $mailer->assertSeeInHtml($this->order->getClient()->getNif());
        $mailer->assertSeeInHtml($this->order->getClient()->getEmail() ?? 'EMAIL VACIO');
    }

    public function testElEmailContieneElNifDeFacturacion(): void
    {
        $taxBreakdownCalculator = $this->getTaxBreakdownCalculatorMock();
        $orderInvoiceGenerator = $this->getOrderInvoiceGeneratorMock();
        $this->mockPaymentMethodWithInstructions('');

        $mailer = new ClientOrderSummaryMail(
            $this->order,
            $taxBreakdownCalculator,
            $orderInvoiceGenerator,
        );

        $mailer->assertSeeInHtml($this->order->getBillingDetails()->getPersonNif());
    }

    public function testElEmailContieneLasInstruccionesDelMetodoDePago(): void
    {
        $order = $this->order;
        $paymentInstructions = 'Texto de las instrucciones del método de pago';

        $this->mockPaymentMethodWithInstructions($paymentInstructions);

        $taxBreakdownCalculator = $this->getTaxBreakdownCalculatorMock();
        $orderInvoiceGenerator = $this->getOrderInvoiceGeneratorMock();

        $mail = new ClientOrderSummaryMail(
            $order,
            $taxBreakdownCalculator,
            $orderInvoiceGenerator,
        );
        $mail->assertSeeInHtml($paymentInstructions);
    }

    /**
     * @throws InvalidPriceException
     */
    public function testSeMuestranLosProductosRegalo(): void
    {
        // Arrange
        $order = (new OrderFactory())->withGiftedProducts()->getFullOrder();
        $taxBreakdownCalculator = $this->getTaxBreakdownCalculatorMock();
        $orderInvoiceGenerator = $this->getOrderInvoiceGeneratorMock();
        $this->mockPaymentMethod();

        // Act
        $mail = new ClientOrderSummaryMail(
            $order,
            $taxBreakdownCalculator,
            $orderInvoiceGenerator,
        );

        // Assert
        foreach ($order->getGiftedProducts() as $giftedProduct) {
            $mail->assertSeeInHtml($giftedProduct->getName(), false);
        }
    }

    /**
     * @throws InvalidPriceException
     */
    public function testSeMuestranLosProductosComprados(): void
    {
        // Arrange
        $order = (new OrderFactory())->withProducts()->getFullOrder();
        $taxBreakdownCalculator = $this->getTaxBreakdownCalculatorMock();
        $orderInvoiceGenerator = $this->getOrderInvoiceGeneratorMock();
        $this->mockPaymentMethod();

        // Act
        $mail = new ClientOrderSummaryMail(
            $order,
            $taxBreakdownCalculator,
            $orderInvoiceGenerator,
        );

        // Assert
        foreach ($order->getProducts() as $giftedProduct) {
            $mail->assertSeeInHtml($giftedProduct->getName(), false);
        }
    }

    public function testNoMuestraLasInstruccionesDePagoSiElMetodoDePagoNoLasTiene(): void
    {
        $order = $this->order;
        $paymentInstructions = 'Texto de las instrucciones del método de pago';
        $this->mockPaymentMethod();

        $taxBreakdownCalculator = Mockery::mock(TaxBreakdownCalculator::class, [
            $this->getShippingVatServiceMock(),
            new VatService(),
        ]);
        $taxBreakdownCalculator->makePartial();
        $orderInvoiceGenerator = Mockery::mock(OrderInvoiceGenerator::class);
        $orderInvoiceGenerator->makePartial();
        $orderInvoiceGenerator->shouldReceive('getFilePath')->andReturn('');

        $mail = new ClientOrderSummaryMail(
            $order,
            $taxBreakdownCalculator,
            $orderInvoiceGenerator
        );

        $mail->assertDontSeeInHtml($paymentInstructions);
        $mail->assertDontSeeInHtml('Instrucciones para realizar el pago');
    }

    public function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }

    private function mockPaymentMethodWithInstructions(string $paymentInstructions): void
    {
        $this->mock('payment_method_class_name', function (Mockery\MockInterface $mock) use ($paymentInstructions): void {
            $mock->shouldReceive('getUserInstructions')->andReturn($paymentInstructions);
            $mock->shouldReceive('getName')->andReturn('Nombre de la forma de pago');
        });
    }

    private function getShippingVatServiceMock(): ShippingVatService|Mockery\MockInterface
    {
        $mock = Mockery::mock(ShippingVatService::class);
        $mock->shouldReceive('getVatRateForShipping')->andReturn((new VatRateFactory())->createOne());
        return $mock;
    }

    private function mockPaymentMethod(): void
    {
        $this->mock('payment_method_class_name', function (Mockery\MockInterface $mock): void {
            $mock->shouldReceive('getUserInstructions')->andReturn(null);
        });
    }

    private function getTaxBreakdownCalculatorMock(
    ): Mockery\LegacyMockInterface&TaxBreakdownCalculator&Mockery\MockInterface
    {
        $taxBreakdownCalculator = Mockery::mock(TaxBreakdownCalculator::class);
        $taxBreakdownCalculator->shouldReceive('calculateBreakdown')->andReturn([]);
        return $taxBreakdownCalculator;
    }

    private function getOrderInvoiceGeneratorMock(): OrderInvoiceGenerator&Mockery\LegacyMockInterface&Mockery\MockInterface
    {
        $orderInvoiceGenerator = Mockery::mock(OrderInvoiceGenerator::class);
        $orderInvoiceGenerator->shouldReceive('getFilePath')->andReturn('');
        return $orderInvoiceGenerator;
    }
}
