<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Unit;

use Bittacora\Bpanel4\Orders\Http\Livewire\ProductQuantity;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;

final class ProductQuantityTest extends TestCase
{
    use RefreshDatabase;

    private Mockery\MockInterface&ProductQuantity $productQuantity;

    public function setUp(): void
    {
        parent::setUp();
        $this->productQuantity = Mockery::mock(ProductQuantity::class)->makePartial();
        $this->productQuantity->productId = 1;
        $country = Country::create(['name' => 'España']);
        State::create(['name' => 'Badajoz', 'country_id' => $country->id]);
    }

    public function testNoSePuedeDisminuirLaCantidadPorDebajoDe0(): void
    {
        $this->productQuantity->quantity = 0;
        $this->productQuantity->decreaseQuantity();
        self::assertEquals(0, $this->productQuantity->quantity);
    }

    public function testSePuedeDisminuirLaCantidad(): void
    {
        $this->productQuantity->quantity = 10;
        $this->productQuantity->decreaseQuantity();
        self::assertEquals(9, $this->productQuantity->quantity);
    }

    public function testSePuedeAumentarLaCantidad(): void
    {
        $this->productQuantity->quantity = 10;
        $this->productQuantity->increaseQuantity();
        self::assertEquals(11, $this->productQuantity->quantity);
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }
}
