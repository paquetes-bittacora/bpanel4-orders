<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Unit;

use Bittacora\Bpanel4\Orders\Actions\Cart\ConfirmCart;
use Bittacora\Bpanel4\Orders\Services\OrderSaver;
use Bittacora\Bpanel4\Orders\Services\StockDiscounter;
use Bittacora\Bpanel4\Orders\Tests\Factories\CartMockFactory;
use Bittacora\Bpanel4\Orders\Tests\Factories\CartServiceMockFactory;
use Bittacora\Bpanel4\Orders\Tests\Factories\OrderMockFactory;
use Bittacora\Bpanel4\Orders\Tests\Factories\OrderSaverMockFactory;
use Bittacora\Bpanel4\Orders\Tests\Factories\ShippingOptionValidatorMockFactory;
use Bittacora\Bpanel4\Payment\Contracts\OrderDetailsDto;
use Bittacora\Bpanel4\Payment\Tests\Factories\PaymentMethodMockFactory;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Exceptions\ShippingOptionNotFoundException;
use Illuminate\Contracts\Events\Dispatcher;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;
use Throwable;

final class ConfirmCartTest extends TestCase
{
    use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws InvalidPriceException
     * @throws ShippingOptionNotFoundException
     * @throws Throwable
     */
    public function testGuardaElPedidoEnLaBaseDeDatos(): void
    {
        /** @var OrderSaver&MockInterface $orderSaver */
        $orderSaver = OrderSaverMockFactory::default();
        $orderSaver->shouldReceive('save')->once()->andReturn(OrderMockFactory::default());

        $confirmCart = new ConfirmCart(
            ShippingOptionValidatorMockFactory::new()->withValidOptions()->getMock(),
            CartServiceMockFactory::new()->getMock(),
            $orderSaver,
            $this->getStockDiscounter(),
            $this->getEventDispatcher(),
        );

        $cart = CartMockFactory::new()->getMock();
        $cart->shouldIgnoreMissing();
        $confirmCart->handle($cart);
    }

    /**
     * @throws Throwable
     * @throws ShippingOptionNotFoundException
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws InvalidPriceException
     */
    public function testLlamaAlMetodoDePagoConLosDatosDelPedido(): void
    {
        /** @var OrderSaver&MockInterface $orderSaver */
        $orderSaver = OrderSaverMockFactory::default();

        $confirmCart = new ConfirmCart(
            ShippingOptionValidatorMockFactory::new()->withValidOptions()->getMock(),
            CartServiceMockFactory::new()->getMock(),
            $orderSaver,
            $this->getStockDiscounter(),
            $this->getEventDispatcher(),
        );

        $paymentMethodMock = PaymentMethodMockFactory::default();
        $cartMock = CartMockFactory::new()->getMock();
        $cartMock->shouldIgnoreMissing();
        $cartMock->shouldReceive('getPaymentMethod')->andReturn($paymentMethodMock);
        $paymentMethodMock->shouldReceive('processPayment')->once()->withArgs(function (OrderDetailsDto $dto) use ($cartMock) {
            return $dto->orderAmount->toFloat() === 123.5; // Definido en OrderMockFactory::__construct
        });
        $confirmCart->handle($cartMock);
    }

    public function testDescuentaElStockDeLosProductos(): void
    {
        /** @var OrderSaver&MockInterface $orderSaver */
        $orderSaver = OrderSaverMockFactory::default();

        $stockDiscounter = Mockery::mock(StockDiscounter::class);
        $stockDiscounter->shouldReceive('discountProductStock')->once();

        $confirmCart = new ConfirmCart(
            ShippingOptionValidatorMockFactory::new()->withValidOptions()->getMock(),
            CartServiceMockFactory::new()->getMock(),
            $orderSaver,
            $stockDiscounter,
            $this->getEventDispatcher(),
        );

        $cart = CartMockFactory::new()->getMock();
        $confirmCart->handle($cart);
   }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    public function getStockDiscounter(): MockInterface|StockDiscounter
    {
        $stockDiscounter = Mockery::mock(StockDiscounter::class);
        $stockDiscounter->shouldIgnoreMissing();
        return $stockDiscounter;
    }

    private function getEventDispatcher(): Dispatcher
    {
        $mock = Mockery::mock(Dispatcher::class);
        $mock->shouldReceive('dispatch')->once();
        return $mock;
    }
}
