<?php

/** @noinspection DynamicInvocationViaScopeResolutionInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Unit;

use Bittacora\Bpanel4\Orders\Exceptions\InvalidShippingOptionException;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Orders\Services\ShippingOptionValidator;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethodPriceCalculator;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Types\ShippingOption;
use Illuminate\Database\Eloquent\Collection;
use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;

final class ShippingOptionValidatorTest extends TestCase
{
    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function testLanzaExcepcionSiLaOpcionSeleccionadaNoSeEncuentraEntreLasDisponibles(): void
    {
        self::expectException(InvalidShippingOptionException::class);

        /**
         * @var MockInterface&Cart $cart
         */
        $cart = Mockery::mock(Cart::class)->shouldIgnoreMissing();
        $cart->shouldReceive('getSelectedShippingMethod')->andReturn('opcion-no-valida');

        $cartService = Mockery::mock(CartService::class);
        $cartService->shouldReceive('getAvailableShippingOptions')->andReturn([]);
        $cartService->shouldIgnoreMissing();

        $shippingOptionValidator = new ShippingOptionValidator($cartService);
        $shippingOptionValidator->validateSelectedShippingOption($cart);
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function testNoDevuelveNadaSiLaOpcionSeleccionadaEsValida(): void
    {
        $shippingOptionMock = $this->getShippingOptionMock();

        /** @var MockInterface&Cart $cart */
        $cart = Mockery::mock(Cart::class)->shouldIgnoreMissing();
        $cart->shouldReceive('getProducts')->andReturn(new Collection([]));
        $cart->shouldReceive('getSelectedShippingMethod')->andReturn($shippingOptionMock->getKey());

        $cartService = Mockery::mock(CartService::class);
        $cartService->shouldReceive('getAvailableShippingOptions')->andReturn([$shippingOptionMock]);
        $cartService->shouldReceive('setCart')->andReturnNull();

        $shippingOptionValidator = new ShippingOptionValidator($cartService);
        try {
            $shippingOptionValidator->validateSelectedShippingOption($cart);
            /** @phpstan-ignore-next-line */
            self::assertTrue(true);
        } catch (InvalidShippingOptionException) {
            self::fail();
        }
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    private function getShippingOptionMock(): ShippingOption
    {
        $shippingMethod = Mockery::mock(ShippingMethod::class);
        $shippingMethod->shouldReceive('getId')->andReturn(1);
        $shippingMethod->shouldReceive('shippingClasses->get->pluck->toArray')->andReturn([]);
        return new ShippingOption(
            $shippingMethod,
            Mockery::mock(ShippingMethodPriceCalculator::class),
        );
    }
}
