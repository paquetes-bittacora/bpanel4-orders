<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Unit;

use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Services\StockDiscounter;
use Bittacora\Bpanel4\Orders\Tests\Feature\Factories\CartableProductFactory;
use Bittacora\Bpanel4\Products\Database\Factories\CartProductFactory;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Models\CartProduct;
use Bittacora\Bpanel4\Products\Models\Product;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Mockery;
use Tests\TestCase;

final class StockDiscounterTest extends TestCase
{
    use RefreshDatabase;

    private StockDiscounter $stockDiscounter;

    protected function setUp(): void
    {
        parent::setUp();
        $this->stockDiscounter = $this->app->make(StockDiscounter::class);
    }

    /**
     * Nota: No compruebo si el stock puede ser negativo o no. En principio se permite stock negativo y será cada
     * proyecto el que tenga que interpretar qué se hace si el stock es negativo (no mostrar el producto, por ejemplo).
     * @throws Exception
     */
    public function testDescuentaElStockDeLosProductos(): void
    {
        $stock = random_int(50, 100);
        $quantity = random_int(1, 20);
        $product = $this->getProduct($stock);

        $this->stockDiscounter->discountProductStock(new Collection([$this->getCartProduct($product, $quantity)]));

        $product->refresh();
        $this->assertEquals($stock - $quantity, $product->getStock());
    }

    /**
     * @param int $stock
     * @return Product
     */
    public function getProduct(int $stock): Product
    {
        $product = (new ProductFactory())->createOne();
        $product->setStock($stock);
        $product->save();
        return $product;
    }

    /**
     * @param Product $product
     * @param int $quantity
     * @return CartableProduct|Mockery\MockInterface
     */
    public function getCartProduct(Product $product, int $quantity): CartableProduct|Mockery\MockInterface {
        $cartProduct = Mockery::mock(CartableProduct::class);
        $cartProduct->shouldReceive('getProductId')->andReturn($product->getId());
        $cartProduct->shouldReceive('getQuantity')->andReturn($quantity);
        return $cartProduct;
    }
}
