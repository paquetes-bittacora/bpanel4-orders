<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Unit;

use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\Invoices\Services\OrderInvoiceGenerator;
use Bittacora\Bpanel4\Orders\Actions\Order\UpdateOrderDetails;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Dtos\OrderDataDto;
use Bittacora\Bpanel4\Orders\Models\Order\OrderBillingDetail;
use Bittacora\Bpanel4\Orders\Models\Order\OrderClientDetail;
use Bittacora\Bpanel4\Orders\Models\Order\OrderShippingDetail;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Random\RandomException;
use Tests\TestCase;
use function random_int;

final class UpdateOrderDetailsTest extends TestCase
{
    use RefreshDatabase;

    private OrderInvoiceGenerator $orderInvoiceGenerator;
    private UpdateOrderDetails $updateOrderDetails;

    protected function setUp(): void
    {
        parent::setUp();
        $this->updateOrderDetails = $this->app->make(UpdateOrderDetails::class);
        $this->createShopConfiguration();
        $this->orderInvoiceGenerator = $this->app->make(OrderInvoiceGenerator::class);
        $vatRate = (new VatRateFactory())->createOne();
        $vatRate->apply_to_shipping = true;
        $vatRate->save();
    }

    /**
     * @throws RandomException
     */
    public function testSePuedenCambiarLosDatosDeUnPedido(): void
    {
        // Arrange
        $order = (new OrderFactory())->getFullOrder();
        $newNif = random_int(10000000, 99999999) . 'A';
        $newInvoiceNumber = (string) random_int(9999, 999999);

        $dto = new OrderDataDto(
            'Nombre editado',
            'Apellidos editados',
            $newNif,
            '678678678',
            'email@editado.com',
            // Envío
            'Nombre',
            'Apellidos',
            'Dirección de envío editada',
            2,
            2,
            'Localidad envío editada',
            '10000',
            // Facturación
            'Nombre',
            'Apellidos',
            'Dirección de facturación editada',
            1,
            1,
            'Localidad de facturación editada',
            '20000',
            'Empresa editada',
            $newInvoiceNumber,
        );

        // Act
        $this->orderInvoiceGenerator->getFilePath($order);
        $this->updateOrderDetails->execute($order, $dto);

        // Assert
        $this->assertDatabaseHas(OrderClientDetail::class, [
            'order_id' => $order->getId(),
            'name' => 'Nombre editado',
            'surname' => 'Apellidos editados',
            'nif' => $newNif,
            'company' => 'Empresa editada',
            'phone' => '678678678',
            'email' => 'email@editado.com',
        ]);

        $this->assertDatabaseHas(OrderShippingDetail::class, [
            'order_id' => $order->getId(),
            'address' => 'Dirección de envío editada',
            'country_id' => 2,
            'state_id' => 2,
            'location' => 'Localidad envío editada',
            'postal_code' => '10000',
        ]);

        $this->assertDatabaseHas(OrderBillingDetail::class, [
            'order_id' => $order->getId(),
            'address' => 'Dirección de facturación editada',
            'country_id' => 1,
            'state_id' => 1,
            'location' => 'Localidad de facturación editada',
            'postal_code' => '20000',
        ]);

        $invoice = Invoice::where('order_id', $order->getId())->firstOrFail();
        $this->assertEquals($newInvoiceNumber, $invoice->getInvoiceNumber());
    }

    private function createShopConfiguration(): void
    {
        $shopConfiguration = new ShopConfiguration();
        $shopConfiguration->name = 'Tienda';
        $shopConfiguration->cif = '00000000A';
        $shopConfiguration->address = 'Dirección';
        $shopConfiguration->save();
    }
}