<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Factories;

use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Mockery;
use Mockery\LegacyMockInterface;

final class CartServiceMockFactory
{
    private readonly LegacyMockInterface&CartService $mock;

    /**
     * @throws InvalidPriceException
     */
    public function __construct()
    {
        $this->mock = Mockery::mock(CartService::class);
        $this->mock->shouldReceive('getSubtotal')->andReturn(new Price(90))
            ->byDefault();
        $this->mock->shouldReceive('getSubtotalWithTaxes')->andReturn(new Price(90 * 1.21))
            ->byDefault();
        $this->mock->shouldReceive('getSubtotalWithTaxesAndDiscount')->andReturn(new Price(90 * 1.21))
            ->byDefault();
        $this->mock->shouldReceive('getCartTotalWithDiscount')->andReturn(new Price(113.9))
            ->byDefault();
        $this->mock->shouldReceive('getShippingCosts')->andReturn(new Price(5))->byDefault();
        $this->mock->shouldReceive('getTotalTaxes')->andReturn(new Price(18.9))->byDefault();
        $this->mock->shouldReceive('getCartTotal')->andReturn(new Price(113.9))->byDefault();
        $this->mock->shouldReceive('getSelectedShippingMethod')
            ->andReturn(Mockery::mock(ShippingMethod::class))->byDefault();
        $this->mock->shouldReceive('setCart')->andReturnNull()->byDefault();
        $this->mock->shouldReceive('getShippingCostsTaxes')->andReturn(new Price(0))->byDefault();
        $this->mock->shouldReceive('getDiscountByUnit')->andReturn([]);
    }

    public function getMock(): LegacyMockInterface&CartService
    {
        return $this->mock;
    }

    public static function new(): self
    {
        return new self();
    }
}
