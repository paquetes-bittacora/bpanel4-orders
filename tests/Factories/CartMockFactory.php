<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Factories;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Payment\Tests\Factories\PaymentMethodMockFactory;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;

final class CartMockFactory
{
    private readonly MockInterface&Cart $mock;

    public function __construct()
    {
        $this->mock = Mockery::mock(Cart::class);
        $this->mock->shouldReceive('getSelectedShippingMethod')->andReturn('opcion-de-envio')->byDefault();
        $this->mock->shouldReceive('getShippingAddress')->andReturn(AddressMockFactory::default())
            ->byDefault();
        $this->mock->shouldReceive('getBillingAddress')->andReturn(AddressMockFactory::default())
            ->byDefault();
        $this->mock->shouldReceive('getClient')->andReturn(ClientMockFactory::default())->byDefault();
        $this->mock->shouldReceive('getProducts')->andReturn(new Collection([]))->byDefault();
        $this->mock->shouldReceive('getVatNumber')->andReturnNull()->byDefault();
        $this->mock->shouldReceive('getIp')->andReturn('192.192.192.192')->byDefault();
        $this->mock->shouldReceive('getCountryCode')->andReturn('')->byDefault();
        $this->mock->shouldReceive('getPaymentMethod')->andReturn(PaymentMethodMockFactory::default())
            ->byDefault();
        $this->mock->shouldReceive('getOrderNotes')->andReturnNull()->byDefault();
        $this->mock->shouldReceive('getGiftedProducts')->andReturn(new Collection([]))->byDefault();
    }

    /**
     * @throws InvalidPriceException
     */
    public function withSubtotalWithTaxes(int $subtotal): self
    {
        $this->mock->shouldReceive('getSubtotalWithTaxes')->andReturn(new Price((float)$subtotal));
        return $this;
    }

    public function getMock(): LegacyMockInterface&Cart
    {
        return $this->mock;
    }

    public static function new(): self
    {
        return new self();
    }
}
