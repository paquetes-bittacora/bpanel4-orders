<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Factories;

use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Illuminate\Database\Eloquent\Model;
use Mockery;
use Mockery\LegacyMockInterface;

final class ClientMockFactory
{
    private readonly LegacyMockInterface&CartableClient $mock;

    public function __construct()
    {
        $this->mock = Mockery::mock(CartableClient::class, Model::class);
        $this->mock->shouldReceive('getClient')->andReturnSelf()->byDefault();
    }

    public function getMock(): LegacyMockInterface&CartableClient
    {
        return $this->mock;
    }

    public static function default(): CartableClient&LegacyMockInterface
    {
        return (new self())->getMock();
    }

    public static function new(): self
    {
        return new self();
    }
}
