<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Factories;

use Bittacora\Bpanel4\Orders\Services\OrderSaver;
use Mockery;
use Mockery\LegacyMockInterface;

final class OrderSaverMockFactory
{
    private readonly LegacyMockInterface&OrderSaver $mock;

    public function __construct()
    {
        $this->mock = Mockery::mock(OrderSaver::class);
        $this->mock->shouldReceive('save')->andReturn(OrderMockFactory::default())->byDefault();
    }

    public function getMock(): LegacyMockInterface
    {
        return $this->mock;
    }

    public static function default(): LegacyMockInterface
    {
        return (new self())->getMock();
    }

    public static function new(): self
    {
        return new self();
    }
}
