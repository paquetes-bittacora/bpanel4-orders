<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Factories;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Mockery;
use Mockery\MockInterface;

final class AddressMockFactory
{
    private readonly MockInterface&ModelAddress $mock;

    public function __construct()
    {
        $this->mock = Mockery::mock(ModelAddress::class);
    }

    public static function default(): \Mockery\LegacyMockInterface
    {
        return (new self())->getMock();
    }

    public function getMock(): \Mockery\LegacyMockInterface
    {
        return $this->mock;
    }

    public static function new(): self
    {
        return new self();
    }
}
