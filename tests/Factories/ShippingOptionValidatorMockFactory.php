<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Factories;

use Bittacora\Bpanel4\Orders\Services\ShippingOptionValidator;
use Mockery;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;

final class ShippingOptionValidatorMockFactory
{
    private readonly MockInterface&ShippingOptionValidator $mock;

    public function __construct()
    {
        $this->mock = Mockery::mock(ShippingOptionValidator::class);
        $this->mock->shouldIgnoreMissing();
    }

    public function getMock(): LegacyMockInterface&ShippingOptionValidator
    {
        return $this->mock;
    }

    public function withValidOptions(): self
    {
        $this->mock->shouldReceive('validateSelectedShippingOption')->andReturnNull();
        return $this;
    }

    public static function new(): self
    {
        return new self();
    }
}
