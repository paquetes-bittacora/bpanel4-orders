<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Factories;

use Bittacora\Bpanel4\Orders\Repositories\OrderRepository;
use Mockery;

final class OrderRepositoryMockFactory
{
    public static function default(): OrderRepository|Mockery\MockInterface
    {
        $orderRepositoryMock = Mockery::mock(OrderRepository::class);

        $orderRepositoryMock->shouldReceive('getById')
            ->andReturn(OrderMockFactory::default());

        return $orderRepositoryMock;
    }
}
