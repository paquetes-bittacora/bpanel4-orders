<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Tests\Factories;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Prices\Types\Price;
use Exception;
use Mockery;
use Mockery\LegacyMockInterface;

final class OrderMockFactory
{
    private readonly LegacyMockInterface&Order $mock;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->mock = Mockery::mock(Order::class);
        $this->mock->shouldReceive('getId')->andReturn(random_int(1, 100));
        $exampleOrderTotal = new Price(123.5);
        $this->mock->shouldReceive('getOrderTotal')->andReturn($exampleOrderTotal->toInt());
    }

    public function getMock(): LegacyMockInterface
    {
        return $this->mock;
    }

    public static function default(): LegacyMockInterface
    {
        return (new self())->getMock();
    }

    public static function new(): self
    {
        return new self();
    }
}
