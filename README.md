# Pedidos

DOCUMENTACIÓN PENDIENTE DE TERMINAR Y ORDENAR

## Configuración

Se puede configurar el correo al que llegarán los nuevos pedidos publicando el 
archivo de configuración y editando después `config/orders.php`. En ese archivo
está la opción `orders_admin_email` que es la que indica la dirección de recepción
de los pedidos. Por defecto, se usará el mismo valor que en `bpanel4.admin_email`.

## Requisitos

El modelo del cliente debe usar el trait HasCartTrait
El modelo de cliente debe implementar la interfaz CartableClient

El módulo que defina los productos debe definir la configuración config('orders.product_class')
para que devuelva la clase que implementa la interfaz CartableProduct, por ejemplo:

        config([
            'orders.product_class' => TestProduct::class,
        ]);

También hacer el bind:
        $this->app->bind(CartableProduct::class, CartProduct::class);

Decir que en próximas versiones esto no será necesario (usar Reflection o los bindings de laravel para detectar qué clase implementa la interfaz)

El carrito del cliente es único y le "sigue" por sus dispositivos.
Cuando se completa el pedido se elimina el carrito

## IVA intracomunitario

El campo para introducir el número de IVA se puede ocultar estableciendo lo siguiente en `config/bpanel4.php`:

```
    'disable_intracomunitary_vat' => true,
```

## Hooks

El hook `calculate-cart-total` se dispara cuando se está calculando el total del carrito, y permite
por ejemplo, aplicar cupones descuento. Se puede ver un ejemplo en el paquete `bittacora/bpanel4-coupons`, en la clase
`CartTotalIsBeingCalculatedHook`.

El hook `display-additional-total-rows` se dispara en el carrito cuando se dibujan las filas de subtotal,
gastos de envío, etc, y permite añadir filas adicionales.

## Mini carrito

Para mostrar el mini carrito (por ejemplo, en la cabecera), debe usarse el siguiente código:

```
@livewire('bpanel4-orders::mini-cart')
```
