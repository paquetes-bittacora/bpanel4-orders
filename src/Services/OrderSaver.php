<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services;

use Bittacora\Bpanel4\Coupons\Services\Order\CouponDetailsSaver;
use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Events\OrderConfirmed;
use Bittacora\Bpanel4\Orders\Exceptions\OrderNotSavedException;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Services\Order\BillingDetailsSaver;
use Bittacora\Bpanel4\Orders\Services\Order\CartDeleter;
use Bittacora\Bpanel4\Orders\Services\Order\ClientDetailsSaver;
use Bittacora\Bpanel4\Orders\Services\Order\GiftedProductDetailsSaver;
use Bittacora\Bpanel4\Orders\Services\Order\ProductDetailsSaver;
use Bittacora\Bpanel4\Orders\Services\Order\ShippingDetailsSaver;
use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use Exception;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Throwable;

/**
 * Guarda el pedido en la base de datos
 */
final class OrderSaver
{
    public function __construct(
        private readonly DatabaseManager $db,
        private readonly ClientDetailsSaver $clientDetailsSaver,
        private readonly ShippingDetailsSaver $shippingDetailsSaver,
        private readonly BillingDetailsSaver $billingDetailsSaver,
        private readonly ProductDetailsSaver $productDetailsSaver,
        private readonly CartDeleter $cartDeleter,
        private readonly CouponDetailsSaver $couponDetailsSaver,
        private readonly GiftedProductDetailsSaver $giftedProductDetailsSaver,
    ) {
    }

    /**
     * @param Collection<int, CartableProduct&Model> $products
     * @throws OrderNotSavedException|Throwable
     */
    public function save(
        ShippingMethod $shippingMethod,
        ModelAddress $shippingAddress,
        ModelAddress $billingAddress,
        CartableClient $client,
        Collection $products,
        Price $subtotalWithoutTaxes,
        Price $totalTaxes,
        Price $subtotal,
        Price $shippingCosts,
        Price $shippingCostsTaxes,
        Price $orderTotal,
        PaymentMethod $paymentMethod,
        ?string $vatNumber = null,
        ?string $ip = null,
        ?string $countryCode = null,
        array $discountByUnit = null,
        ?string $orderNotes = null,
        ?Collection $giftedProducts = null,
    ): Order {
        $this->db->beginTransaction();
        try {
            $order = $this->saveOrder(
                $subtotalWithoutTaxes,
                $totalTaxes,
                $subtotal,
                $shippingCosts,
                $shippingCostsTaxes,
                $orderTotal,
                $paymentMethod,
                $vatNumber,
                $ip,
                $countryCode,
                $discountByUnit ?? [],
                $orderNotes ?? null,
            );
            $this->clientDetailsSaver->saveClientDetails($client, $order);
            $this->shippingDetailsSaver->saveShippingDetails($shippingMethod, $shippingAddress, $order);
            $this->billingDetailsSaver->saveBillingDetails($billingAddress, $order);
            $this->productDetailsSaver->saveProductDetails($order, $products);
            $this->giftedProductDetailsSaver->saveProductDetails($order, $giftedProducts ?? new Collection());
            $this->couponDetailsSaver->saveCouponsDetails($order);
            $this->cartDeleter->execute();
            $this->db->commit();
            OrderConfirmed::dispatch($order);
            return $order;
        } catch (Exception $exception) {
            report($exception);
            $this->db->rollBack();
            throw new OrderNotSavedException(previous: $exception);
        }
    }

    private function saveOrder(
        Price $subtotalWithoutTaxes,
        Price $totalTaxes,
        Price $subtotal,
        Price $shippingCosts,
        Price $shippingCostsTaxes,
        Price $orderTotal,
        PaymentMethod $paymentMethod,
        ?string $vatNumber,
        ?string $ip,
        ?string $countryCode,
        array $discountByUnit = [],
        ?string $orderNotes = null,
    ): Order {
        $order = new Order();
        $order->setStatus(OrderStatusModel::whereId(OrderStatus::CREATED->value)->firstOrFail());
        $order->setSubtotalWithoutTaxes($subtotalWithoutTaxes->toInt());
        $order->setTotalTaxes($totalTaxes->toInt());
        $order->setSubtotal($subtotal->toInt());
        $order->setShippingCosts($shippingCosts->toInt());
        $order->setShippingCostsTaxes($shippingCostsTaxes->toInt());
        $order->setOrderTotal($orderTotal->toInt());
        $order->setPaymentMethod($paymentMethod);
        $order->setVatNumber($vatNumber);
        $order->setIpAddress($ip);
        $order->setCountryCode($countryCode);
        $order->setDiscountByUnit($discountByUnit);
        $order->setOrderNotes($orderNotes);
        $order->save();

        return $order;
    }
}
