<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Orders\Actions\Cart\CreateCart;
use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Illuminate\Session\SessionManager;

final class ClientCartService
{
    public function __construct(private readonly SessionManager $session)
    {
    }

    public function get(CartableClient $client): Cart
    {
        $cart = Cart::whereRelation('client', 'id', $client->getClientId())->first();
        if (null === $cart) {
            $createCart = new CreateCart();
            $createCart->execute($client);
        }
        return Cart::whereRelation('client', 'id', $client->getClientId())->firstOrFail();
    }

    public function getAnonymousCart(): Cart
    {
        if ($this->session->has('anonymous-cart')) {
            return $this->session->get('anonymous-cart');
        }

        if (null === \Illuminate\Support\Facades\Route::currentRouteName() && !app()->runningInConsole()) {
            // Páginas de error (404, etc). No creamos carrito en la base de datos
            return new Cart();
        }

        $cart = new Cart();
        $address = new ModelAddress();
        $address->country()->associate(Country::whereName('España')->firstOrFail());
        $address->state()->associate(State::whereName('Badajoz')->firstOrFail());
        $address->postal_code = '00000';
        $address->location = '';
        $cart->setShippingAddress($address);
        $cart->setIp(request()->ip());
        $cart->save();
        $this->session->put('anonymous-cart', $cart);
        return $cart;
    }
}
