<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services\Order;

use Bittacora\Bpanel4\Invoices\Services\OrderInvoiceGenerator;
use Bittacora\Bpanel4\Invoices\Services\TaxBreakdownCalculator;
use Bittacora\Bpanel4\Orders\Mail\AdminOrderSummaryMail;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Mail\Mailer;

/**
 * Envía el resumen del pedido al Admine
 */
final class AdminEmailSender
{
    public function __construct(
        private readonly Mailer $mailer,
        private readonly Repository $config,
        private readonly TaxBreakdownCalculator $taxBreakdownCalculator,
        private readonly OrderInvoiceGenerator $orderInvoiceGenerator,
    ) {
    }

    public function send(Order $order): void
    {
        $mailable = new AdminOrderSummaryMail(
            $order,
            $this->config,
            $this->taxBreakdownCalculator,
            $this->orderInvoiceGenerator
        );

        $this->mailer->to($this->config->get('orders.orders_admin_email'))->send($mailable);
    }
}
