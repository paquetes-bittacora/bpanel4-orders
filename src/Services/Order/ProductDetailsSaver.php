<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services\Order;

use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Services\VatService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class ProductDetailsSaver
{

    public function __construct(private readonly VatService $vatService)
    {
    }

    /**
     * @param Collection<int, CartableProduct&Model> $products
     * @throws InvalidPriceException
     */
    public function saveProductDetails(Order $order, Collection $products): void
    {
        $discountByUnit = $order->getDiscountByUnit();
        $products->each(function (CartableProduct $product) use ($order, $discountByUnit): void {
            $orderProduct = $this->getEmptyProduct();
            $orderProduct->setOrderId($order->getId());
            $orderProduct->setProductId($product->getProductId());
            $orderProduct->setName($product->getName());
            $orderProduct->setUnitPrice($product->getOriginalUnitPrice());
            $orderProduct->setDiscountedUnitPrice($product->getDiscountedUnitPrice());
            $orderProduct->setReference($product->getReference());
            $orderProduct->setQuantity($product->getQuantity());
            $orderProduct->setTaxRate($product->getVatRate()?->getRate() ?? .0);
            $discountByUnitForProduct = $discountByUnit[$product->getProductId()] ?? 0;
            $orderProduct->setTaxAmount(Price::fromInt(
                $this->vatService->getVatForPrice(
                    (new Price($product->getUnitPrice()->toFloat() - $discountByUnitForProduct))->toInt(),
                    $product->getVatRate())
            ));
            $orderProduct->setTotal($product->getTotal());
            $orderProduct->save();
        });
    }

    /**
     * Función para permitir cambiar el modelo que se va a guardar en las clases que hereden de esta
     */
    protected function getEmptyProduct(): OrderProduct
    {
        return new OrderProduct();
    }
}
