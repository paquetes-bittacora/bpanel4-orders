<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services\Order;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderBillingDetail;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;

final class BillingDetailsSaver
{
    public function saveBillingDetails(ModelAddress $billingAddress, Order $order): void
    {
        $billingDetails = new OrderBillingDetail();
        $billingDetails->setName($billingAddress->getName() ?? '');
        $billingDetails->setPersonName($billingAddress->getPersonName());
        $billingDetails->setPersonSurname($billingAddress->getPersonSurname());
        $billingDetails->setPersonNif($billingAddress->getNif());
        $billingDetails->setPersonPhone($billingAddress->getPersonPhone());
        $billingDetails->setAddress($billingAddress->getAddress());
        $billingDetails->setLocation($billingAddress->getLocation());
        $billingDetails->setCountryId($billingAddress->getCountry()->getId());
        $billingDetails->setStateId($billingAddress->getState()->getId());
        $billingDetails->setPostalCode($billingAddress->getPostalCode());

        $billingDetails->order()->associate($order);

        $billingDetails->save();
    }
}
