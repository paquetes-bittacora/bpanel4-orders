<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services\Order;

use Bittacora\Bpanel4\Invoices\Services\OrderInvoiceGenerator;
use Bittacora\Bpanel4\Invoices\Services\TaxBreakdownCalculator;
use Bittacora\Bpanel4\Orders\Mail\ClientOrderSummaryMail;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Illuminate\Contracts\Mail\Mailer;

/**
 * Envía el resumen del pedido al cliente
 */
final class ClientEmailSender
{
    public function __construct(
        private readonly Mailer $mailer,
        private readonly TaxBreakdownCalculator $taxBreakdownCalculator,
        private readonly OrderInvoiceGenerator $orderInvoiceGenerator,
    ) {
    }

    public function send(Order $order): void
    {
        $client = $order->getClient();
        $mailable = new ClientOrderSummaryMail($order, $this->taxBreakdownCalculator, $this->orderInvoiceGenerator);
        $this->mailer->to($client->getEmail())->send($mailable);
    }
}
