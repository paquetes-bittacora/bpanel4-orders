<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services\Order;

use Bittacora\Bpanel4\Clients\Contracts\ClientRepository;
use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderClientDetail;

final class ClientDetailsSaver
{
    public function __construct(private readonly ClientRepository $clientRepository)
    {
    }

    public function saveClientDetails(CartableClient $client, Order $order): void
    {
        $user = $this->clientRepository->getById($client->getClientId());
        $clientDetails = new OrderClientDetail();
        $clientDetails->setClientId($client->getClientId());
        $clientDetails->setName($client->getName());
        $clientDetails->setSurname($client->getLastName());
        $clientDetails->setNif($client->getNif());
        $clientDetails->setCompany($client->getCompany());
        $clientDetails->setPhone($client->getPhone());
        $clientDetails->setEmail($client->getEmail());

        $clientDetails->order()->associate($order);

        $clientDetails->save();
    }
}
