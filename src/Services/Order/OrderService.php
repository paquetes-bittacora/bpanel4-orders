<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services\Order;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Clients\Models\Client;

final class OrderService
{
    public function getClientForOrder(Order $order): Client
    {
        return Client::whereId($order->getClient()->getClientId())->firstOrFail();
    }
}
