<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services\Order;

use Bittacora\Bpanel4\Clients\Contracts\ClientService;

/**
 * Borra el carrito del cliente al finalizar un pedido
 */
final class CartDeleter
{
    public function __construct(private readonly ClientService $clientService)
    {
    }

    /**
     * @throws UserNotLoggedInException
     */
    public function execute(): void
    {
        $cart = $this->clientService->getClientCart();
        $cart->delete();
    }
}
