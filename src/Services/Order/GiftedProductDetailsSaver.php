<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services\Order;

use Bittacora\Bpanel4\Orders\Models\Order\OrderGiftedProduct;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;

final class GiftedProductDetailsSaver extends ProductDetailsSaver
{
    protected function getEmptyProduct(): OrderProduct
    {
        return new OrderGiftedProduct();
    }
}
