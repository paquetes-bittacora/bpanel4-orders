<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services\Order;

use Bittacora\Bpanel4\Orders\Exceptions\InvalidShippingOptionException;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderShippingDetail;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Types\NullShippingOption\NullShippingMethod;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;

final class ShippingDetailsSaver
{
    public function saveShippingDetails(
        ShippingMethod $shippingMethod,
        ModelAddress $shippingAddress,
        Order $order
    ): void {
        $this->validateShippingMethod($shippingMethod);
        $shippingDetails = new OrderShippingDetail();
        $shippingDetails->setName($shippingAddress->getName() ?? '');
        $shippingDetails->setPersonName($shippingAddress->getPersonName());
        $shippingDetails->setPersonSurname($shippingAddress->getPersonSurname());
        $shippingDetails->setPersonPhone($shippingAddress->getPersonPhone());
        $shippingDetails->setPersonNif($shippingAddress->getNif());
        $shippingDetails->setAddress($shippingAddress->getAddress());
        $shippingDetails->setLocation($shippingAddress->getLocation());
        $shippingDetails->setCountryId($shippingAddress->getCountry()->getId());
        $shippingDetails->setStateId($shippingAddress->getState()->getId());
        $shippingDetails->setPostalCode($shippingAddress->getPostalCode());
        $shippingDetails->setShippingMethodName($shippingMethod->getName());
        $shippingDetails->setShippingMethodId($shippingMethod->getId());
        $shippingDetails->setShippingMethodType($shippingMethod::class);
        $shippingDetails->order()->associate($order);
        $shippingDetails->save();
    }

    private function validateShippingMethod(ShippingMethod $shippingMethod): void
    {
        if ($shippingMethod instanceof NullShippingMethod) {
            throw new InvalidShippingOptionException('El método de envío seleccionado no es válido');
        }
    }
}
