<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services;

use Bittacora\Bpanel4\Orders\Exceptions\InvalidShippingOptionException;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Types\ShippingOption;

/**
 * Valida que la opción de envío seleccionada para un carrito corresponda realmente con una de las disponibles.
 */
final class ShippingOptionValidator
{
    public function __construct(private readonly CartService $cartService)
    {
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws InvalidShippingOptionException
     */
    public function validateSelectedShippingOption(Cart $cart): void
    {
        $this->cartService->setCart($cart);
        $shippingOptions = $this->cartService->getAvailableShippingOptions();

        foreach ($shippingOptions as $shippingOption) {
            if ($shippingOption->getKey() === $cart->getSelectedShippingMethod() &&
                $this->validateShippingOptionClasses($cart, $shippingOption)) {
                return;
            }
        }

        throw new InvalidShippingOptionException();
    }

    private function validateShippingOptionClasses(Cart $cart, ShippingOption $shippingOption): bool
    {
        $commonShippingClasses = [];
        foreach ($cart->getProducts() as $product) {
            $shippingClasses = $product->getShippingClasses()->pluck('id')->toArray();
            $commonShippingClasses = $commonShippingClasses === [] ?
                $shippingClasses :
                array_intersect($shippingClasses, $commonShippingClasses);
        }

        $shippingOptionClasses = $shippingOption->shippingMethod->shippingClasses()->get()->pluck('id')->toArray();
        return count(array_intersect($shippingOptionClasses, $commonShippingClasses)) > 0 ||
            count($shippingOptionClasses) === 0;

    }
}
