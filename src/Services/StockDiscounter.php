<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services;

use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

final class StockDiscounter
{
    /** @param EloquentCollection<CartableProduct>|SupportCollection<CartableProduct> $cartableProducts */
    public function discountProductStock(EloquentCollection|SupportCollection $cartableProducts): void
    {
        foreach ($cartableProducts as $cartableProduct) {
            $product = Product::whereId($cartableProduct->getProductId())->firstOrFail();
            $product->setStock(($product->getStock() ?? 0) - $cartableProduct->getQuantity());
            $product->save();
        }
    }
}
