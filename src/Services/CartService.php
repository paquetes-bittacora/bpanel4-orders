<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Services;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Coupons\Exceptions\NotApplicableCouponException;
use Bittacora\Bpanel4\Coupons\Services\CartDiscountCalculator;
use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Exceptions\InvalidShippingOptionException;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Exceptions\ShippingOptionNotFoundException;
use Bittacora\Bpanel4\Shipping\Services\ShippingCostsCalculator;
use Bittacora\Bpanel4\Shipping\Types\NullShippingOption\NullShippingMethod;
use Bittacora\Bpanel4\Shipping\Types\ShippingOption;
use Bittacora\Bpanel4\Vat\Services\ShippingVatService;
use Bittacora\Bpanel4\Vat\Services\VatService;
use Illuminate\Support\Facades\Session;

class CartService
{
    public function __construct(
        private Cart $cart,
        private readonly ShippingCostsCalculator $shippingCostsCalculator,
        private readonly VatService $vatService,
        private readonly CartDiscountCalculator $cartDiscountCalculator,
        private readonly ShippingVatService $shippingVatService,
    ) {
    }

    public function getCartTotalWithoutTaxesBeforeDiscounts(): Price
    {
        $this->cart->disableCoupons();
        $total = $this->getSubtotal();
        $this->cart->enableCoupons();
        return $total;
    }

    public function getDiscountByUnit(): array
    {
        try {
            return $this->cartDiscountCalculator->getDiscountByUnit($this->cart, $this) ?? [];
        } catch (NotApplicableCouponException $exception) {
            $this->cart->appliedCoupons()->latest()->detach();
            Session::flash('alert-danger', 'El cupón que intentaba aplicar no era válido y se ha eliminado. Motivo: ' .
            $exception->getMessage());
            return $this->cartDiscountCalculator->getDiscountByUnit($this->cart, $this) ?? [];
        }
    }

    public function setCart(Cart $cart): void
    {
        $this->cart = $cart;
    }

    public function getCart(): Cart
    {
        return $this->cart;
    }

    /**
     * @return array<ShippingOption>
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function getAvailableShippingOptions(): array
    {
        return $this->shippingCostsCalculator->getAvailableOptions($this->cart);
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws ShippingOptionNotFoundException
     */
    public function getShippingCosts(): ?Price
    {
        return $this->shippingCostsCalculator->calculateShippingCosts(
            $this->cart,
            $this->getSelectedShippingMethod(),
        );
    }

    public function getShippingCostsTaxes(): ?Price
    {
        return Price::fromInt($this->vatService->getVatForPrice(
            $this->getShippingCosts()?->toInt() ?? 0,
            $this->shippingVatService->getVatRateForShipping(),
        ));
    }

    /**
     * @throws InvalidPriceException
     */
    public function getSubtotal(): Price
    {
        $output = 0;

        $discountByUnit = $this->getDiscountByUnit();

        foreach ($this->cart->products()->get() as $product) {
            /** @var CartableProduct $product */
            $price = $product->getUnitPrice();
            $discountByUnitForProduct = $discountByUnit[$product->getProductId()] ?? 0;
            $priceWithVatAndDiscount = $price->toFloat() - (new Price($discountByUnitForProduct))->toFloat();
            $output += (new Price($priceWithVatAndDiscount * $product->getQuantity()))->toInt();
        }

        return Price::fromInt($output);
    }

    /**
     * @throws InvalidPriceException
     */
    public function getTotalTaxes(): Price
    {
        if (null !== $this->cart->getVatNumber()) {
            return new Price(0);
        }
        $output = 0;

        $discountByUnit = $this->getDiscountByUnit();

        foreach ($this->cart->products()->get() as $product) {
            $discountByUnitForProduct = $discountByUnit[$product->getProductId()] ?? 0;
            $priceWithDiscount = $product->getUnitPrice()->toInt() - (new Price($discountByUnitForProduct))->toInt();
            $output += $this->vatService->getVatForPriceAsFloat($priceWithDiscount, $product->getVatRate()) *
                $product->getQuantity();
        }

        $output += $this->vatService->getVatForPrice(
            $this->getShippingCosts()?->toInt() ?? 0,
            $this->shippingVatService->getVatRateForShipping(),
        );

        return Price::fromInt((int)$output);
    }

    /**
     * @throws InvalidPriceException
     */
    public function getSubtotalWithTaxes(): Price
    {
        if (null === $this->cart->getVatNumber()) {
            return Price::fromInt($this->getSubtotal()->toInt() + $this->getTotalTaxes()->toInt());
        }
        return $this->getSubtotal();
    }

    /**
     * @throws InvalidPriceException
     */
    public function getSubtotalWithTaxesAndDiscount(): Price
    {
        if (null !== $this->cart->getVatNumber()) {
            return $this->getSubtotal();
        }

        return new Price($this->getSubtotal()->toFloat() + $this->getTotalTaxes()->toFloat());
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws ShippingOptionNotFoundException|InvalidPriceException
     */
    public function getCartTotal(): Price
    {
        if (null !== $this->cart->getVatNumber()) {
            return Price::fromInt($this->getSubtotal()->toInt() + $this->getShippingCosts()?->toInt());
        }
        return Price::fromInt($this->getSubtotalWithTaxes()->toInt() + $this->getShippingCosts()?->toInt());
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws ShippingOptionNotFoundException
     * @throws InvalidPriceException
     */
    public function getCartTotalWithDiscount(): Price
    {
        return Price::fromInt(
            $this->getSubtotalWithTaxesAndDiscount()->toInt() + $this->getShippingCosts()?->toInt()
        );
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function getSelectedShippingMethod(): ShippingMethod
    {
        if ($this->shippingMethodIsNotEmpty()) {
            foreach ($this->getAvailableShippingOptions() as $option) {
                if ($option->getKey() === $this->cart->getSelectedShippingMethod()) {
                    return $option->shippingMethod;
                }
            }
        }

        if ([] === $this->getAvailableShippingOptions()) {
            return new NullShippingMethod();
        }

        return $this->getAvailableShippingOptions()[0]->shippingMethod;
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function setShippingOptions(string $shippingOptionKey, int $shippingAddressId, int $billingAddressId): void
    {
        $shippingOption = $this->getShippingOption($shippingOptionKey);
        $this->cart->setSelectedShippingMethod($shippingOption->getKey());
        $this->cart->setShippingAddress(ModelAddress::where('id', $shippingAddressId)->firstOrFail());
        $this->cart->setBillingAddress(ModelAddress::where('id', $billingAddressId)->firstOrFail());
        $this->cart->refresh();
    }

    public function hasDiscountedProducts(): bool
    {
        foreach ($this->cart->getProducts() as $product) {
            if (null === $product->getDiscountedUnitPrice()) {
                continue;
            }

            if ($product->getDiscountedUnitPrice()->toInt() === $product->getOriginalUnitPrice()->toInt()) {
                continue;
            }

            return true;
        }

        return false;
    }

    /**
     * Mantener este método como protected hasta que refactorice, si no no puedo
     * probar esta clase bien porque no la he estructurado de una forma que se
     * pueda testear fácilmente.
     *
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    protected function getShippingOption(string $shippingOptionKey): ShippingOption
    {
        $shippingOption = current(array_filter(
            $this->getAvailableShippingOptions(),
            static fn (ShippingOption $option): bool => $option->getKey() === $shippingOptionKey
        ));

        if (!$shippingOption instanceof ShippingOption) {
            throw new InvalidShippingOptionException();
        }

        return $shippingOption;
    }

    private function shippingMethodIsNotEmpty(): bool
    {
        return null !== $this->cart->getSelectedShippingMethod() && "" !== $this->cart->getSelectedShippingMethod();
    }
}
