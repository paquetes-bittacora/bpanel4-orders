<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @method array<string, string> validated($key = null, $default = null)
 */
final class UpdateCartShippingOptionsRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'selected_shipping_option' => 'required|string',
            'selected_shipping_address' => 'required|numeric',
            'selected_billing_address' => 'required|numeric',
        ];
    }
}
