<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @method array<string, string> validated($key = null, $default = null)
 */
final class RegisterFromCartRequest extends FormRequest
{
    public function rules(): array
    {
        $rules = [
            'name' => 'required',
            'surname' => 'required',
            'dni' => 'required',
            'company' => 'nullable',
            'email' => 'required|unique:users|max:255|email:rfc',
            'phone' => 'nullable',
            'country' => 'required|numeric|min:1',
            'state' => 'required|numeric|min:1',
            'password' => 'required|min:8|max:16|same:password_confirmation',
            'accept-policies' => 'required',
            'client_type' => 'nullable',
            'address.name' => 'required',
            'address.location' => 'required',
            'address.postal_code' => 'required',
            'address.address' => 'required',
        ];

        if (config('captcha.secret')) {
            $rules['g-recaptcha-response'] = ['required', 'captcha'];
        }

        return $rules;
    }

    public function messages(): array
    {
        return [
            'country.min' => 'Debe indicar el país',
            'state.min' => 'Debe indicar la provincia',
        ];
    }
}