<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Requests;

use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Illuminate\Foundation\Http\FormRequest;

/**
 * No permito que un cliente borre un item del un carrito que no sea suyo.
 */
final class RemoveCartRowRequest extends FormRequest
{
    /**
     * @throws UserNotLoggedInException
     */
    public function authorize(ClientService $clientService): bool
    {
        return $this->route()?->parameter('cartRow')->cart_id === $clientService->getClientCart()->getId();
    }

    /** @return array<mixed> */
    public function rules(): array
    {
        return [];
    }
}
