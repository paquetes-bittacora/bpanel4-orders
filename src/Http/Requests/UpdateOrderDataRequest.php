<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @method array<string, string> validated($key = null, $default = null)
 */
final class UpdateOrderDataRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'clientName' => 'nullable|string',
            'clientSurname' => 'nullable|string',
            'clientNif' => 'nullable|string',
            'clientCompany' => 'nullable|string',
            'clientPhone' => 'nullable|string',
            'clientEmail' => 'nullable|email',
            'clientShippingAddressName' => 'required|string',
            'clientShippingAddressSurname' => 'required|string',
            'clientShippingAddressPhone' => 'nullable|string',
            'clientShippingAddress' => 'required|string',
            'clientShippingCountry' => 'required|integer',
            'clientShippingState' => 'required|integer',
            'clientShippingLocation' => 'required|string',
            'clientShippingPostalCode' => 'required|string',
            'clientBillingAddressName' => 'required|string',
            'clientBillingAddressSurname' => 'required|string',
            'clientBillingAddressPhone' => 'nullable|string',
            'clientBillingAddress' => 'required|string',
            'clientBillingCountry' => 'required|integer',
            'clientBillingState' => 'required|integer',
            'clientBillingLocation' => 'required|string',
            'clientBillingPostalCode' => 'required|string',
            'invoiceNumber' => 'nullable|numeric',
            'order_notes' => 'nullable|string',
            'clientBillingAddressNif' => 'required|string',
        ];
    }

    public function attributes(): array
    {
        return [
            'clientName' => 'nombre del cliente',
            'clientSurname' => 'apellidos del cliente',
            'clientNif' => 'DNI/NIF/NIE del cliente',
            'clientCompany' => 'empresa del cliente',
            'clientPhone' => 'teléfono del cliente',
            'clientEmail' => 'email del cliente',
            'clientShippingAddressName' => 'nombre (dirección de envío)',
            'clientShippingAddressSurname' => 'apellidos (dirección de envío)',
            'clientShippingAddressPhone' => 'teléfono (dirección de envío)',
            'clientShippingAddress' => 'dirección (dirección de envío)',
            'clientShippingCountry' => 'país (dirección de envío)',
            'clientShippingState' => 'provincia (dirección de envío)',
            'clientShippingLocation' => 'localidad (dirección de envío)',
            'clientShippingPostalCode' => 'código postal (dirección de envío)',
            'clientBillingAddressName' => 'nombre (dirección de facturación)',
            'clientBillingAddressSurname' => 'apellidos (dirección de facturación)',
            'clientBillingAddressPhone' => 'teléfono (dirección de facturación)',
            'clientBillingAddress' => 'dirección (dirección de facturación)',
            'clientBillingCountry' => 'país (dirección de facturación)',
            'clientBillingState' => 'provincia (dirección de facturación)',
            'clientBillingLocation' => 'localidad (dirección de facturación)',
            'clientBillingPostalCode' => 'código postal (dirección de facturación)',
            'clientBillingAddressNif' => 'DNI/NIF/NIE (dirección de facturación)',
            'invoiceNumber' => 'número de factura',
            'order_notes' => 'notas del pedido',
        ];
    }
}