<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Livewire;


use Illuminate\Contracts\View\View;
use Livewire\Component;

final class OrderProductMetaEditor extends Component
{
    public $orderProduct;
    public $status = 'saved';
    public $meta = [];
    public bool $enableEdit = false;

    public function mount(): void
    {
        if ($this->orderProduct->meta !== null) {
            $this->meta = json_decode($this->orderProduct->meta, true);
        }
    }

    public function render(): View
    {
        return view('bpanel4-orders::livewire.order-product-meta-editor');
    }

    public function addField(): void
    {
        $this->meta[] = ['name' => '', 'value' => ''];
    }

    public function removeField(string $fieldIndex): void
    {
        unset($this->meta[$fieldIndex]);
    }

    public function saveOrderProductMeta(): void
    {
        $this->orderProduct->meta = json_encode($this->meta);
        $this->orderProduct->save();
        $this->status = 'saved';
        session()->flash('alert-success-order-product', 'Producto actualizado.');
    }
}