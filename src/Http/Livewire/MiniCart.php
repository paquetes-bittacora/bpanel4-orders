<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Livewire;

use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Orders\Actions\Cart\SetProductQuantity;
use Illuminate\Contracts\View\View;
use Livewire\Component;

final class MiniCart extends Component
{
    /**
     * @var array<string, string>
     */
    public $listeners = [
        'cartUpdated' => 'update',
        'productAdded' => 'productAdded',
        'updateProductQuantity' => 'updateProductQuantity',
    ];

    public int $productCount = 0;

    /**
     * @throws UserNotLoggedInException
     */
    public function update(ClientService $clientService): void
    {
        try {
            $cart = $clientService->getClientCart();
            $this->productCount = $cart->getItemCount();
        } catch (UserNotLoggedInException) {
            $this->productCount = 0;
        }
    }

    /**
     * @throws UserNotLoggedInException
     */
    public function mount(ClientService $clientService): void
    {
        $this->update($clientService);
    }

    public function render(): View
    {
        return view('bpanel4-orders::cart.mini-cart');
    }

    public function productAdded(): void
    {
        $this->dispatchBrowserEvent('cart-updated');
    }

    public function updateProductQuantity(SetProductQuantity $setProductQuantity, int $productId, int $quantity): void
    {
        $setProductQuantity->execute($productId, $quantity);
        $this->emit('productQuantityUpdated');
        $this->dispatchBrowserEvent('cart-updated');
    }
}
