<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Livewire;

use Bittacora\Bpanel4\Orders\Actions\Order\CreateOrderProduct;
use Bittacora\Bpanel4\Orders\Actions\Order\RecalculateOrderTotals;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Contracts\View\View;
use Livewire\Component;

final class OrderProductCreator extends Component
{
    public int $orderId;
    public string $name = '';
    public string $reference = '';
    public ?float $unitPrice = null;
    public int $quantity = 1;
    public float $taxRate = 21;
    public float $taxAmount = 0;
    public string|null $total = null;
    public string|null $totalWithTaxes = null;
    private CreateOrderProduct $createOrderProduct;
    private RecalculateOrderTotals $recalculateOrderTotals;

    public function boot(
        CreateOrderProduct $createOrderProduct,
        RecalculateOrderTotals $recalculateOrderTotals,
    ): void {
        $this->createOrderProduct = $createOrderProduct;
        $this->recalculateOrderTotals = $recalculateOrderTotals;
    }

    public function render(): View
    {
        $this->taxAmount = $this->quantity * $this->unitPrice * $this->taxRate / 100;
        $this->total = (string)(new Price($this->quantity * $this->unitPrice));
        $this->totalWithTaxes = (string)(new Price($this->quantity * $this->unitPrice + $this->taxAmount));
        return view('bpanel4-orders::bpanel.livewire.partials.create-order-product');
    }

    /**
     * @throws InvalidPriceException
     */
    public function createProduct(): void
    {
        $this->createOrderProduct->execute(
            $this->orderId,
            $this->name,
            $this->reference,
            $this->quantity,
            new Price($this->unitPrice),
            $this->taxRate,
        );
        $this->recalculateOrderTotals->execute(Order::whereId($this->orderId)->firstOrFail());
        $this->emit('order-product-created');
    }
}