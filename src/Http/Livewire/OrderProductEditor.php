<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Livewire;

use Bittacora\Bpanel4\Orders\Actions\Order\RecalculateOrderTotals;
use Bittacora\Bpanel4\Orders\Actions\Order\UpdateOrderProduct;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Throwable;

final class OrderProductEditor extends Component
{
    public OrderProduct $orderProduct;
    public string $name;
    public string $reference;
    public int $quantity;
    public float $paidUnitPrice;
    public float $vatRate;
    public float $taxAmount;
    public string $total;
    public string $totalWithTaxes;
    private RecalculateOrderTotals $recalculateOrderTotals;

    protected UpdateOrderProduct $updateOrderProduct;

    public function boot(
        UpdateOrderProduct $updateOrderProduct,
        RecalculateOrderTotals $recalculateOrderTotals,
    ): void {
        $this->updateOrderProduct = $updateOrderProduct;
        $this->recalculateOrderTotals = $recalculateOrderTotals;
    }

    public function mount(): void
    {
        $this->name = $this->orderProduct->name;
        $this->reference = $this->orderProduct->reference;
        $this->quantity = $this->orderProduct->quantity;
        $this->paidUnitPrice = $this->orderProduct->getPaidUnitPrice()->toFloat();
        $this->vatRate = $this->orderProduct->getTaxRate();
    }

    /**
     * @throws InvalidPriceException
     */
    public function render(): View
    {
        $this->taxAmount = $this->quantity * $this->paidUnitPrice * $this->vatRate / 100;
        $this->total = (string)(new Price($this->quantity * $this->paidUnitPrice));
        $this->totalWithTaxes = (string)(new Price($this->quantity * $this->paidUnitPrice + $this->taxAmount));
        return view('bpanel4-orders::bpanel.livewire.order-product-editor');
    }

    /**
     * @throws InvalidPriceException
     * @throws Throwable
     */
    public function updateProduct(): void
    {
        try {
            $paidUnitPrice = new Price($this->paidUnitPrice);
            $this->updateOrderProduct->execute(
                $this->orderProduct,
                $this->name,
                $this->reference,
                $this->quantity,
                $paidUnitPrice,
                $this->vatRate,
            );
            $this->recalculateOrderTotals->execute(Order::whereId($this->orderProduct->getOrderId())->firstOrFail());
            $this->emit('order-product-updated', $this->orderProduct->getId());
            $this->emit('close-order-product-edit', $this->orderProduct->getId());
        } catch (Throwable $e) {
            report($e);
            throw $e;
        }
    }

    public function cancel(): void
    {
        $this->emit('close-order-product-edit', $this->orderProduct->getId());
    }
}