<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Livewire;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderClientDetail;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class OrdersDatatable extends DataTableComponent
{
    public ?string $defaultSortColumn = 'created_at';
    public string $defaultSortDirection = 'desc';

    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Nombre', 'client.name')->sortable(function(Builder $query, $direction) {
                return $query->orderBy(OrderClientDetail::select('name')->whereColumn('order_id', 'orders.id'), $direction);
            })->searchable(fn(Builder $query, $searchTerm) => $query
                ->whereRaw('LOWER(`name`) LIKE ?', [addslashes(strtolower('%' . trim(json_encode($searchTerm), '"'))) . '%'])
            ),
            Column::make('Apellidos', 'client.surname')->sortable(function(Builder $query, $direction) {
                return $query->orderBy(OrderClientDetail::select('surname')->whereColumn('order_id', 'orders.id'), $direction);
            })->searchable(fn(Builder $query, $searchTerm) => $query
                ->orWhereRaw('LOWER(`surname`) LIKE ?', [addslashes(strtolower('%' . trim(json_encode($searchTerm), '"'))) . '%'])
            ),
            Column::make('Nº pedido', 'id')->sortable()->searchable(fn(Builder $query, $searchTerm) => $query
                ->orWhereRaw('orders.id = ?', [(int) $searchTerm])
            ),
            Column::make('Total', 'order_total')
                ->format(fn($value, $row, Column $column) => \Bittacora\Bpanel4\Prices\Types\Price::fromInt($row->getOrderTotal()))
                ->sortable(),
            Column::make('Fecha', 'created_at')
                ->format(fn($value, $row, Column $column) => $row->getDate()->format('d-m-Y H:i:s'))
                ->sortable(),
            Column::make('Estado', 'order_status_id')->view('bpanel4-orders::bpanel.livewire.datatable-columns.order-status'),
            Column::make('Nº Factura', 'id')->view('bpanel4-orders::bpanel.livewire.datatable-columns.invoice-number'),
            Column::make('Factura', 'id')->view('bpanel4-orders::bpanel.livewire.datatable-columns.invoice'),
            Column::make('Acciones', 'id')->view('bpanel4-orders::bpanel.livewire.datatable-columns.actions'),
        ];
    }

    /**
     * @return Builder<Order>
     */
    public function query(): Builder
    {
        return Order::query();
    }

    public function rowView(): string
    {
        return 'bpanel4-orders::bpanel.livewire.orders-datatable';
    }

    /**
     * @return string[]
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        if ([] !== $this->selectedKeys()) {
            Order::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }

    public function configure(): void
    {
        $this
            ->setPrimaryKey('id')
            ->setTdAttributes(function(Column $column, $rows) {
                if ($column->isField('order_status_id')) {
                    return ['class' => 'text-center'];
                }
                if ($column->getTitle() === 'Factura') {
                    return ['class' => 'text-center'];
                }
                return [];
            })
            ->setThAttributes(function (Column $column) {
                if ($column->isField('order_status_id')) {
                    return ['class' => 'text-center'];
                }
                if ($column->getTitle() === 'Factura') {
                    return ['class' => 'text-center'];
                }
                return [];
            })
        ;
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
