<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Livewire;

use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderClientDetail;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class ClientOrdersDatatable extends DataTableComponent
{
    /** @var int $orderCount */
    public $orderCount;

    private ClientService $clientService;

    public ?string $defaultSortColumn = 'created_at';

    /**
     * @return Column[]
     */
    public function columns(): array
    {
        return [
            Column::make(__('Número de pedido'), 'id'),
            Column::make(__('Total'), 'order_total')->format(fn($value, $row, Column $column) => \Bittacora\Bpanel4\Prices\Types\Price::fromInt($row->getOrderTotal())),
            Column::make(__('Fecha'), 'created_at')->format(fn($value, $row, Column $column) => $row->getDate()->format('d-m-Y H:i:s')),
            Column::make(__('Estado'), 'order_status_id')->format(fn($value, $row, Column $column) => $row->getStatus()->name),
            Column::make(__('Factura'), 'id')->view('bpanel4-orders::livewire.datatable-columns.invoice'),
            Column::make(__('Acciones' ), 'id')->view('bpanel4-orders::livewire.datatable-columns.actions'),
        ];
    }


    public function booted(): void
    {
        $this->clientService = resolve(ClientService::class);
        parent::booted();
    }

    /**
     * @return Builder<Order>
     */
    public function query(): Builder
    {
        /** @var Builder<Order> $query */
        $query = Order::query()
            ->orderBy('created_at', 'DESC')
            ->whereIn(
                'orders.id',
                OrderClientDetail::whereClientId($this->clientService->getCurrentClient()->getClientId())
                    ->pluck('order_id')
            );
        return $query;
    }

    public function rowView(): string
    {
        return 'bpanel4-orders::livewire.client-orders-datatable';
    }

    public function render()
    {
        $this->orderCount = $this->query()->count();
        return parent::render();
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setColumnSelectDisabled();
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
