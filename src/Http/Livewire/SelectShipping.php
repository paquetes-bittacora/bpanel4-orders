<?php

/** @noinspection PhpMissingFieldTypeInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Livewire;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Services\ShippingCostsCalculator;
use Bittacora\Bpanel4\Shipping\Types\NullShippingOption\NullShippingOption;
use Bittacora\Bpanel4\Shipping\Types\ShippingOption;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

final class SelectShipping extends Component
{
    /** @var  Cart $cart */
    public $cart;

    /** @var string $selectedShippingMethod */
    public $selectedShippingMethod = '';

    /** @var ShippingOption[]|null */
    private ?array $options = null;

    /** @var string[] */
    public $listeners = ['shippingAddressHasChanged' => 'updateDisplayedShippingOptions'];
    private ShippingCostsCalculator $shippingCostsCalculator;

    public function boot(ShippingCostsCalculator $shippingCostsCalculator): void
    {
        $this->shippingCostsCalculator = $shippingCostsCalculator;
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function hydrate(): void
    {
        if (null !== $this->cart) {
            $this->calculateOptions();
        }
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function mount(): void
    {
        $this->calculateOptions();
        $this->selectFirstOption();

        $this->setDefaultShippingMethod();
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function calculateOptions(): void
    {
        $this->options = $this->shippingCostsCalculator->getAvailableOptions($this->cart);
    }

    public function setSelectedOption(string $selectedOptionKey): void
    {
        $this->cart->setSelectedShippingMethod($selectedOptionKey);
        $this->cart->save();
        $this->selectedShippingMethod = $selectedOptionKey;
    }

    public function render(): Factory|View
    {
        return view('bpanel4-orders::livewire.select-shipping', ['options' => $this->options]);
    }

    /**
     * @return bool true si no hay ninguna opción de envío disponible.
     */
    public function getNoShippingOptionsAvailableProperty(): bool
    {
        if (null === $this->options) {
            return true;
        }

        return 1 === count($this->options) && $this->options[0] instanceof NullShippingOption;
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function updateDisplayedShippingOptions(): void
    {
        $this->calculateOptions();
        $this->selectFirstOption();
    }

    private function setDefaultShippingMethod(): void
    {
        if (null === $this->options) {
            return;
        }

        foreach ($this->options as $option) {
            if ($option->getKey() === $this->cart->getSelectedShippingMethod()) {
                $this->selectedShippingMethod = $option->getKey();
            }
        }
    }

    private function selectFirstOption(): void
    {
        if ([] === $this->options) {
            return;
        }
        if (!isset($this->options[0])) {
            return;
        }
        $this->selectedShippingMethod = $this->options[0]->getKey();
    }
}
