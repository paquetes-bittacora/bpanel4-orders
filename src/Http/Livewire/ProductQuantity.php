<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Livewire;

use Bittacora\Bpanel4\Orders\Events\ProductQuantityHasBeenUpdated;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ProductQuantity extends Component
{
    public int $productId;
    public int $quantity;

    public function render(): View
    {
        return view('bpanel4-orders::livewire.product-quantity');
    }

    public function decreaseQuantity(): void
    {
        if ($this->quantity > 1) {
            --$this->quantity;
            $this->emit('updateProductQuantity', $this->productId, $this->quantity);
            $this->emit('cartUpdated');
            $this->dispatchEvent();
        }
    }

    public function increaseQuantity(): void
    {
        ++$this->quantity;
        $this->emit('updateProductQuantity', $this->productId, $this->quantity);
        $this->emit('cartUpdated');
        $this->dispatchEvent();
    }

    private function dispatchEvent(): void
    {
        ProductQuantityHasBeenUpdated::dispatch($this->productId, $this->quantity);
    }
}
