<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Livewire;

use Bittacora\Bpanel4\Orders\Actions\Order\RemoveOrderGiftedProduct;
use Bittacora\Bpanel4\Orders\Actions\Order\RemoveOrderProduct;
use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Illuminate\Contracts\View\View;
use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Livewire\Component;

/**
 * Este componente se creó a partir de código que ya estaba en la vista de edición del pedido, cuando era un blade
 * normal. Se pasó a Livewire para poder hacer la parte de edición de los productos, pero está más o menos "en bruto".
 */
final class OrderEditor extends Component
{
    public Order $order;
    public bool $orderCanBeEdited;
    public ?int $invoiceNumber;
    /** @var array<OrderStatus> */
    public array $orderStatus;
    /** @var array<int>  */
    public array $productsBeingEdited = [];
    public bool $showAddProductForm = false;
    public ?Invoice $invoice = null;

    public $listeners = [
        'close-order-product-edit' => 'closeOrderProductEdit',
        'order-product-updated' => 'orderProductUpdated',
        'order-product-created' => 'orderProductCreated',
    ];

    private RemoveOrderGiftedProduct $removeOrderGiftedProduct;

    private RemoveOrderProduct $removeOrderProduct;

    public function boot(
        RemoveOrderProduct $removeOrderProduct,
        RemoveOrderGiftedProduct $removeOrderGiftedProduct,
    ): void {
        $this->removeOrderProduct = $removeOrderProduct;
        $this->removeOrderGiftedProduct = $removeOrderGiftedProduct;
    }

    public function render(): View
    {
        return view('bpanel4-orders::bpanel.livewire.order-editor');
    }

    public function deleteProduct(string $orderProductId): void
    {
        $orderProductId = (int) $orderProductId;
        OrderProduct::whereId($orderProductId)->delete();
    }

    public function editProduct(string $orderProductId): void
    {
        $orderProductId = (int) $orderProductId;
        if (!in_array($orderProductId, $this->productsBeingEdited, true)) {
            $this->productsBeingEdited[] = $orderProductId;
        }
    }

    public function orderProductUpdated(string $orderProductId): void
    {
        $this->closeOrderProductEdit($orderProductId);
        session()->flash('order-editor-success', 'Producto actualizado');
    }

    public function orderProductCreated(): void
    {
        $this->showAddProductForm = false;
        session()->flash('order-editor-success', 'Producto añadido');
    }

    public function closeOrderProductEdit(string $orderProductId): void
    {
        $orderProductId = (int) $orderProductId;
        $key = array_search($orderProductId, $this->productsBeingEdited, true);
        unset($this->productsBeingEdited[$key]);
    }

    /**
     * @throws InvalidPriceException
     */
    public function removeProduct(string $productId): void
    {
        $productId = (int) $productId;
        $this->removeOrderProduct->execute($productId);
        $this->order->refresh();
    }

    /**
     * @throws InvalidPriceException
     */
    public function removeGiftedProduct(string $productId): void
    {
        $productId = (int) $productId;
        $this->removeOrderGiftedProduct->execute($productId);
        $this->order->refresh();
    }
}