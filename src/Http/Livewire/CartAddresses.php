<?php

/** @noinspection PhpMissingFieldTypeInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Livewire;

use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Addresses\UseCases\CreateAddress;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use JsonException;
use Livewire\Component;

final class CartAddresses extends Component
{
    /**
     * @var array<string, string>
     */
    protected $listeners = [
        'processShippingAddressForm' => 'processShippingAddressForm',
        'processBillingAddressForm' => 'processBillingAddressForm',
        'updateCartShippingAddress' => 'updateCartShippingAddress',
    ];

    /** @var Model&CartableClient */
    public $client;
    public ?int $selectedShippingAddress = null;
    public ?int $selectedBillingAddress = null;
    public bool $billingAddressIsTheSameAsShipping = true;

    private ClientService $clientService;
    private Factory $view;
    private CreateAddress $createAddress;

    public function boot(Factory $view, CreateAddress $createAddress, ClientService $clientService): void
    {
        $this->view = $view;
        $this->createAddress = $createAddress;
        $this->clientService = $clientService;
    }

    /**
     * @throws UserNotLoggedInException
     */
    public function mount(): void
    {
        $cart = $this->clientService->getClientCart();
        $this->selectedShippingAddress = $cart->getShippingAddress()->getId();
        $this->selectedBillingAddress = $cart->getBillingAddress()->getId();
        $this->billingAddressIsTheSameAsShipping =
            $this->client->getShippingAddress()->getId() === $this->client->getBillingAddress()->getId();
    }

    public function render(): View
    {
        return $this->view->make('bpanel4-orders::livewire.cart-addresses');
    }

    public function processShippingAddressForm(string $fieldsAsJson): void
    {
        try {
            $this->createAddress(
                $fieldsAsJson,
                fn (ModelAddress $address): int => $this->selectedShippingAddress = $address->getId()
            );
        } catch (Exception $e) {
            report($e);
            $this->dispatchBrowserEvent('error-saving-address');
        }
    }

    public function processBillingAddressForm(string $fieldsAsJson): void
    {
        try {
            $this->createAddress(
                $fieldsAsJson,
                fn (ModelAddress $address): int => $this->selectedBillingAddress = $address->getId()
            );
        } catch (Exception $e) {
            report($e);
            $this->dispatchBrowserEvent('error-saving-address');
        }
    }

    /**
     * @throws UserNotLoggedInException
     */
    public function updateCartShippingAddress(): void
    {
        if ($this->selectedShippingAddress > 0) {
            $this->clientService->getClientCart()->setShippingAddress(
                ModelAddress::where('id', $this->selectedShippingAddress)->firstOrFail()
            );
            $this->emit('shippingAddressHasChanged');
        }
    }

    /**
     * @throws JsonException
     */
    private function createAddress(string $fieldsAsJson, callable $updateSelectedAddress): void
    {
        $addressFields = json_decode($fieldsAsJson, true, 10, JSON_THROW_ON_ERROR);
        $addressFields = $this->formatAddressFields($addressFields);
        $address = $this->createAddress->handle($addressFields, $this->client::class, $this->client->getClientId());
        $updateSelectedAddress($address);
        $this->client->refresh();
        $this->dispatchBrowserEvent('address-saved');
    }

    /**
     * @param array<string, string> $fields
     * @return array<string, string|string[]>
     */
    private function formatAddressFields(array $fields): array
    {
        $fields['address'] = [
            'name' => $fields['name'],
            'address' => $fields['address'],
            'location' => $fields['location'],
            'postal_code' => $fields['postal_code'],
        ];
        unset($fields['name'], $fields['location'], $fields['postal_code']);
        return $fields;
    }
}
