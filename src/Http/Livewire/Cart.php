<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Livewire;

use Bittacora\Bpanel4\Addresses\Exceptions\CountryNotFoundException;
use Bittacora\Bpanel4\Addresses\Exceptions\StateNotFoundException;
use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Coupons\Services\CartDiscountCalculator;
use Bittacora\Bpanel4\Coupons\Services\CouponDiscountCalculator;
use Bittacora\Bpanel4\IntracomunitaryVat\Services\IntracomunitaryVatService;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart as CartModel;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Payment\Payment;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Exceptions\ShippingOptionNotFoundException;
use Bittacora\Bpanel4\Shipping\Services\ShippingCostsCalculator;
use Bittacora\Bpanel4\Vat\Services\ShippingVatService;
use Bittacora\Bpanel4\Vat\Services\VatService;
use Bittacora\Bpanel4Users\Models\User;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

final class Cart extends Component
{
    /** @var ?string  */
    public $shippingOption;
    /** @var ?int $shippingAddress */
    public $shippingAddress;
    /** @var ?int $billingAddress */
    public $billingAddress;
    /** @var bool $isPreview */
    public $isPreview = false;

    /** @var int $selectedPaymentMethod */
    public $selectedPaymentMethod = 0;
    /** @var ?string $paymentMethodInstructions */
    public $paymentMethodInstructions;

    /** @var ?string $orderNotes  */
    public $orderNotes = null;

    private CartDiscountCalculator $cartDiscountCalculator;

    private ClientService $clientService;
    private CouponDiscountCalculator $couponDiscountCalculator;
    private Payment $payment;
    private ShippingCostsCalculator $shippingCostsCalculator;
    private ShippingVatService $shippingVatService;
    private VatService $vatService;
    private Factory $view;
    private ?User $user = null;

    /**
     * @var array<string, string>
     */
    protected $listeners = [
        'productQuantityUpdated' => 'refreshCart',
        'refreshComponent' => '$refresh',
    ];
    private IntracomunitaryVatService $intracomunitaryVatService;

    public function mount(): void
    {
        $this->setSelectedPaymentMethod((string) $this->selectedPaymentMethod);
    }

    public function boot(
        ClientService $clientService,
        ShippingCostsCalculator $shippingCostsCalculator,
        VatService $vatService,
        Factory $view,
        Payment $payment,
        AuthManager $auth,
        CouponDiscountCalculator $couponDiscountCalculator,
        CartDiscountCalculator $cartDiscountCalculator,
        IntracomunitaryVatService $intracomunitaryVatService,
        ShippingVatService $shippingVatService,
    ): void {
        $this->clientService = $clientService;
        $this->shippingCostsCalculator = $shippingCostsCalculator;
        $this->vatService = $vatService;
        $this->view = $view;
        $this->payment = $payment;
        $this->user = $auth->user();
        $this->cartDiscountCalculator = $cartDiscountCalculator;
        $this->couponDiscountCalculator = $couponDiscountCalculator;
        $this->intracomunitaryVatService = $intracomunitaryVatService;
        $this->shippingVatService = $shippingVatService;
    }

    /**
     * @throws InvalidPriceException
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws ShippingOptionNotFoundException
     */
    public function render(): View
    {
        $cart = $this->clientService->getClientCart();
        $cartService = new CartService(
            $cart,
            $this->shippingCostsCalculator,
            $this->vatService,
            $this->cartDiscountCalculator,
            $this->shippingVatService,
        );

        try {
            if ($this->isPreview) {
                $cartService->setShippingOptions($this->shippingOption, $this->shippingAddress, $this->billingAddress);
                $this->validateShippingAddress();
                $this->validateBillingAddress();
            } else {
                $this->validateShippingAddress();
            }
            return $this->view->make('bpanel4-orders::livewire.cart', $this->getViewVariables($cart, $cartService));
        } catch (CountryNotFoundException|StateNotFoundException $exception) {
            report($exception);
            return $this->view->make('bpanel4-orders::livewire.cart-error', [
                'cartError' => 'address-error',
                'cartErrorAddress' => $exception->getAddress(),
                'isShippingAddressError' => $exception->getAddress()->getId() === $this->clientService->getClientCart()->getShippingAddress()->getId(),
                'isBillingAddressError' => $exception->getAddress()->getId() === $this->clientService->getClientCart()->getBillingAddress()->getId()
            ]);
        }
    }

    public function refreshCart(): void
    {
        $this->emit('refreshComponent');
    }

    public function setSelectedPaymentMethod(string $key): void
    {
        $key = (int) $key;
        $this->selectedPaymentMethod = $key;
        $paymentMethod = $this->payment->getPaymentMethodsForUser($this->user)[$key];
        $cart = $this->clientService->getClientCart();
        $cart->setPaymentMethod($paymentMethod);
        $cart->save();
        $this->paymentMethodInstructions = $paymentMethod->getUserInstructions();
    }

    public function updatedOrderNotes(): void
    {
        $cart = $this->clientService->getClientCart();
        $cart->setOrderNotes($this->orderNotes);
        $cart->save();
    }

    /**
     * @return array<string, int|float>
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws InvalidPriceException
     * @throws ShippingOptionNotFoundException
     */
    private function getViewVariables(CartModel $cart, CartService $cartService): array
    {
        return [
            'cart' => $cart,
            'isPreview' => true,
            'subtotal' => $cartService->getSubtotal(),
            'totalTaxes' => $cartService->getTotalTaxes(),
            'subtotalWithTaxes' => $cartService->getSubtotalWithTaxesAndDiscount(),
            'shippingCosts' => $cartService->getShippingCosts(),
            'cartTotal' => $cartService->getCartTotalWithDiscount(),
            'shippingMethodName' => $cartService->getSelectedShippingMethod()->getName(),
            'shippingCostsAmount' => $cartService->getShippingCosts(),
            'paymentMethods' => $this->payment->getPaymentMethodsForUser($this->user),
            'couponDiscountCalculator' => $this->couponDiscountCalculator,
            'clientIsInTheEU' => $this->intracomunitaryVatService->isUserInEurope(),
        ];
    }

    /**
     * Al intentar obtener el país o provincia, si no existe, se lanzará una excepción
     * @return void
     * @throws CountryNotFoundException
     * @throws StateNotFoundException
     */
    private function validateShippingAddress(): void
    {
        $this->clientService->getClientCart()->getShippingAddress()->getCountry();
        $this->clientService->getClientCart()->getShippingAddress()->getState();
    }

    /**
     * Al intentar obtener el país o provincia, si no existe, se lanzará una excepción
     * @return void
     * @throws CountryNotFoundException
     * @throws StateNotFoundException
     */
    private function validateBillingAddress(): void
    {
        $this->clientService->getClientCart()->getBillingAddress()->getCountry();
        $this->clientService->getClientCart()->getBillingAddress()->getState();
    }
}
