<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Invoices\Dtos\TaxBreakdown;
use Bittacora\Bpanel4\Invoices\Services\TaxBreakdownCalculator;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Services\Order\OrderService;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Symfony\Component\HttpKernel\Exception\HttpException;

final class MyOrdersController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly AuthManager $auth,
    ) {
    }

    public function index(ClientService $clientService): View
    {
        $this->authorize('my-orders.index');
        return $this->view->make('bpanel4-orders::my-orders.index', [
            'client' => $clientService->getCurrentClient(),
        ]);
    }

    /**
     * @throws AuthorizationException
     * @throws InvalidPriceException
     */
    public function show(
        Order $order,
        OrderService $orderService,
        ClientService $clientService,
        TaxBreakdownCalculator $taxBreakdownCalculator
    ): View {
        $this->authorize('my-orders.show');
        $this->checkIfOrderBelongsToCurrentUser($orderService, $order);

        return $this->view->make('bpanel4-orders::my-orders.show', [
            'order' => $order,
            'client'=> $clientService->getCurrentClient(),
            'clientDetails' => $order->getClient(),
            'shippingDetails' => $order->getShippingDetails(),
            'billingDetails' => $order->getBillingDetails(),
            'products' => $order->getProducts(),
            'giftedProducts' => $order->getGiftedProducts(),
            'coupons' => $order->getCoupons(),
            'taxBreakdown' => $this->getTaxBreakdown($order, $taxBreakdownCalculator),
        ]);
    }

    private function checkIfOrderBelongsToCurrentUser(OrderService $orderService, Order $order): void
    {
        if ($this->auth?->user()->id !== $orderService->getClientForOrder($order)->getUser()->getId()) {
            throw new HttpException(403);
        }
    }

    /**
     * @return TaxBreakdown[]
     * @throws InvalidPriceException
     */
    private function getTaxBreakdown(Order $order, TaxBreakdownCalculator $taxBreakdownCalculator): array
    {
        return $taxBreakdownCalculator->calculateBreakdown(
            $order->getProducts(),
            Price::fromInt($order->getShippingCosts()),
            $order->getDiscountByUnit(),
        );
    }
}
