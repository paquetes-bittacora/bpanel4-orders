<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Coupons\Services\CartDiscountCalculator;
use Bittacora\Bpanel4\Orders\Actions\Cart\CreateCart;
use Bittacora\Bpanel4\Orders\Actions\Cart\RemoveCartRow;
use Bittacora\Bpanel4\Orders\Exceptions\CartAlreadyExistsException;
use Bittacora\Bpanel4\Orders\Http\Requests\RemoveCartRowRequest;
use Bittacora\Bpanel4\Orders\Http\Requests\UpdateCartShippingOptionsRequest;
use Bittacora\Bpanel4\Orders\Models\Cart\CartRow;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Shipping\Services\ShippingCostsCalculator;
use Bittacora\Bpanel4\Vat\Services\ShippingVatService;
use Bittacora\Bpanel4\Vat\Services\VatService;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

final class CartController extends Controller
{
    public function __construct(
        private readonly AuthManager $auth,
        private readonly Factory $view,
        private readonly Redirector $redirector
    ) {
    }

    /**
     * @throws CartAlreadyExistsException
     */
    public function create(CreateCart $createCart): void
    {
        $user = $this->auth->user();
        $createCart->execute($user);
    }

    public function show(
        ClientService $clientService,
        ShippingCostsCalculator $shippingCostsCalculator,
        VatService $vatService,
        CartDiscountCalculator $cartDiscountCalculator,
        ShippingVatService $shippingVatService,
    ): View {
        $cart = $clientService->getClientCart();
        $cartService = new CartService(
            $cart,
            $shippingCostsCalculator,
            $vatService,
            $cartDiscountCalculator,
            $shippingVatService
        );
        return $this->view->make('bpanel4-orders::cart.cart')->with([
            'cart' => $cart,
            'cartService' => $cartService,
        ]);
    }

    public function removeProduct(
        RemoveCartRowRequest $removeCartRowRequest, // Ver request, solo se usa para autorizar o no el borrado
        CartRow $cartRow,
        RemoveCartRow $removeCartRow,
    ): RedirectResponse {
        $removeCartRowRequest->validated();
        $removeCartRow->execute($cartRow);
        return $this->redirector->route('cart.show');
    }

    public function preview(UpdateCartShippingOptionsRequest $request): View
    {
        $this->authorize('cart.preview');

        $requestData = $request->validated();
        return $this->view->make('bpanel4-orders::cart.preview', [
            'shippingOption' => $requestData['selected_shipping_option'],
            'shippingAddress' => (int) $requestData['selected_shipping_address'],
            'billingAddress' => (int) $requestData['selected_billing_address'],
        ]);
    }
}
