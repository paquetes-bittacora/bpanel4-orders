<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Controllers;

use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Routing\Controller;

final class CartShippingController extends Controller
{
    public function __construct(
        private readonly Factory $view
    ) {
    }

    /**
     * @throws UserNotLoggedInException
     */
    public function show(ClientService $clientService): View
    {
        return $this->view->make('bpanel4-orders::cart.cart-addresses', [
            'client' => $clientService->getCurrentClient(),
            'cart' => $clientService->getClientCart(),
        ]);
    }
}
