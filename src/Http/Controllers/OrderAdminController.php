<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Invoices\Contracts\InvoicesModule;
use Bittacora\Bpanel4\Orders\Actions\Order\DeleteOrder;
use Bittacora\Bpanel4\Orders\Actions\Order\UpdateOrderDetails;
use Bittacora\Bpanel4\Orders\Actions\Order\UpdateOrderStatus;
use Bittacora\Bpanel4\Orders\Dtos\OrderDataDto;
use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Http\Requests\UpdateOrderDataRequest;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\OrderStatus\Contracts\OrderStatusModule;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use Bittacora\OrderStatus\OrderStatusFacade;
use DateTime;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\Factory;
use Throwable;

final class OrderAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly Redirector $redirector,
        private readonly ExceptionHandler $exceptionHandler,
    ) {
    }

    public function index(): View
    {
        $this->authorize('orders-admin.index');
        return $this->view->make('bpanel4-orders::bpanel.order.index');
    }

    public function edit(Order $order, InvoicesModule $invoicesModule, OrderStatusModule $orderStatusModule): View
    {
        $this->authorize('orders-admin.edit');
        return $this->view->make('bpanel4-orders::bpanel.order.edit', [
            'order' => $order,
            'invoiceNumber' => $invoicesModule->getInvoiceNumberForOrderId($order->getId()),
            'invoice' => $invoicesModule->getInvoiceForOrderId($order->getId()),
            'orderStatus' => $orderStatusModule->getAllAsArray(),
            'orderCanBeEdited' =>  $order->getStatus()->id === OrderStatus::WAITING->value,
        ]);
    }

    public function update(
        UpdateOrderDataRequest $request,
        Order $order,
        UpdateOrderDetails $updateOrderDetails,
        InvoicesModule $invoicesModule,
    ): RedirectResponse {
        try {
            $dto = OrderDataDto::fromArray($request->validated());
            $updateOrderDetails->execute($order, $dto);
            $invoiceDate = $request->get("invoice_date");
            if (null !== $invoiceDate) {
                $invoiceDate = \DateTime::createFromFormat('d/m/Y H:i', $invoiceDate);
                $invoice = $invoicesModule->getInvoiceForOrderId($order->getId());
                $invoicesModule->updateInvoiceDate($invoice, $invoiceDate);
            }
            return $this->redirector->back()->with('alert-success', 'Datos del pedido actualizados');
        } catch (Throwable $throwable) {
            $this->exceptionHandler->report($throwable);
            return $this->redirector->back()->with('alert-danger', 'Ocurrió un error al actualizar los datos del pedido');
        }
    }

    public function updateStatus(Request $request, Order $order, UpdateOrderStatus $updateOrderStatus): RedirectResponse
    {
        $this->authorize('orders-admin.update');

        $orderStatusId = (int) $request->get('order_status');

        $updateOrderStatus->handle($order, OrderStatusModel::whereId($orderStatusId)->firstOrFail());

        return $this->redirector->route('orders-admin.edit', ['order' => $order])
            ->with(['alert-success' => 'El estado del pedido se actualizó correctamente']);
    }

    public function destroy(Order $order, DeleteOrder $deleteOrder): RedirectResponse
    {
        $this->authorize('orders-admin.destroy');

        $deleteOrder->handle($order);

        return $this->redirector->route('orders-admin.index')
            ->with(['alert-success' => 'El pedido se eliminó correctamente']);
    }

    public function checkInvoiceNumber(int $orderNumber, int $invoiceNumber, InvoicesModule $invoicesModule): JsonResponse
    {
        return new JsonResponse($invoicesModule->invoiceNumberIsTaken($invoiceNumber, $orderNumber));
    }
}
