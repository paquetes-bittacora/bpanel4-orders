<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Controllers;

use Bittacora\Bpanel4\Orders\Actions\Cart\RegisterClientFromCart;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Bittacora\Bpanel4\Orders\Http\Requests\RegisterFromCartRequest;
use Session;
use Throwable;

final class CartAccountController
{
    public function __construct(
        private readonly Factory $view,
        private readonly Redirector $redirector,
    ) {
    }

    public function showRegistration(): View
    {
        return $this->view->make('bpanel4-orders::cart.register-form', [
            'password' => bin2hex(random_bytes(4)),
        ]);
    }

    public function register(RegisterFromCartRequest $request, RegisterClientFromCart $registerClientFromCart): RedirectResponse
    {
        try {
            $user = $registerClientFromCart->execute($request->all());
            event(new Registered($user));
            Auth::login($user);
            return $this->redirector->route('cart.show');
        } catch (ValidationException $e) {
            $request->flash();
            return $this->redirector->back()->with(
                'alert-danger',
                'Ocurrió un error al intentar crear su cuenta: ' . $e->getMessage(),
            );
        } catch (Throwable $e) {
            $request->flash();
            \Log::error('Ocurrió un error al intentar crear su cuenta' . json_encode($request->all()));
            \Log::error($e->getMessage());
            report($e);
            return $this->redirector->back()->with(
                'alert-danger',
                'Ocurrió un error al intentar crear su cuenta'
            );
        }
    }
}
