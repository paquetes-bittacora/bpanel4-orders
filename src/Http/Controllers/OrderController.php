<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Orders\Actions\Cart\ConfirmCart;
use Bittacora\Bpanel4\Orders\Exceptions\NotEnoughProductStockException;
use Bittacora\Bpanel4\Orders\Exceptions\OrderNotSavedException;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Exceptions\ShippingOptionNotFoundException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Throwable;

final class OrderController extends Controller
{
    public function __construct(
        private readonly ClientService $clientService,
        private readonly Redirector $redirector,
        private readonly Factory $view,
    ) {
    }

    /**
     * @throws OrderNotSavedException
     * @throws Throwable
     * @throws ShippingOptionNotFoundException
     * @throws UserNotLoggedInException
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws InvalidPriceException
     */
    public function confirm(ConfirmCart $confirmCart): RedirectResponse
    {
        $this->authorize('order.confirm');
        try {
            return $confirmCart->handle($this->clientService->getClientCart());
        } catch (NotEnoughProductStockException $e) {
            return $this->redirector->route('cart.show')->with('alert-danger', $e->getMessage());
        } catch (OrderNotSavedException|InvalidShippingMethodPriceCalculatorException|InvalidPriceException $e) {
            report($e);
            return $this->redirector->route('cart.show')->with('alert-danger', 'No se pudo confirmar su pedido');
        }
    }

    public function confirmed(): View
    {
        $this->authorize('order.confirmed');

        $isGuest = auth()->user()?->isGuest();
        if ($isGuest) {
            auth()->logout();
        }

        return $this->view->make('bpanel4-orders::order.confirmed', ['isGuest' => $isGuest]);
    }

    public function cancelled(): View
    {
        return $this->view->make('bpanel4-orders::order.cancelled');
    }
}
