<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders;

use Bittacora\Bpanel4\Orders\Commands\CreateDefaultOrderStatuses;
use Bittacora\Bpanel4\Orders\Commands\InstallCommand;
use Bittacora\Bpanel4\Orders\Events\OrderConfirmed;
use Bittacora\Bpanel4\Orders\Events\ProductHasBeenRemovedFromCart;
use Bittacora\Bpanel4\Orders\Events\ProductQuantityHasBeenUpdated;
use Bittacora\Bpanel4\Orders\Http\Livewire\Cart;
use Bittacora\Bpanel4\Orders\Http\Livewire\CartAddresses;
use Bittacora\Bpanel4\Orders\Http\Livewire\ClientOrdersDatatable;
use Bittacora\Bpanel4\Orders\Http\Livewire\MiniCart;
use Bittacora\Bpanel4\Orders\Http\Livewire\OrderEditor;
use Bittacora\Bpanel4\Orders\Http\Livewire\OrderProductCreator;
use Bittacora\Bpanel4\Orders\Http\Livewire\OrderProductEditor;
use Bittacora\Bpanel4\Orders\Http\Livewire\OrderProductMetaEditor;
use Bittacora\Bpanel4\Orders\Http\Livewire\OrdersDatatable;
use Bittacora\Bpanel4\Orders\Http\Livewire\ProductQuantity;
use Bittacora\Bpanel4\Orders\Http\Livewire\SelectShipping;
use Bittacora\Bpanel4\Orders\Listeners\AdminOrderConfirmationEmailListener;
use Bittacora\Bpanel4\Orders\Listeners\ClientLoginListener;
use Bittacora\Bpanel4\Orders\Listeners\ClientOrderConfirmationEmailListener;
use Bittacora\Bpanel4\Orders\Listeners\GuestOrderConfirmedListener;
use Bittacora\Bpanel4\Orders\Listeners\RemoveInvalidCouponsAfterProductDeletion;
use Bittacora\Bpanel4\Orders\Listeners\RemoveInvalidCouponsAfterProductQuantityChange;
use Bittacora\Multimedia\Contracts\Dtos\MediaConversionDto;
use Bittacora\Multimedia\Contracts\MultimediaModule;
use Bittacora\Multimedia\MultimediaFacade;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;

final class OrdersServiceProvider extends ServiceProvider
{
    public const PACKAGE_PREFIX = 'bpanel4-orders';

    public function boot(): void
    {
        $this->commands([
            InstallCommand::class,
            CreateDefaultOrderStatuses::class,
        ]);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->registerEventListeners();
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'orders-admin');

        Livewire::component(self::PACKAGE_PREFIX . '::mini-cart', MiniCart::class);
        Livewire::component(self::PACKAGE_PREFIX . '::product-quantity', ProductQuantity::class);
        Livewire::component(self::PACKAGE_PREFIX . '::cart-addresses', CartAddresses::class);
        Livewire::component(self::PACKAGE_PREFIX . '::select-shipping', SelectShipping::class);
        Livewire::component(self::PACKAGE_PREFIX . '::cart', Cart::class);
        Livewire::component(self::PACKAGE_PREFIX . '::livewire.orders-table', OrdersDatatable::class);
        Livewire::component(
            self::PACKAGE_PREFIX . '::livewire.client-orders-table',
            ClientOrdersDatatable::class
        );
        Livewire::component(
            self::PACKAGE_PREFIX . '::livewire.order-product-meta-editor',
            OrderProductMetaEditor::class
        );
        Livewire::component(self::PACKAGE_PREFIX . '::order-editor', OrderEditor::class);
        Livewire::component(self::PACKAGE_PREFIX . '::order-product-editor', OrderProductEditor::class);
        Livewire::component(self::PACKAGE_PREFIX . '::order-product-creator', OrderProductCreator::class);

        $this->mergeConfigFrom(__DIR__ . '/../config/orders.php', 'orders');

        $this->publishes([__DIR__.'/../config/orders.php' => config_path('orders.php'),]);

        $this->registerMediaConversions();
    }

    /**
     * @throws BindingResolutionException
     */
    public function registerMediaConversions(): void
    {
        $multimediaModule = $this->app->make(MultimediaModule::class);
        $multimediaModule->registerMediaConversion(new MediaConversionDto(
            'cart-product-thumb',
            200,
            200,
            'images'
        ));
    }

    private function registerEventListeners(): void
    {
        Event::listen(OrderConfirmed::class, [ClientOrderConfirmationEmailListener::class, 'handle']);
        Event::listen(OrderConfirmed::class, [AdminOrderConfirmationEmailListener::class, 'handle']);
        Event::listen(Login::class, [ClientLoginListener::class, 'handle']);
        Event::listen(
            ProductHasBeenRemovedFromCart::class,
            [RemoveInvalidCouponsAfterProductDeletion::class, 'handle']
        );
        Event::listen(
            ProductQuantityHasBeenUpdated::class,
            RemoveInvalidCouponsAfterProductQuantityChange::class
        );
        Event::listen(OrderConfirmed::class, [GuestOrderConfirmedListener::class, 'handle']);
    }
}
