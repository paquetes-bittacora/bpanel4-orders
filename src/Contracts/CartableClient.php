<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Contracts;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Illuminate\Database\Eloquent\Model;

interface CartableClient
{
    /**
     * Devuelve el ID de CLIENTE. Si solo se usa la tabla de usuarios, el ID de
     * cliente será el mismo que el de usuario, pero puede haber casos en los
     * que sean tablas distintas.
     */
    public function getClientId(): int;

    public function getName(): string;

    public function getLastName(): string;

    public function getNif(): string;

    public function getCompany(): string;

    public function getPhone(): string;

    public function getEmail(): string;

    /**
     * Devuelve el objeto correspondiente al cliente
     * @return CartableClient&Model No lo pongo directamente en el tipo de retorno para poder hacer mocks
     */
    public function getClient(): CartableClient;

    public function getShippingAddress(): ModelAddress;

    public function getBillingAddress(): ModelAddress;
}
