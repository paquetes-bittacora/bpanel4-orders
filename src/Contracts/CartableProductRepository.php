<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Contracts;

interface CartableProductRepository
{
    public function getByProductId(int $productId): CartableProduct;
}
