<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Contracts;

use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Contracts\ApplicableVatRate;
use Illuminate\Support\Collection;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Interfaz que deben implementar los modelos que puedan ser pedidos y añadidos al carrito.
 *
 * No asumimos nada sobre el producto, solo que puede devolvernos los datos que indican estos
 * métodos.
 */
interface CartableProduct
{
    /**
     * Obtiene el ID del producto
     */
    public function getId(): int;

    /**
     * Obtiene el ID de la relación entre CartableProduct y Product, es decir
     * el id de la fila en la tabla pivote.
     */
    public function getCartableProductId(): int;

    /**
     * Devuelve el nombre del producto
     */
    public function getName(): string;

    /**
     * Devuelve el precio unitario del producto sin ningún descuento.
     *
     * Se usa int para evitar errores de coma flotante, así que el precio tendrá que venir en céntimos, es decir,
     * que si un producto vale 10,50 €, el valor devuelto debe ser 1050.
     */
    public function getOriginalUnitPrice(): Price;

    /**
     * Devuelve el precio unitario del producto con los descuentos aplicados
     */
    public function getDiscountedUnitPrice(): ?Price;

    /**
     * Devuelve el precio a pagar del producto. Si el producto tiene descuento, se devolverá el precio con descuento,
     * si no, el precio sin descuento.
     */
    public function getUnitPrice(): Price;

    public function getUnitPriceWithoutVat(): Price;

    public function getOriginalUnitPriceWithoutVat(): Price;

    /**
     * Devuelve el total del producto, es decir el precio unitario por el número de unidades.
     */
    public function getTotal(): Price;

    public function getTotalWithoutVat(): Price;

    /**
     * Obtiene la referencia del producto
     */
    public function getReference(): string;

    /**
     * @return Collection<int, Media>
     */
    public function getImages(): Collection;

    public function getVatRate(): ?ApplicableVatRate;

    public function getQuantity(): int;

    public function getProductId(): int;

    public function getStock(): int;
}
