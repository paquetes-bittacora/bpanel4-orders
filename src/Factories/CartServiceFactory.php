<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Factories;

use Bittacora\Bpanel4\Coupons\Services\CartDiscountCalculator;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Shipping\Services\ShippingCostsCalculator;
use Bittacora\Bpanel4\Vat\Services\ShippingVatService;
use Bittacora\Bpanel4\Vat\Services\VatService;

final class CartServiceFactory
{
    public function __construct(
        private readonly ShippingCostsCalculator $shippingCostsCalculator,
        private readonly VatService $vatService,
        private readonly CartDiscountCalculator $cartDiscountCalculator,
        private readonly ShippingVatService $shippingVatService,
    ) {
    }

    public function getCartService(Cart $cart): CartService
    {
        return new CartService(
            $cart,
            $this->shippingCostsCalculator,
            $this->vatService,
            $this->cartDiscountCalculator,
            $this->shippingVatService,
        );
    }
}
