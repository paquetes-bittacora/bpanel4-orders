<?php

namespace Bittacora\Bpanel4\Orders\Traits;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Illuminate\Database\Eloquent\Relations\MorphOne;

trait HasCartTrait
{
    /**
     * @return MorphOne<Cart>
     */
    public function cart(): MorphOne
    {
        return $this->morphOne(Cart::class, 'cartable');
    }
}
