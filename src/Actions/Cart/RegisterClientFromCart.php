<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Cart;

use Bittacora\Bpanel4\Clients\Contracts\Client;
use Bittacora\Bpanel4\Clients\Contracts\ClientRepository;
use Bittacora\Bpanel4\Orders\Services\ClientCartService;
use Bittacora\Bpanel4Users\Models\User;
use Bittacora\Bpanel4\Clients\UseCases\RegisterNewClient;
use Illuminate\Database\Connection;
use Illuminate\Validation\ValidationException;
use Throwable;

final class RegisterClientFromCart
{
    private Client $client;

    public function __construct(
        private readonly Connection $db,
        private readonly RegisterNewClient $registerNewClient,
        private readonly ClientRepository $clientRepository,
        private readonly ClientCartService $clientCartService,
    ) {
    }

    /**
     * @param array<string, string> $data
     * @throws Throwable
     * @throws ValidationException
     */
    public function execute(array $data): User
    {
        $this->db->beginTransaction();
        try {
            $user = $this->registerNewClient->execute($data);
            $user->setIsGuest(true);
            $user->save();
            $this->client = $this->clientRepository->getByUserId($user->getId());
            $this->moveAnonymousCartToClient();
            $cart = $this->clientCartService->get($this->client);
            $cart->setShippingAddress($this->client->getShippingAddress());
            $cart->setBillingAddress($this->client->getShippingAddress());
            $this->db->commit();
            return $user;
        } catch (Throwable $exception) {
            report($exception);
            $this->db->rollBack();
            throw $exception;
        }
    }

    private function moveAnonymousCartToClient(): void
    {
        $cart = $this->clientCartService->getAnonymousCart();
        $cart->client_type = $this->client::class;
        $cart->client_id = $this->client->getClientId();
        $cart->setShippingAddress($this->client->getShippingAddress());
        $cart->save();
    }
}
