<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Cart;

use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;

/**
 * Añade un producto al carrito
 */
class AddProductToCart
{
    public function execute(Cart $cart, CartableProduct $product, int $quantity): void
    {
        $cart->addProduct($product, $quantity);
    }
}
