<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Cart;

use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Orders\Contracts\CartableProductRepository;

class SetProductQuantity
{
    public function __construct(
        private readonly ClientService $clientService,
        private readonly CartableProductRepository $cartableProductRepository,
    ) {
    }

    public function execute(int $productId, int $quantity): void
    {
        $cart = $this->clientService->getClientCart();
        $cartProduct = $this->cartableProductRepository->getByProductId($productId);
        $cart->setProductQuantity($cartProduct, $quantity);
    }
}
