<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Cart;

use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Events\CartIsBeingConfirmed;
use Bittacora\Bpanel4\Orders\Exceptions\NotEnoughProductStockException;
use Bittacora\Bpanel4\Orders\Exceptions\OrderNotSavedException;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Orders\Services\OrderSaver;
use Bittacora\Bpanel4\Orders\Services\ShippingOptionValidator;
use Bittacora\Bpanel4\Orders\Services\StockDiscounter;
use Bittacora\Bpanel4\Payment\Contracts\OrderDetailsDto;
use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Bittacora\Bpanel4\Payment\Exceptions\InvalidPaymentMethodException;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Exceptions\ShippingOptionNotFoundException;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config;
use Throwable;

final class ConfirmCart
{
    private Cart $cart;

    public function __construct(
        private readonly ShippingOptionValidator $shippingOptionValidator,
        private readonly CartService $cartService,
        private readonly OrderSaver $orderSaver,
        private readonly StockDiscounter $stockDiscounter,
        private readonly Dispatcher $dispatcher,
    ) {
    }

    /**
     * @throws InvalidPriceException
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws ShippingOptionNotFoundException
     * @throws Throwable
     */
    public function handle(Cart $cart): RedirectResponse
    {
        $this->cart = $cart;
        $this->cartService->setCart($cart);
        $this->shippingOptionValidator->validateSelectedShippingOption($cart);
        $this->dispatcher->dispatch(new CartIsBeingConfirmed($cart));
        $this->checkProductStock($cart);

        $order = $this->saveOrder($cart, $this->cartService->getDiscountByUnit());
        $this->stockDiscounter->discountProductStock($cart->getProducts());

        return $this->processPayment(new OrderDetailsDto(
            $order->getId(),
            Price::fromInt($order->getOrderTotal()),
            ''
        ));
    }

    /**
     * @throws InvalidPaymentMethodException
     */
    private function processPayment(OrderDetailsDto $orderDetails): RedirectResponse
    {
        $paymentMethod = $this->cart->getPaymentMethod();

        if (!$paymentMethod instanceof PaymentMethod) {
            throw new InvalidPaymentMethodException();
        }

        return $paymentMethod->processPayment($orderDetails);
    }

    /**
     * @throws ShippingOptionNotFoundException
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws Throwable
     * @throws OrderNotSavedException
     * @throws InvalidPriceException
     */
    private function saveOrder(Cart $cart, array $discountByUnit = []): Order
    {
        return $this->orderSaver->save(
            $this->cartService->getSelectedShippingMethod(),
            $cart->getShippingAddress(),
            $cart->getBillingAddress(),
            $cart->getClient(),
            $cart->getProducts(),
            $this->cartService->getSubtotal(),
            $this->cartService->getTotalTaxes(),
            $this->cartService->getSubtotalWithTaxesAndDiscount(),
            $this->cartService->getShippingCosts(),
            $this->cartService->getShippingCostsTaxes(),
            $this->cartService->getCartTotalWithDiscount(),
            $this->cart->getPaymentMethod(),
            $cart->getVatNumber(),
            $cart->getIp(),
            $cart->getCountryCode(),
            $discountByUnit,
            $cart->getOrderNotes(),
            $cart->getGiftedProducts(),
        );
    }

    /**
     * @throws NotEnoughProductStockException
     */
    private function checkProductStock(Cart $cart): void
    {
        if (Config::get('orders.ignore_stock', false) === true) {
            return;
        }

        foreach ($cart->getProducts() as $product) {
            if ($product->getQuantity() > $product->getStock()) {
                throw new NotEnoughProductStockException(
                    'No hay stock suficiente del producto "'. strip_tags($product->getName()) .
                    '". No se puede hacer el pedido'
                );
            }
        }
    }
}
