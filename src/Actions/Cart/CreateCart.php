<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Cart;

use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Bittacora\Bpanel4\Orders\Exceptions\CartAlreadyExistsException;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use function request;

class CreateCart
{
    /**
     * @throws CartAlreadyExistsException
     */
    public function execute(CartableClient $cartableClient): void
    {
        $this->checkIfCartAlreadyExistsForClient($cartableClient);
        $cart = new Cart();
        $cart->setClient($cartableClient->getClient());
        $cart->setIp(request()->ip());
        $cart->save();
    }

    /**
     * @throws CartAlreadyExistsException
     */
    private function checkIfCartAlreadyExistsForClient(CartableClient $cartableClient): void
    {
        if (Cart::whereRelation('client', 'id', $cartableClient->getClientId())->count() > 0) {
            throw new CartAlreadyExistsException();
        }
    }
}
