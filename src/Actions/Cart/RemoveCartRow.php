<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Cart;

use Bittacora\Bpanel4\Orders\Events\ProductHasBeenRemovedFromCart;
use Bittacora\Bpanel4\Orders\Models\Cart\CartRow;
use Illuminate\Support\Facades\Cache;

class RemoveCartRow
{
    public function execute(CartRow $cartRow): void
    {
        $cartId = $cartRow->cart_id;
        $cartableType = $cartRow->cartable_type;
        $cartProduct = (new $cartableType)->whereId($cartRow->cartable_id)->firstOrFail();
        $cartRow->delete();

        Cache::driver('array')->clear();
        ProductHasBeenRemovedFromCart::dispatch($cartId, $cartProduct);
    }
}
