<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Order;

use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;

final class CreateOrderProduct
{
    /**
     * @throws InvalidPriceException
     */
    public function execute(
        int $orderId,
        string $name,
        string $reference,
        int $quantity,
        Price $unitPrice,
        float $taxRate,
    ): void {
        $orderProduct = new OrderProduct();
        $orderProduct->setOrderId($orderId);
        $orderProduct->setName($name);
        $orderProduct->setReference($reference);
        $orderProduct->setQuantity($quantity);
        $orderProduct->setUnitPrice($unitPrice);
        $orderProduct->setTaxRate($taxRate);
        $taxAmount = $this->calculateTaxAmount($taxRate, $quantity, $unitPrice);
        $orderProduct->setTaxAmount($taxAmount);
        $orderProduct->setTotal(Price::fromInt($quantity * $unitPrice->toInt()));
        $orderProduct->save();
    }

    /**
     * @throws InvalidPriceException
     */
    private function calculateTaxAmount(float $taxRate, int $quantity, Price $unitPrice): Price
    {
        return new Price($taxRate / 100 * ($unitPrice->toFloat() * $quantity));
    }
}