<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Order;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderGiftedProduct;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;

final class RemoveOrderGiftedProduct
{
    public function __construct(private readonly RecalculateOrderTotals $recalculateOrderTotals)
    {
    }

    /**
     * @throws InvalidPriceException
     */
    public function execute(OrderGiftedProduct|int $orderProduct): void
    {
        if (is_int($orderProduct)) {
            $orderProduct = OrderGiftedProduct::whereId($orderProduct)->firstOrFail();
        }
        $orderProduct->delete();
        $this->recalculateOrderTotals->execute(Order::whereId($orderProduct->getOrderId())->firstOrFail());
    }
}