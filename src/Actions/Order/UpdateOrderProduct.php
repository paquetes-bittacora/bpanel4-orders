<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Order;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;

final class UpdateOrderProduct
{
    public function __construct(private readonly RecalculateOrderTotals $recalculateOrderTotals)
    {
    }

    /**
     * @throws InvalidPriceException
     */
    public function execute(
        OrderProduct $orderProduct,
        string $name,
        string $reference,
        int $quantity,
        Price $paidUnitPrice,
        float $vatRate,
    ): void {
        $orderProduct->setName($name);
        $orderProduct->setReference($reference);
        $orderProduct->setQuantity($quantity);
        $orderProduct->setUnitPrice($paidUnitPrice);
        $orderProduct->setTaxRate($vatRate);
        $orderProduct->setTaxAmount(new Price($vatRate / 100 * $paidUnitPrice->toFloat()));
        $newTotal = $paidUnitPrice->toInt() * $quantity;
        $orderProduct->setTotal(Price::fromInt($newTotal));
        $orderProduct->save();
        $this->updateOrder($orderProduct);
    }

    private function updateOrder(OrderProduct $orderProduct): void
    {
        $order = Order::whereId($orderProduct->getOrderId())->firstOrFail();
        $this->recalculateOrderTotals->execute($order);
    }
}