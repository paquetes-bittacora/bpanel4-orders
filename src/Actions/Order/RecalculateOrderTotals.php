<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Order;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Contracts\VatModule;
use Bittacora\Bpanel4\Vat\Models\VatRate;

final class RecalculateOrderTotals
{
    public function __construct(private readonly VatModule $vatModule)
    {
    }

    /**
     * @throws InvalidPriceException
     */
    public function execute(Order $order): void {
        $subtotalWithoutTaxes = 0;
        $totalProductTaxes = 0;

        foreach ($order->getProducts() as $product) {
            $subtotalWithoutTaxes += $product->getUnitPrice()->toInt() * $product->getQuantity();
            $productTaxes = Price::fromInt($this->vatModule->getVatForPrice(
                    (new Price($product->getUnitPrice()->toFloat()))->toInt() * $product->getQuantity(),
                    $this->vatModule->getVatRateFromRate($product->getTaxRate())
            ));
            $product->setTaxAmount($productTaxes);
            $totalProductTaxes += $productTaxes->toInt();
        }

        $order->setSubtotalWithoutTaxes($subtotalWithoutTaxes);
        $order->setSubtotal($subtotalWithoutTaxes);
        $order->setTotalTaxes($totalProductTaxes + $order->getShippingCostsTaxes());
        $order->setOrderTotal($subtotalWithoutTaxes + $order->getShippingCosts() + $order->getTotalTaxes());
        $order->save();
    }
}