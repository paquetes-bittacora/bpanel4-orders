<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Order;

use Bittacora\Bpanel4\Invoices\Exceptions\CouldNotCreateInvoiceException;
use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\Invoices\Services\OrderInvoiceGenerator;
use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use Doctrine\DBAL\Driver\Exception;
use Throwable;

final class UpdateOrderStatus
{
    public function __construct(
        private readonly OrderInvoiceGenerator $orderInvoiceGenerator,
    ) {
    }

    /**
     * @throws Throwable
     * @throws CouldNotCreateInvoiceException
     * @throws Exception
     */
    public function handle(Order $order, OrderStatusModel $orderStatus): void
    {
        if ($this->shouldResendInvoice($order, $orderStatus)) {
            $this->regenerateInvoice($order);
        }

        $order->setStatus($orderStatus);
        $order->save();

        if (
            $orderStatus->id === OrderStatus::COMPLETED->value &&
            config('orders.generate_invoice_when_payment_completed') === true
        ) {
            $this->orderInvoiceGenerator->getFilePath($order);
        }
    }

    private function shouldResendInvoice(Order $order, OrderStatusModel $orderStatus): bool
    {
        $invoiceAlreadyExists = Invoice::where('order_id', $order->id)->exists();
        return (
            $order->getStatus()->id === OrderStatus::WAITING->value &&
            $orderStatus->id === OrderStatus::COMPLETED->value &&
            config('orders.resend_invoice_after_modification', false)
        ) || (
            !$invoiceAlreadyExists &&
            config('bpanel4-invoices.send_invoice_to_email_when_generated', false) &&
            $orderStatus->id === OrderStatus::COMPLETED->value
        );
    }

    /**
     * @throws Throwable
     * @throws CouldNotCreateInvoiceException
     * @throws Exception
     */
    private function regenerateInvoice(Order $order): void
    {
        $this->orderInvoiceGenerator->generateInvoice($order, true);
    }
}
