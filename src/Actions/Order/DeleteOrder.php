<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Order;

use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;

final class DeleteOrder
{
    public function handle(Order $order): void
    {
        $order->billingDetails()->delete();
        $order->client()->delete();
        $order->products()->delete();
        $order->shippingDetails()->delete();
        $order->billingDetails()->delete();
        $this->deleteInvoice($order);
        $order->delete();
    }

    private function deleteInvoice(Order $order): void
    {
        $invoice = Invoice::where('order_id', $order->getId())->first();

        if (null === $invoice) {
            return;
        }
        $invoiceNumber = $invoice->invoice_number;
        $invoice->forceDelete();

        $this->decreaseNextInvoiceNumber($invoiceNumber);
    }

    private function decreaseNextInvoiceNumber(int $deletedInvoiceNumber): void
    {
        $shopConfiguration = ShopConfiguration::whereId(1)->firstOrFail();

        if ($deletedInvoiceNumber === $shopConfiguration->next_invoice_number - 1) {
            --$shopConfiguration->next_invoice_number;
            $shopConfiguration->save();
        }
    }
}
