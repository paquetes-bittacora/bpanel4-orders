<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Order;

use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\Orders\Dtos\OrderDataDto;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Database\DatabaseManager;
use Throwable;

final class UpdateOrderDetails
{
    private OrderDataDto $dto;
    private Order $order;

    public function __construct(
        private readonly DatabaseManager $db,
        private readonly ExceptionHandler $exceptionHandler,
    ) {
    }

    /**
     * @throws Throwable
     */
    public function execute(Order $order, OrderDataDto $dto): void
    {
        $this->order = $order;
        $this->dto = $dto;
        try {
            $this->db->beginTransaction();
            $this->updateClientDetails();
            $this->updateShippingDetails();
            $this->updateBillingDetails();
            $this->updateInvoiceNumber();
            $this->updateOrderNotes();
            $this->db->commit();
        } catch (Throwable $throwable) {
            $this->db->rollBack();
            $this->exceptionHandler->report($throwable);
            throw $throwable;
        }
    }

    private function updateClientDetails(): void
    {
        $client = $this->order->getClient();
        $client->name = $this->dto->clientName;
        $client->surname = $this->dto->clientSurname;
        $client->nif = $this->dto->clientNif;
        $client->company = $this->dto->clientCompany;
        $client->phone = $this->dto->clientPhone;
        $client->email = $this->dto->clientEmail;
        $client->save();
    }

    private function updateShippingDetails(): void
    {
        $shippingDetails = $this->order->getShippingDetails();
        $shippingDetails->person_name = $this->dto->clientShippingAddressName;
        $shippingDetails->person_surname = $this->dto->clientShippingAddressSurname;
        $shippingDetails->person_phone = $this->dto->clientShippingAddressPhone;
        $shippingDetails->address = $this->dto->clientShippingAddress;
        $shippingDetails->country_id = $this->dto->clientShippingCountry;
        $shippingDetails->state_id = $this->dto->clientShippingState;
        $shippingDetails->location = $this->dto->clientShippingLocation;
        $shippingDetails->postal_code = $this->dto->clientShippingPostalCode;
        $shippingDetails->save();
    }

    private function updateBillingDetails(): void
    {
        $billingDetails = $this->order->getBillingDetails();
        $billingDetails->person_name = $this->dto->clientBillingAddressName;
        $billingDetails->person_surname = $this->dto->clientBillingAddressSurname;
        $billingDetails->person_phone = $this->dto->clientBillingAddressPhone;
        $billingDetails->address = $this->dto->clientBillingAddress;
        $billingDetails->country_id = $this->dto->clientBillingCountry;
        $billingDetails->state_id = $this->dto->clientBillingState;
        $billingDetails->location = $this->dto->clientBillingLocation;
        $billingDetails->postal_code = $this->dto->clientBillingPostalCode;
        $billingDetails->person_nif = $this->dto->clientBillingAddressNif;
        $billingDetails->save();
    }

    private function updateInvoiceNumber(): void
    {
        $invoice = Invoice::where('order_id', $this->order->getId())->first();

        if (null !== $invoice) {
            $invoice->invoice_number = $this->dto->invoiceNumber;
            $invoice->save();
        }
    }

    private function updateOrderNotes(): void
    {
        $this->order->setOrderNotes($this->dto->orderNotes);
        $this->order->save();
    }
}