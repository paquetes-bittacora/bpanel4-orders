<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Actions\Order;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;

final class RemoveOrderProduct
{
    public function __construct(private readonly RecalculateOrderTotals $recalculateOrderTotals)
    {
    }

    /**
     * @throws InvalidPriceException
     */
    public function execute(OrderProduct|int $orderProduct): void
    {
        if (is_int($orderProduct)) {
            $orderProduct = OrderProduct::whereId($orderProduct)->firstOrFail();
        }
        $orderProduct->delete();
        $this->recalculateOrderTotals->execute(Order::whereId($orderProduct->getOrderId())->firstOrFail());
    }
}