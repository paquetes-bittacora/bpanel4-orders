<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Events;

use Bittacora\Bpanel4\Products\Models\CartProduct;
use Illuminate\Foundation\Events\Dispatchable;

final class ProductHasBeenRemovedFromCart
{
    use Dispatchable;

    public function __construct(public readonly int $cartId, public readonly CartProduct $cartProduct)
    {
    }
}