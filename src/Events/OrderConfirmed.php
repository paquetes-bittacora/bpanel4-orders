<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Events;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Illuminate\Foundation\Events\Dispatchable;

final class OrderConfirmed
{
    use Dispatchable;

    public function __construct(public readonly Order $order)
    {
    }
}
