<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Events;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Illuminate\Foundation\Events\Dispatchable;

final class CartIsBeingConfirmed
{
    use Dispatchable;

    public function __construct(public readonly Cart $cart)
    {
    }
}
