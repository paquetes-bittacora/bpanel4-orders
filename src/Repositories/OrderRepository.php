<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Repositories;

use Bittacora\Bpanel4\Orders\Models\Order\Order;

final class OrderRepository
{
    public function getById(int $orderId): Order
    {
        return Order::whereId($orderId)->firstOrFail();
    }
}
