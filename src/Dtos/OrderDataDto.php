<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Dtos;

use Bittacora\Dtos\Dto;

final class OrderDataDto extends Dto
{
    public function __construct(
        public readonly string $clientName = '',
        public readonly string $clientSurname = '',
        public readonly string $clientNif = '',
        public readonly string $clientPhone = '',
        public readonly string $clientEmail = '',
        public readonly string $clientShippingAddressName,
        public readonly string $clientShippingAddressSurname,
        public readonly string $clientShippingAddress,
        public readonly int $clientShippingCountry,
        public readonly int $clientShippingState,
        public readonly string $clientShippingLocation,
        public readonly string $clientShippingPostalCode,
        public readonly string $clientBillingAddressName,
        public readonly string $clientBillingAddressSurname,
        public readonly string $clientBillingAddress,
        public readonly int $clientBillingCountry,
        public readonly int $clientBillingState,
        public readonly string $clientBillingLocation,
        public readonly string $clientBillingPostalCode,
        public readonly ?string $clientCompany = '',
        public readonly ?string $invoiceNumber = null,
        public readonly ?string $orderNotes = null,
        public readonly ?string $clientShippingAddressPhone = null,
        public readonly ?string $clientBillingAddressPhone = null,
        public readonly ?string $clientBillingAddressNif = null,
    ) {
    }

}