<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Mail;

use Bittacora\Bpanel4\Invoices\Services\OrderInvoiceGenerator;
use Bittacora\Bpanel4\Invoices\Services\TaxBreakdownCalculator;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Mail\Mailable;

final class AdminOrderSummaryMail extends Mailable
{
    public function __construct(
        private readonly Order $order,
        private readonly Repository $config,
        private readonly TaxBreakdownCalculator $taxBreakdownCalculator,
        private readonly OrderInvoiceGenerator $orderInvoiceGenerator,
    ) {
    }

    /**
     * @throws InvalidPriceException
     */
    public function build(): AdminOrderSummaryMail
    {
        return $this->subject('Se ha recibido un nuevo pedido (esperando confirmación de pago)')
            ->view('bpanel4-orders::mail.admin-order-details', [
                'clientDetails' => $this->order->getClient(),
                'shippingDetails' => $this->order->getShippingDetails(),
                'billingDetails' => $this->order->getBillingDetails(),
                'products' => $this->order->getProducts(),
                'giftedProducts' => $this->order->getGiftedProducts(),
                'order' => $this->order,
                'shopName' => $this->config->get('app.name'),
                'taxBreakdown' => $this->getTaxBreakdown($this->order),
            ]);
    }

    /**
     * @param Order $order
     * @return \Bittacora\Bpanel4\Invoices\Dtos\TaxBreakdown[]
     * @throws \Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException
     */
    private function getTaxBreakdown(Order $order): array
    {
        $taxBreakdown = $this->taxBreakdownCalculator->calculateBreakdown(
            $order->getProducts(),
            Price::fromInt($order->getShippingCosts()),
            $order->getDiscountByUnit(),
        );
        return $taxBreakdown;
    }
}
