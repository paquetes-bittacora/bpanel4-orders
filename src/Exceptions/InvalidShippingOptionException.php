<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Exceptions;

use RuntimeException;

final class InvalidShippingOptionException extends RuntimeException
{
    /** @var string */
    protected $message = 'La opción de envío seleccionada no es válida';
}
