<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Exceptions;

use Exception;

final class OrderNotSavedException extends Exception
{
    /** @var string $message */
    protected $message = 'Ocurrió un error al guardar su pedido';
}
