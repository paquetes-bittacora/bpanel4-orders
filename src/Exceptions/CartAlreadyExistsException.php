<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Exceptions;

use Exception;

class CartAlreadyExistsException extends Exception
{
    /** @var string $message */
    protected $message = 'El cliente ya tiene un carrito';
}
