<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Models\Cart;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use RuntimeException;

/**
 * Bittacora\Bpanel4\Orders\Models\Cart\Cart
 *
 * @property int $id
 * @property string $client_type
 * @property int $client_id
 * @property string $selected_shipping_method
 * @property ?string $order_notes
 * @property ?string $payment_method
 * @property int $shipping_address_id
 * @property int $billing_address_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Model|Eloquent $client
 * @property-read Collection|CartableProduct[] $products
 * @property-read int|null $products_count
 * @property ?string $ip_address
 * @property ?string $vat_number
 * @property ?string $country_code
 * @method static Builder|Cart newModelQuery()
 * @method static Builder|Cart newQuery()
 * @method static Builder|Cart query()
 * @method static Builder|Cart whereClientId($value)
 * @method static Builder|Cart whereClientType($value)
 * @method static Builder|Cart whereCreatedAt($value)
 * @method static Builder|Cart whereId($value)
 * @method static Builder|Cart whereUpdatedAt($value)
 * @mixin Eloquent
 */
final class Cart extends Model
{
    use SoftDeletes;

    private bool $enableCoupons = true;

    /**
     * @return Collection<int, CartableProduct>
     */
    public function getGiftedProducts(): Collection
    {
        return $this->giftedProducts()->get();
    }

    /**
     * @return MorphTo<CartableClient, Cart>
     */
    public function client(): MorphTo
    {
        /** @phpstan-ignore-next-line */
        return $this->morphTo();
    }

    /**
     * @return MorphToMany<CartableProduct>
     */
    public function products(): MorphToMany
    {
        return $this->morphedByMany($this->getProductClass(), 'cartable', 'cart_products')->withPivot(['quantity', 'id']);
    }

    /**
     * @return MorphToMany<CartableProduct>
     */
    public function giftedProducts(): MorphToMany
    {
        return $this->morphedByMany($this->getProductClass(), 'cartable', 'cart_gifted_products')->withPivot(['quantity', 'id']);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setClient(CartableClient $client): void
    {
        $this->client()->associate($client);
    }

    public function getClient(): CartableClient
    {
        return $this->client()->get()->firstOrFail();
    }

    private function getProductClass(): string
    {
        if (!Config::has('orders.product_class')) {
            throw new RuntimeException('No se ha establecido orders.product_class');
        }
        return config('orders.product_class');
    }

    public function addProduct(CartableProduct $product, int $quantity): void
    {
        $productInCart = $this->products()->wherePivot('cartable_id', $product->getId())->first();

        if ($productInCart instanceof CartableProduct) {
            $newQuantity = $productInCart->getQuantity() + $quantity;
            $this->setProductQuantity($product, $newQuantity);
            return;
        }

        $this->products()->attach($product->getId(), ['quantity' => $quantity]);
    }

    public function addGiftedProduct(Coupon $coupon, CartableProduct $product, int $quantity): void
    {
        $this->giftedProducts()->attach($product->getId(), ['quantity' => $quantity, 'coupon_id' => $coupon->getId()]);
    }

    /**
     * @return Collection<int, CartableProduct>
     */
    public function getProducts(): Collection
    {
        return Cache::store('array')->remember('cart-products-' . $this->getId(), 10, function () {
        return $this->products()->get();
        });
    }

    public function getItemCount(): int
    {
        $output = 0;

        foreach ($this->products()->get() as $product) {
            $output += $product->getQuantity();
        }

        return $output;
    }

    public function setProductQuantity(CartableProduct $product, int $quantity): void
    {
        $this->products()->updateExistingPivot($product->getId(), [
            'quantity' => $quantity,
        ]);
        Cache::forget('cart-products-' . $this->getId());
    }

    public function setSelectedShippingMethod(string $shippingMethod): void
    {
        $this->selected_shipping_method = $shippingMethod;
    }

    public function getSelectedShippingMethod(): ?string
    {
        return $this->selected_shipping_method;
    }

    public function setShippingAddress(ModelAddress $address): void
    {
        if (null !== $address->id) {
            $this->shippingAddress()->associate($address->getId());
            $this->save();
        }
    }

    public function getShippingAddress(): ModelAddress
    {
        $address = $this->shippingAddress()->where('id', $this->shipping_address_id)->first();

        if (!$this->shipping_address_id) {
            $address = new ModelAddress();
            $address->country()->associate(Country::whereName('España')->firstOrFail());
            $address->state()->associate(State::whereName('Badajoz')->firstOrFail());
            $address->postal_code = '00000';
            $address->location = '';
            return $address;
        }

        return $address ?? $this->getClient()->getShippingAddress();
    }

    public function setBillingAddress(ModelAddress $address): void
    {
        $this->billingAddress()->associate($address->getId());
        $this->save();
    }

    public function getBillingAddress(): ModelAddress
    {
        $billingAddress = $this->billingAddress()->where('id', $this->billing_address_id)->first();
        return $billingAddress ?? $this->getClient()->getBillingAddress();
    }

    /**
     * @return BelongsTo<ModelAddress, Cart>
     */
    public function shippingAddress(): BelongsTo
    {
        return $this->belongsTo(ModelAddress::class, 'shipping_address_id');
    }

    /**
     * @return BelongsTo<ModelAddress, Cart>
     */
    public function billingAddress(): BelongsTo
    {
        return $this->belongsTo(ModelAddress::class, 'billing_address_id');
    }

    public function getPaymentMethod(): ?PaymentMethod
    {
        return resolve($this->payment_method);
    }

    public function setPaymentMethod(?PaymentMethod $paymentMethod): void
    {
        $this->payment_method = $paymentMethod::class;
    }

    /**
     * @throws InvalidPriceException
     */
    public function getSubtotalWithTaxes(): Price
    {
        return Cache::store('array')->remember('subtotal-with-taxes-' . $this->getId(), 10, function () {
        $output = 0;

        foreach ($this->products()->get() as $product) {
                $output += ($product->getUnitPrice()->toInt())* $product->getQuantity();
        }

        return Price::fromInt($output);
        });
    }

    public function setIp(string $ip): void
    {
        $this->ip_address = $ip;
    }

    public function getIp(): string {
        return $this->ip_address ?? '';
    }

    /**
     * @return string|null
     */
    public function getVatNumber(): ?string
    {
        return $this->vat_number;
    }

    /**
     * @param string|null $vat_number
     */
    public function setVatNumber(?string $vat_number): void
    {
        $this->vat_number = $vat_number;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->country_code;
    }

    /**
     * @param string|null $country_code
     */
    public function setCountryCode(?string $country_code): void
    {
        $this->country_code = $country_code;
    }

    public function appliedCoupons(): BelongsToMany
    {
        return $this->belongsToMany(Coupon::class, 'coupons_carts');
    }

    public function addCoupon(Coupon $coupon): void
    {
        $this->appliedCoupons()->save($coupon);
    }

    /**
     * @return Collection<Coupon>
     */
    public function getCoupons(): Collection
    {
        if (!$this->enableCoupons) {
            return new Collection();
        }

        $coupons = $this->appliedCoupons()->get();

        if (null === $coupons || $coupons->count() === 0) {
            return new Collection();
        }

        return $coupons;
    }

    public function disableCoupons(): void
    {
        $this->enableCoupons = false;
    }

    public function enableCoupons(): void
    {
        $this->enableCoupons = true;
    }

    public function getOrderNotes(): ?string
    {
        return $this->order_notes;
    }

    public function setOrderNotes(?string $order_notes): void
    {
        $this->order_notes = $order_notes;
    }
}
