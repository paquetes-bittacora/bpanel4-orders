<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Models\Cart;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Bittacora\Bpanel4\Orders\Models\Cart\CartRow
 *
 * @property int $id
 * @property int $cartable_id
 * @property string $cartable_type
 * @property int $cart_id
 * @property int $quantity
 * @method static Builder|CartRow newModelQuery()
 * @method static Builder|CartRow newQuery()
 * @method static Builder|CartRow query()
 * @method static Builder|CartRow whereCartId($value)
 * @method static Builder|CartRow whereCartableId($value)
 * @method static Builder|CartRow whereCartableType($value)
 * @method static Builder|CartRow whereId($value)
 * @method static Builder|CartRow whereQuantity($value)
 * @mixin Eloquent
 */
class CartRow extends Model
{
    public $table = 'cart_products';
    public $timestamps = false;
}
