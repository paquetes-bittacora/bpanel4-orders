<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Models\Order;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * \Bittacora\Bpanel4\Orders\Models\Order\OrderClientDetail
 *
 * @property int $id
 * @property int $order_id
 * @property int $client_id
 * @property string $name
 * @property string $surname
 * @property string $nif
 * @property string|null $company
 * @property string $phone
 * @property string $client_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|OrderClientDetail newModelQuery()
 * @method static Builder|OrderClientDetail newQuery()
 * @method static Builder|OrderClientDetail query()
 * @method static Builder|OrderClientDetail whereClientId($value)
 * @method static Builder|OrderClientDetail whereClientType($value)
 * @method static Builder|OrderClientDetail whereCompany($value)
 * @method static Builder|OrderClientDetail whereCreatedAt($value)
 * @method static Builder|OrderClientDetail whereId($value)
 * @method static Builder|OrderClientDetail whereName($value)
 * @method static Builder|OrderClientDetail whereNif($value)
 * @method static Builder|OrderClientDetail whereOrderId($value)
 * @method static Builder|OrderClientDetail wherePhone($value)
 * @method static Builder|OrderClientDetail whereSurname($value)
 * @method static Builder|OrderClientDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property ?string $email
 * @property-read \Bittacora\Bpanel4\Orders\Models\Order\Order $order
 * @method static Builder|OrderClientDetail whereEmail($value)
 */
final class OrderClientDetail extends Model
{
    use SoftDeletes;

    public function getId(): int
    {
        return $this->id;
    }

    public function getOrderId(): int
    {
        return $this->order_id;
    }

    public function setOrderId(int $order_id): void
    {
        $this->order_id = $order_id;
    }

    public function getClientId(): int
    {
        return $this->client_id;
    }

    public function setClientId(int $client_id): void
    {
        $this->client_id = $client_id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    public function getNif(): string
    {
        return $this->nif;
    }

    public function setNif(string $nif): void
    {
        $this->nif = $nif;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): void
    {
        $this->company = $company;
    }

    public function getPhone(): string
    {
        return $this->phone ?? '';
    }

    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    public function getClientType(): string
    {
        return $this->client_type;
    }

    public function setClientType(string $client_type): void
    {
        $this->client_type = $client_type;
    }

    /**
     * @return BelongsTo<Order, OrderClientDetail>
     */
    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }
}
