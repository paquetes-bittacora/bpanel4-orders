<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Models\Order;

use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 *
 * @method static Builder|OrderShippingDetail newModelQuery()
 * @method static Builder|OrderShippingDetail newQuery()
 * @method static Builder|OrderShippingDetail query()
 * @mixin Eloquent
 * @property int $id
 * @property int $order_id
 * @property int $country_id
 * @property int $state_id
 * @property string $postal_code
 * @property string $location
 * @property string $name
 * @property string $person_name
 * @property string $person_surname
 * @property string|null $person_nif
 * @property string|null $person_phone
 * @property string $address
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Order $order
 * @method static Builder|OrderShippingDetail whereAddress($value)
 * @method static Builder|OrderShippingDetail whereCountryId($value)
 * @method static Builder|OrderShippingDetail whereCreatedAt($value)
 * @method static Builder|OrderShippingDetail whereId($value)
 * @method static Builder|OrderShippingDetail whereLocation($value)
 * @method static Builder|OrderShippingDetail whereName($value)
 * @method static Builder|OrderShippingDetail whereOrderId($value)
 * @method static Builder|OrderShippingDetail wherePostalCode($value)
 * @method static Builder|OrderShippingDetail whereStateId($value)
 * @method static Builder|OrderShippingDetail whereUpdatedAt($value)
 */
final class OrderBillingDetail extends Model
{
    use SoftDeletes;

    public function setOrderId(int $orderId): void
    {
        $this->order_id = $orderId;
    }

    public function setCountryId(int $countryId): void
    {
        $this->country_id = $countryId;
    }

    public function setStateId(int $stateId): void
    {
        $this->state_id = $stateId;
    }

    public function setPostalCode(string $postalCode): void
    {
        $this->postal_code = $postalCode;
    }

    public function setLocation(string $location): void
    {
        $this->location = $location;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setPersonName(?string $personName = ''): void
    {
        $this->person_name = $personName;
    }

    public function setPersonSurname(?string $personSurname = ''): void
    {
        $this->person_surname = $personSurname;
    }

    public function getPersonName(): string
    {
        return $this->person_name ?? '';
    }

    public function getPersonSurname(): string
    {
        return $this->person_surname ?? '';
    }

    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getOrderId(): int
    {
        return $this->order_id;
    }

    public function getCountryId(): int
    {
        return $this->country_id;
    }

    public function getStateId(): int
    {
        return $this->state_id;
    }

    public function getPostalCode(): string
    {
        return $this->postal_code;
    }

    public function getLocation(): string
    {
        return $this->location;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return BelongsTo<Order, OrderBillingDetail>
     */
    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function getCountryName(): string
    {
        return Country::whereId($this->country_id)->first()?->name;
    }

    public function getStateName(): string
    {
        return State::whereId($this->state_id)->first()?->name;
    }

    public function getPersonPhone(): ?string
    {
        return $this->person_phone;
    }

    public function setPersonPhone(?string $person_phone): void
    {
        $this->person_phone = $person_phone;
    }

    public function getPersonNif(): ?string
    {
        return $this->person_nif;
    }

    public function setPersonNif(?string $person_nif): void
    {
        $this->person_nif = $person_nif;
    }
}
