<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Models\Order;

use Bittacora\Bpanel4\Prices\Casts\PriceCast;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * \Bittacora\Bpanel4\Orders\Models\Order\ProductDetails
 *
 * @method static Builder|OrderProduct newModelQuery()
 * @method static Builder|OrderProduct newQuery()
 * @method static Builder|OrderProduct query()
 * @mixin Eloquent
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property string $name
 * @property Price $unit_price
 * @property ?Price $discounted_unit_price
 * @property string $reference
 * @property int $quantity
 * @property float $tax_rate
 * @property Price $tax_amount
 * @property Price $total Total pagado por el producto (Imp. incl)
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|OrderProduct whereCreatedAt($value)
 * @method static Builder|OrderProduct whereDiscountedUnitPrice($value)
 * @method static Builder|OrderProduct whereId($value)
 * @method static Builder|OrderProduct whereName($value)
 * @method static Builder|OrderProduct whereOrderId($value)
 * @method static Builder|OrderProduct whereProductId($value)
 * @method static Builder|OrderProduct whereQuantity($value)
 * @method static Builder|OrderProduct whereReference($value)
 * @method static Builder|OrderProduct whereTaxAmount($value)
 * @method static Builder|OrderProduct whereTaxRate($value)
 * @method static Builder|OrderProduct whereTotal($value)
 * @method static Builder|OrderProduct whereUnitPrice($value)
 * @method static Builder|OrderProduct whereUpdatedAt($value)
 */
class OrderProduct extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'order_products';

    /**
     * @var array<string, class-string<\Bittacora\Bpanel4\Prices\Casts\PriceCast>>
     */
    protected $casts = [
        'unit_price' => PriceCast::class,
        'discounted_unit_price' => PriceCast::class,
        'tax_amount' => PriceCast::class,
        'total' => PriceCast::class,
    ];

    public function getId(): int
    {
        return $this->id;
    }

    public function getOrderId(): int
    {
        return $this->order_id;
    }

    public function setOrderId(int $order_id): void
    {
        $this->order_id = $order_id;
    }

    public function getProductId(): ?int
    {
        return $this->product_id;
    }

    public function setProductId(?int $productId): void
    {
        $this->product_id = $productId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getUnitPrice(): Price
    {
        return $this->unit_price;
    }

    public function setUnitPrice(Price $unitPrice): void
    {
        $this->unit_price = $unitPrice;
    }

    public function getDiscountedUnitPrice(): ?Price
    {
        return $this->discounted_unit_price;
    }

    public function getPaidUnitPrice(): Price
    {
        return $this->getDiscountedUnitPrice() ?? $this->getUnitPrice();
    }

    public function setDiscountedUnitPrice(?Price $discountedUnitPrice): void
    {
        $this->discounted_unit_price = $discountedUnitPrice;
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function setReference(string $reference): void
    {
        $this->reference = $reference;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    public function getTaxRate(): float
    {
        return $this->tax_rate;
    }

    public function setTaxRate(float $taxRate): void
    {
        $this->tax_rate = $taxRate;
    }

    public function getTaxAmount(): Price
    {
        return $this->tax_amount;
    }

    public function setTaxAmount(Price $taxAmount): void
    {
        $this->tax_amount = $taxAmount;
    }

    public function getTotal(): Price
    {
        return $this->total;
    }

    /**
     * @throws InvalidPriceException
     */
    public function getTotalWithTaxes(): Price
    {
        return Price::fromInt($this->total->toInt() + $this->tax_amount->toInt());
    }

    public function setTotal(Price $total): void
    {
        $this->total = $total;
    }

    /**
     * @return BelongsTo<Order, OrderProduct>
     */
    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
}
