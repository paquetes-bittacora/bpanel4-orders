<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Models\Order;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Bittacora\Bpanel4\Coupons\Models\OrderCoupon;
use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * \Bittacora\Bpanel4\Orders\Models\Order\Order
 *
 * @method static Builder|Order newModelQuery()
 * @method static Builder|Order newQuery()
 * @method static Builder|Order query()
 * @mixin Eloquent
 * @property int $id
 * @property int $client_id
 * @property string $client_type
 * @property int $subtotal_without_taxes
 * @property int $total_taxes
 * @property int $subtotal
 * @property int $shipping_costs
 * @property int $order_total
 * @property string $order_status_id
 * @property ?string $payment_status
 * @property string $payment_method
 * @property string $payment_method_name
 * @property ?string $ip_address
 * @property ?string $vat_number
 * @property ?string $country_code
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $discountByUnit
 * @property ?string $order_notes
 * @method static Builder|Order whereClientId($value)
 * @method static Builder|Order whereCreatedAt($value)
 * @method static Builder|Order whereId($value)
 * @method static Builder|Order whereOrderTotal($value)
 * @method static Builder|Order whereShippingCosts($value)
 * @method static Builder|Order whereSubtotal($value)
 * @method static Builder|Order whereSubtotalWithoutTaxes($value)
 * @method static Builder|Order whereTotalTaxes($value)
 * @method static Builder|Order whereUpdatedAt($value)
 */
final class Order extends Model
{
    use SoftDeletes;

    protected $casts = [
        'discount_by_unit' => 'array',
    ];

    public function getId(): int
    {
        return $this->id;
    }

    public function getClientId(): int
    {
        return $this->client_id;
    }

    public function setClientId(int $clientId): void
    {
        $this->client_id = $clientId;
    }

    public function getClientType(): string
    {
        return $this->client_type;
    }

    public function setClientType(string $client_type): void
    {
        $this->client_type = $client_type;
    }

    public function getSubtotalWithoutTaxes(): int
    {
        return $this->subtotal_without_taxes;
    }

    public function setShippingCostsTaxes(int $shippingCostsTaxes): void
    {
        $this->shipping_costs_taxes = $shippingCostsTaxes;
    }

    public function getShippingCostsTaxes(): int
    {
        return $this->shipping_costs_taxes;
    }

    public function setSubtotalWithoutTaxes(int $subtotalWithoutTaxes): void
    {
        $this->subtotal_without_taxes = $subtotalWithoutTaxes;
    }

    public function getTotalTaxes(): int
    {
        return $this->total_taxes;
    }

    public function setTotalTaxes(int $totalTaxes): void
    {
        $this->total_taxes = $totalTaxes;
    }

    public function getSubtotal(): int
    {
        return $this->subtotal;
    }

    public function setSubtotal(int $subtotal): void
    {
        $this->subtotal = $subtotal;
    }

    public function getShippingCosts(): int
    {
        return $this->shipping_costs;
    }

    public function setShippingCosts(int $shippingCosts): void
    {
        $this->shipping_costs = $shippingCosts;
    }

    public function getOrderTotal(): int
    {
        return $this->order_total;
    }

    public function setOrderTotal(int $orderTotal): void
    {
        $this->order_total = $orderTotal;
    }

    public function getShippingDetails(): OrderShippingDetail
    {
        return $this->shippingDetails()->get()->firstOrFail();
    }

    /**
     * @return HasOne<OrderShippingDetail>
     */
    public function shippingDetails(): HasOne
    {
        return $this->hasOne(OrderShippingDetail::class);
    }

    public function getBillingDetails(): OrderBillingDetail
    {
        return $this->billingDetails()->get()->firstOrFail();
    }

    /**
     * @return HasOne<OrderBillingDetail>
     */
    public function billingDetails(): HasOne
    {
        return $this->hasOne(OrderBillingDetail::class);
    }

    public function getClient(): OrderClientDetail
    {
        return $this->client()->firstOrFail();
    }

    /**
     * @return HasOne<OrderClientDetail>
     */
    public function client(): HasOne
    {
        return $this->hasOne(OrderClientDetail::class);
    }

    /**
     * @return Collection<int, OrderProduct>
     */
    public function getProducts(): Collection
    {
        return $this->products()->get();
    }

    /**
     * @return HasMany<OrderProduct>
     */
    public function products(): HasMany
    {
        return $this->hasMany(OrderProduct::class);
    }

    /**
     * @return Collection<int, OrderGiftedProduct>
     */
    public function getGiftedProducts(): Collection
    {
        return $this->giftedProducts()->get();
    }

    /**
     * @return HasMany<OrderGiftedProduct>
     */
    public function giftedProducts(): HasMany
    {
        return $this->hasMany(OrderGiftedProduct::class);
    }

    public function setStatus(OrderStatusModel $status): void
    {
        $this->order_status_id = $status->id;
    }

    public function getStatus(): OrderStatusModel
    {
        return OrderStatusModel::whereId($this->order_status_id)->firstOrFail();
    }

    public function setPaymentMethod(PaymentMethod $paymentMethod): void
    {
        $this->payment_method = $paymentMethod::class;
        $this->payment_method_name = $paymentMethod->getName();
    }

    public function getPaymentMethodInstructions(): ?string
    {
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = resolve($this->payment_method);
        return $paymentMethod->getUserInstructions();
    }

    public function getPaymentStatus(): ?string
    {
        return $this->payment_status;
    }

    public function getDate(): DateTime
    {
        return $this->created_at;
    }

    /**
     * @return Collection<OrderCoupon>
     */
    public function getCoupons(): Collection
    {
        return $this->hasMany(OrderCoupon::class)->get();
    }

    /**
     * @return string|null
     */
    public function getIpAddress(): ?string
    {
        return $this->ip_address;
    }

    /**
     * @param string|null $ip_address
     */
    public function setIpAddress(?string $ip_address): void
    {
        $this->ip_address = $ip_address;
    }

    /**
     * @return string|null
     */
    public function getVatNumber(): ?string
    {
        return $this->vat_number;
    }

    /**
     * @param string|null $vat_number
     */
    public function setVatNumber(?string $vat_number): void
    {
        $this->vat_number = $vat_number;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->country_code;
    }

    /**
     * @param string|null $country_code
     */
    public function setCountryCode(?string $country_code): void
    {
        $this->country_code = $country_code;
    }

    public function setDiscountByUnit(array $discountByUnit): void
    {
        $this->discount_by_unit = $discountByUnit ?? 0;
    }

    public function getDiscountByUnit(): array
    {
        return $this->discount_by_unit ?? [];
    }

    public function getOrderNotes(): ?string
    {
        return $this->order_notes;
    }

    public function setOrderNotes(?string $order_notes): void
    {
        $this->order_notes = $order_notes;
    }
}
