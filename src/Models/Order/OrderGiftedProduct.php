<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Models\Order;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Database\Eloquent\Casts\Attribute;

class OrderGiftedProduct extends OrderProduct
{
    /** @var string */
    protected $table = 'order_gifted_products';

    public function discountedUnitPrice(): Attribute
    {
        return Attribute::make(
            set: static fn(?string $value) => 0
        );
    }

    public function unitPrice(): Attribute
    {
        return Attribute::make(
            set: static fn(string $value) => 0
        );
    }

    public function total(): Attribute
    {
        return Attribute::make(
            set: static fn(string $value) => 0
        );
    }

    public function taxAmount(): Attribute
    {
        return Attribute::make(
            set: static fn(string $value) => 0
        );
    }

    /**
     * @throws InvalidPriceException
     */
    public function getDiscountedUnitPrice(): ?Price
    {
        return Price::fromInt(0);
    }

    /**
     * @throws InvalidPriceException
     */
    public function getTaxAmount(): Price
    {
        return Price::fromInt(0);
    }

    /**
     * @throws InvalidPriceException
     */
    public function getTotal(): Price
    {
        return Price::fromInt(0);
    }

    /**
     * @throws InvalidPriceException
     */
    public function getUnitPrice(): Price
    {
        return Price::fromInt(0);
    }
}
