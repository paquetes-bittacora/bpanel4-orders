<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Listeners;

use Bittacora\Bpanel4\Orders\Events\OrderConfirmed;
use Bittacora\Bpanel4\Orders\Services\Order\ClientEmailSender;

final class ClientOrderConfirmationEmailListener
{
    public function __construct(private readonly ClientEmailSender $sender)
    {
    }

    public function handle(OrderConfirmed $event): void
    {
        $this->sender->send($event->order);
    }
}
