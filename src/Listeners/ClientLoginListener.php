<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Listeners;

use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Models\Cart\CartRow;
use Bittacora\Bpanel4\Orders\Services\ClientCartService;
use Illuminate\Auth\Events\Login;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

final class ClientLoginListener
{
    public function __construct(private readonly ClientCartService $clientCartService)
    {
    }

    /*
     * Al hacer login, si en el carrito había productos, lo pasamos al carrito del cliente que hace login. Si el carrito
     * está vacío, mantenemos el carrito que tuviese el cliente.
     */
    public function handle(Login $login): void
    {
        $client = Client::where('user_id', $this->getUserId($login))->first();

        if (null === $client) {
            return;
        }

        $guestCartProducts = $this->clientCartService->getAnonymousCart()->getProducts();
        if ($guestCartProducts->isEmpty()) {
            // Si el carrito estaba vacío, mantenemos el que tuviese el cliente
            return;
        }

        $clientCart = $this->createNewCartForClient($client);
        $this->moveGuestProductsToClientCart($guestCartProducts, $clientCart);
    }

    private function createNewCartForClient(Client $client): Cart {
        $clientCart = $this->clientCartService->get($client);
        return $this->clientCartService->get($client);
    }

    /**
     * @param Collection<int, CartableProduct&Model> $guestCartProducts
     */
    private function moveGuestProductsToClientCart(Collection $guestCartProducts, Cart $clientCart): void
    {
        foreach ($guestCartProducts as $product) {
            $cartProduct = CartRow::where('id', $product->getCartableProductId())->firstOrFail();
            $cartProduct->cart_id = $clientCart->id;
            $cartProduct->cartable_id = $product->getId();
            $cartProduct->save();
        }
    }

    private function getUserId(Login $login): ?int
    {
        if (!is_numeric($login->user->id)) {
            return null;
        }

        return (int) $login->user->id;
    }
}