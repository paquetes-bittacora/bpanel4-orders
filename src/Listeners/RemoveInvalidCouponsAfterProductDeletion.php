<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Listeners;

use Bittacora\Bpanel4\Coupons\Exceptions\NotApplicableCouponException;
use Bittacora\Bpanel4\Coupons\Services\CartCouponRemover;
use Bittacora\Bpanel4\Coupons\Services\CouponValidator;
use Bittacora\Bpanel4\Orders\Events\ProductHasBeenRemovedFromCart;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Services\CartService;

final class RemoveInvalidCouponsAfterProductDeletion
{
    public function __construct(
        private readonly CouponValidator $couponValidator,
        private readonly CartService $cartService,
        private readonly CartCouponRemover $cartCouponRemover,
    ) {
    }

    public function handle(ProductHasBeenRemovedFromCart $event): void
    {
        $cart = Cart::whereId($event->cartId)->firstOrFail();
        $this->cartService->setCart($cart);

        foreach ($cart->getCoupons() as $coupon) {
            try {
                $this->couponValidator->validateCoupon($coupon, $this->cartService);
            } catch (NotApplicableCouponException) {
                $this->cartCouponRemover->removeCouponFromCart($coupon);
            }
        }
    }
}