<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Listeners;

use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Coupons\Exceptions\NotApplicableCouponException;
use Bittacora\Bpanel4\Coupons\Services\CartCouponRemover;
use Bittacora\Bpanel4\Coupons\Services\CouponValidator;
use Bittacora\Bpanel4\Orders\Events\ProductQuantityHasBeenUpdated;
use Bittacora\Bpanel4\Orders\Services\CartService;

final class RemoveInvalidCouponsAfterProductQuantityChange
{
    public function __construct(
        private readonly CouponValidator $couponValidator,
        private readonly ClientService $clientService,
        private readonly CartCouponRemover $cartCouponRemover,
        private readonly CartService $cartService,
    ) {
    }

    public function handle(ProductQuantityHasBeenUpdated $event): void
    {
        $cart = $this->clientService->getClientCart();
        $this->cartService->setCart($cart);

        foreach ($cart->getCoupons() as $coupon) {
            try {
                $this->couponValidator->validateCoupon($coupon, $this->cartService);
            } catch (NotApplicableCouponException) {
                $this->cartCouponRemover->removeCouponFromCart($coupon);
            }
        }
    }
}