<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Listeners;

use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Events\OrderConfirmed;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Models\Cart\CartRow;
use Bittacora\Bpanel4\Orders\Services\ClientCartService;
use Bittacora\Bpanel4\Orders\Services\Order\ClientEmailSender;
use Bittacora\Bpanel4Users\Models\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

final class GuestOrderConfirmedListener
{
    public function __construct()
    {
    }

    /**
     * Como por ahora no hay una forma buena de manejar los pedidos como invitado en bPanel, lo que hago es "anular"
     * al usuario invitado una vez se confirma el pedido.
     */
    public function handle(OrderConfirmed $event): void
    {
        /** @var User $user */
        $client = Client::whereId($event->order->getClient()->getClientId())->firstOrFail();
        $user = $client->getUser();

        if (null === $user || !$user->isGuest()) {
            return;
        }

        // Establecemos una contraseña aleatoria por seguridad y lo eliminamos (soft-delete). También modificamos el
        // email, ya que al ser único en la BD, si lo guardamos bien no permitiría más pedidos ni registrarse más
        // adelante con ese email
        $user->setPassword(Hash::make(bin2hex(random_bytes(4))));
        $user->setEmail('GUEST_' . $user->getId() .'_' . $user->getEmail());
        $user->save();
    }
}