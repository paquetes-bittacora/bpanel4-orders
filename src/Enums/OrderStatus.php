<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Enums;

enum OrderStatus: int
{
    case CREATED = 1;
    case PAYMENT_PENDING = 2;
    case PAYMENT_COMPLETED = 3;
    case PAYMENT_ERROR = 4;
    case CANCELLED = 5;
    case COMPLETED = 6;
    case SENT = 7;
    case WAITING = 8;
}
