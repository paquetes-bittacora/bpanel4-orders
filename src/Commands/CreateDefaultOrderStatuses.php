<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Commands;

use Bittacora\OrderStatus\Models\OrderStatusModel;
use Illuminate\Console\Command;

final class CreateDefaultOrderStatuses extends Command
{
    protected $signature = 'bpanel4-orders:create-default-order-statuses';

    protected $description = 'Instala el paquete para el carrito y los pedidos de bPanel 4.';

    public function handle(): void
    {
        OrderStatusModel::create(['name' => 'Creado', 'background_color' => '#f7f7f7', 'color' => '#000000']);
        OrderStatusModel::create(['name' => 'Pendiente de pago', 'background_color' => '#6688c3', 'color' => '#ffffff']);
        OrderStatusModel::create(['name' => 'Pagado', 'background_color' => '#b25da6', 'color' => '#ffffff']);
        OrderStatusModel::create(['name' => 'Error en el pago', 'background_color' => '#b25da6', 'color' => '#ffffff']);
        OrderStatusModel::create(['name' => 'Cancelado', 'background_color' => '#f20707', 'color' => '#f7ff00']);
        OrderStatusModel::create(['name' => 'Completado', 'background_color' => '#aadabb', 'color' => '#000000']);
        OrderStatusModel::create(['name' => 'Enviado', 'background_color' => '#48a56a', 'color' => '#ffffff']);
        OrderStatusModel::create(['name' => 'En espera', 'background_color' => '#000000', 'color' => '#ffffff']);
    }
}
