<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Orders\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use JsonException;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    private const PERMISSIONS = ['index', 'create', 'show', 'edit', 'destroy', 'store', 'update'];
    private const REGULAR_USER_PERMISSIONS = [
        'order.confirm',
        'order.confirmed',
        'cart.show',
        'cart.remove-product',
        'cart.preview',
        'my-orders.index',
        'my-orders.show',
    ];

    protected $signature = 'bpanel4-orders:install';

    protected $description = 'Instala el paquete para el carrito y los pedidos de bPanel 4.';

    public function handle(AdminMenu $adminMenu): void
    {
        $this->addJsDependencies();
        $this->registerModuleJs();
        $this->createDefaultOrderStatuses();
        $this->createMenuEntries($adminMenu);
        $this->createTabs();
        $this->giveAdminPermissions();
        $this->giveRegularUserPermissions();
    }

    private function registerModuleJs(): void
    {
        $path = base_path('resources/bpanel4/assets/js/app.public.js');
        $originalContent = file_get_contents($path);

        if (str_contains($originalContent, 'bPanel 4 orders')) {
            return;
        }

        $newContent = $originalContent . "\n\n// bPanel 4 orders\nimport '/vendor/bittacora/bpanel4-orders/resources/assets/js/bpanel4-orders.js'";


        file_put_contents($path, $newContent);
    }


    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createGroup('ecommerce', 'Tienda', 'far fa-shopping-cart');
        $adminMenu->createModule(
            'ecommerce',
            'orders-admin',
            'Pedidos',
            'far fa-shopping-cart'
        );
        $adminMenu->createAction(
            'orders-admin',
            'Listado',
            'index',
            'fas fa-list'
        );
    }

    private function createTabs(): void
    {
        Tabs::createItem(
            'orders-admin.edit',
            'orders-admin.index',
            'orders-admin.index',
            'Listado',
            'fa fa-list'
        );
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        /** @var Role $adminRole */
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'orders-admin.' . $permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function giveRegularUserPermissions(): void
    {
        $this->comment('Añadiendo permisos para los clientes...');
        /** @var Role $adminRole */

        $role = Role::findOrCreate('any-user');
        foreach (self::REGULAR_USER_PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => $permission]);
            $role->givePermissionTo($permission);
        }
    }

    /**
     * @throws JsonException
     */
    public function addJsDependencies(): void
    {
        $path = base_path('package.json');

        $packageJson = json_decode(file_get_contents($path), true, 512, JSON_THROW_ON_ERROR);

        if (!isset($packageJson['dependencies']['toastr'])) {
            $packageJson['dependencies']['toastr'] = '^2.1.4';
            file_put_contents($path, json_encode($packageJson, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT));
            $this->warn('El paquete bPanel 4 Orders necesita que ejecutes npm install');
        }
    }

    public function createDefaultOrderStatuses(): void
    {
        Artisan::call('bpanel4-orders:create-default-order-statuses');
    }
}
