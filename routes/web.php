<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Orders\Http\Controllers\CartAccountController;
use Bittacora\Bpanel4\Orders\Http\Controllers\CartController;
use Bittacora\Bpanel4\Orders\Http\Controllers\CartShippingController;
use Bittacora\Bpanel4\Orders\Http\Controllers\MyOrdersController;
use Bittacora\Bpanel4\Orders\Http\Controllers\OrderAdminController;
use Bittacora\Bpanel4\Orders\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;

Route::prefix('/carrito')->name('cart.')->middleware(['web'])->group(function () {
    Route::get('/', [CartController::class, 'show'])->name('show');
    Route::get('/seleccionar-envio', [CartShippingController::class, 'show'])->name('select-shipping');
    Route::post('/vista-previa', [CartController::class, 'preview'])->name('preview');
    Route::get('{cartRow}/quitar', [CartController::class, 'removeProduct'])->name('remove-product');
    Route::get('/crear-cuenta', [CartAccountController::class, 'showRegistration'])->name('show-registration');
    Route::post('/crear-cuenta', [CartAccountController::class, 'register'])->name('register');
});

Route::prefix('/pedido')->name('order.')->middleware(['web'])->group(function () {
    Route::get('/confirmar', [OrderController::class, 'confirm'])->name('confirm');
    Route::get('/pedido-confirmado', [OrderController::class, 'confirmed'])->name('confirmed');
    Route::get('/cancelado', [OrderController::class, 'cancelled'])->name('cancelled');
});

Route::prefix('bpanel/pedidos')->name('orders-admin.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function () {
        Route::get('/listar', [OrderAdminController::class, 'index'])->name('index');
        Route::get('{order}/editar', [OrderAdminController::class, 'edit'])->name('edit');
        Route::put('{order}/actualizar', [OrderAdminController::class, 'update'])->name('update');
        Route::put('{order}/cambiar-estado', [OrderAdminController::class, 'updateStatus'])->name('updateStatus');
        Route::delete('{order}/eliminar', [OrderAdminController::class, 'destroy'])->name('destroy');
        Route::get('/comprobar-num-factura/{order}/{invoiceNumber}', [OrderAdminController::class, 'checkInvoiceNumber'])->name('checkInvoiceNumber');
    });

Route::post('/create-cart', [CartController::class, 'create']); // Se usa principalmente en los tests

Route::prefix('mi-cuenta')->name('my-account.')->middleware(['web'])->group(static function () {
    Route::get('/mis-pedidos', [MyOrdersController::class, 'index'])->name('orders');
    Route::get('/ver-pedido/{order}', [MyOrdersController::class, 'show'])->name('view-order');
});
