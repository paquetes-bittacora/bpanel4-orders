<?php

declare(strict_types=1);

return [
    'product_class' => '', // Definir la clase que implementa la interfaz \Bittacora\Bpanel4\Orders\Contracts\CartableProduct
    'ignore_stock' => false, // Poner a true para que no se compruebe el stock antes de confirmar un pedido
    // Generar facturas automáticamente cuando se confirma el pago. Si se pone a false, se generan desde el listado de pedidos
    'generate_invoice_when_payment_completed' => false,
    // Email al que se enviarán los correos de administración sobre los pedidos
    'orders_admin_email' => config('bpanel4.admin_email'),
    // Activa o desactiva el botón para descargar facturas desde el apartado "Mi cuenta"
    'clients_can_download_invoices' => true,
    // Reenviar la factura después de hacer cambios en el pedido desde el panel. El pedido debe pasarse a "En espera"
    // y después otra vez a "Completado" para que sea reenviado.
    'resend_invoice_after_modification' => false,
    // Al ir actualizando el formato de las facturas (sobre todo en Eurojamón), se ha hecho innecesario poder editar
    // los detalles del cliente en la vista de edición de pedido, porque en la factura solo se muestran la dirección de
    // envío y la de facturación. Creo esta opción para poder mostrarlos otra vez si es necesario.
    'hide_client_details_on_order_edit' => true,
    // Indica si se muestra un botón en el carrito para terminar el pedido como invitado
    'enable-guest-orders' => true,
];
